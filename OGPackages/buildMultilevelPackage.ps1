param([string]$build)

$unityPath = "C:\Program Files\Unity\Hub\Editor\2020.3.0f1\Editor";
$unityExe = "Unity.exe";
$export = "$PSScriptRoot/../../Packages";
$exportFile = "MultiLevelStatisticsSystemVer$build.unitypackage"

if(!(Test-Path $export -PathType Container))
{
    New-Item -ItemType Directory -Force -Path $export
}

$asset1 = "Assets/Common/Animation";
$asset2 = "Assets/Common/Extensions";
$asset3 = "Assets/Common/Systems";
$asset4 = "Assets/Common/UI";
$asset5 = "Assets/Common/Utilities/General";
$asset6 = "Assets/Multilevel Stats System";

&"$unityPath/$unityExe" -batchmode -projectPath "$PSScriptRoot/.." -exportPackage "$asset1" "$asset2" "$asset3" "$asset4" "$asset5" "$asset6" "$export/$exportFile" -quit