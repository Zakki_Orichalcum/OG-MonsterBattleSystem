using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoTestController : MonoBehaviour
{
    public BattleType BattleType;
    public MonsterScriptable[] Monsters;

    public void DemoTest()
    {
        var e = new EncounterInformation();
        e.BattleType = BattleType;
        e.Units = new UnitInformation[]
        {
            new UnitInformation
            {
                
            }
        };

    }
}
