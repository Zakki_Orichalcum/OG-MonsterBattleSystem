﻿using UnityEngine;
using System.Collections;

public class RunescapeExperienceFunction : Function
{
    public bool DebugFunction = false;
    private string totalwalk = "";

    public override int Invoke(int input)
    {
        totalwalk = "";

        var s = InnerCurve(input);

        if(DebugFunction)
            Debug.Log($"{totalwalk} || {s}");

        return Mathf.FloorToInt( s/ 4.0f);
    }

    public override float Invoke(float input)
    {
        return Invoke(Mathf.RoundToInt(input));
    }

    public float InnerCurve(int lvl)
    {
        if (lvl == 0)
        {
            totalwalk += "0";
            return 0;
        }

        var a = lvl / 7.0f;
        var p = Mathf.Pow(2, a);
        var b = 300 * p;
        var c = lvl + b;
        var s = Mathf.Floor(c);
        
        var t = InnerCurve(lvl - 1) + s;

        totalwalk += $"{s} ({t}), ";

        return t;
    }
}
