using OrichalcumGames.Statistics.MultiLevelStatistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperienceDisplayController : MonoBehaviour
{
    public Image Sprite;
    public Text Amount;

    private float speed = 0;
    
    public void Init(LevelStatInformation[] stats, int amount, float sp, float lifeInMilliseconds)
    {
        var sprites = new List<Image> { Sprite };
        if(stats.Length > 1)
        {
            for(var i = 1; i < stats.Length; i++)
            {
                var s = Instantiate<Image>(Sprite,
                    Sprite.rectTransform.position + (new Vector3(Sprite.rectTransform.GetWidth() * i, 0)),
                    Sprite.rectTransform.rotation,
                    transform);
                sprites.Add(s);
            }

            Amount.rectTransform.localPosition += new Vector3(Sprite.rectTransform.GetWidth() * (stats.Length - 1), 0);
        }

        Amount.text = amount.ToString();
        for(var i = 0; i < stats.Length; i++)
        {
            var s = sprites[i];
            s.sprite = stats[i].Sprite;
        }

        speed = sp;
        Destroy(gameObject, lifeInMilliseconds);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition += new Vector3(0, speed);
    }
}
