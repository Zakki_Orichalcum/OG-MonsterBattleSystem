using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace OrichalcumGames.Statistics.MultiLevelStatistics
{
    public class MultiLevelStatistics : Statistics, IEnableable
    {
        public const string GainExperienceNotification = "MultiLevelStatistics.GainExperience";
        public static string ExperienceDidChangeNotification(string stat) => Statistics.DidChangeNotification(Exp(stat));
        public static string CurrentLevelDidChangeNotification(string stat) => Statistics.DidChangeNotification(stat);
        public static string MaxLevelDidChangeNotification(string stat) => Statistics.DidChangeNotification(Max(stat));

        public LevelStatInformation[] LevelStats;
        public int MaxStatLimit = 99;
        public int MaxStatMin = 1;
        public Function LevelFunc;

        public static string Max(string stat) => $"{stat}.Max";
        public static string Exp(string stat) => $"{stat}.Exp";

        public override void Disable()
        {
            this.RemoveObserver(OnStatGainedExperience, GainExperienceNotification, this);
            foreach (var ls in LevelStats)
            {
                this.RemoveObserver(OnExperienceDidChange, ExperienceDidChangeNotification(ls.StatName), this);
            }
        }

        public override void Enable()
        {
            this.AddObserver(OnStatGainedExperience, GainExperienceNotification, this);
            foreach(var ls in LevelStats)
            {
                this.AddObserver(OnExperienceDidChange, ExperienceDidChangeNotification(ls.StatName), this);
            }
        }

        // Start is called before the first frame update
        public override void OnAwake()
        {
            var list = new List<StatInformation>();
            foreach(var ls in LevelStats)
            {
                list.Add(new StatInformation {
                    Name = ls.StatName,
                    HasMinimun = true,
                    MinimumValue = MaxStatMin
                });
                list.Add(new StatInformation
                {
                    Name = Max(ls.StatName),
                    HasMaximum = true,
                    MaximumValue = MaxStatLimit,
                    HasMinimun = true,
                    MinimumValue = MaxStatMin
                });
                list.Add(new StatInformation
                {
                    Name = Exp(ls.StatName),
                    HasMinimun = true,
                    MinimumValue = 0
                });
            }
            Stats = list.ToArray();

            base.OnAwake();

            InitializeValues();
        }

        void OnStatGainedExperience(object sender, object args)
        {
            Tuple<string[], int> s = args as Tuple<string[], int>;

            foreach(var t in s.Item1)
            {
                this[Exp(t)] += s.Item2;
            }

        }

        void OnExperienceDidChange(object sender, object args)
        {
            DidChangeArgs s = args as DidChangeArgs;
            var sn = s.StatName.Split('.')[0];

            var a = LevelForExperience(s.NewValue);
            var cmax = this[Max(sn)];
            if (a > cmax)
            {
                this[sn] += a - cmax;

                SetValue(Max(sn), a, false);
            }
        }

        public int ExperiencePointsToNextLevel(string stat)
        {
            var max = this[Max(stat)];
            return ExperienceForLevel(max + 1) - this[Exp(stat)];
        }

        public int TotalExperiencePointsToNextLevel(string stat)
        {
            var max = this[Max(stat)];
            return ExperienceForLevel(max + 1) - ExperienceForLevel(max);
        }

        public int LevelForExperience(int exp)
        {
            int lvl = MaxStatLimit;
            for (; lvl >= MaxStatMin; --lvl)
                if (exp >= ExperienceForLevel(lvl))
                    break;
            return lvl;
        }

        public int ExperienceForLevel(int level)
        {
            return LevelFunc.Invoke(level - MaxStatMin);
        }
    }

    [System.Serializable]
    public class LevelStatInformation
    {
        public string StatName;
        public Sprite Sprite;
    }

    //public struct LevelInfo
    //{
    //    public int Current;
    //    public int Max;
    //    public int 
    //}

    public static class MultilevelStatisticsExtensions
    {
        public static void AddExperienceNotication(this object o, MultiLevelStatistics s, string stat, int amount)
        {
            s.PostNotification(MultiLevelStatistics.GainExperienceNotification, new Tuple<string[], int>(new string[] { stat }, amount));
        }

        public static void AddExperienceNotication(this object o, MultiLevelStatistics s, string[] stats, int amount)
        {
            s.PostNotification(MultiLevelStatistics.GainExperienceNotification, new Tuple<string[], int>(stats, amount));
        }
    }
}