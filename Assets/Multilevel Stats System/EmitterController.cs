﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics.MultiLevelStatistics;
using System;
using System.Linq;

public class EmitterController : MonoBehaviour
{
    [SerializeField]
    private MultiLevelStatistics _stats;
    [SerializeField]
    private ExperienceDisplayController ExpPrefab;

    public float Speed;
    public float Lifetime;

    private void OnEnable()
    {
        this.AddObserver(OnGainExperience, MultiLevelStatistics.GainExperienceNotification, _stats);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnGainExperience, MultiLevelStatistics.GainExperienceNotification, _stats);
    }

    public void OnGainExperience(object sender, object args)
    {
        Tuple<string[], int> a = args as Tuple<string[], int>;

        var i = Instantiate<ExperienceDisplayController>(ExpPrefab, transform);
        i.Init(a.Item1.Select(x => _stats.LevelStats.First(y => y.StatName == x)).ToArray(), a.Item2, Speed, Lifetime);
    }
}
