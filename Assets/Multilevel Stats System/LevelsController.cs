using OrichalcumGames.Statistics.MultiLevelStatistics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelsController : MonoBehaviour
{
    public ScrollRect ScrollView;
    public LevelPanelController LevelPrefab;
    public GameObject Checks;
    public ToggleController TogglesPrefab;
    public InputField ExpInput;

    private MultiLevelStatistics _stats;

    // Start is called before the first frame update
    void Start()
    {
        _stats = GetComponent<MultiLevelStatistics>();

        foreach(var s in _stats.LevelStats)
        {
            var st = Instantiate<LevelPanelController>(LevelPrefab, ScrollView.content);
            st.Init(_stats, s);

            var t = Instantiate<ToggleController>(TogglesPrefab, Checks.transform);
            t.Init(s);
        }
    }

    public void GetExperience()
    {
        var ch = Checks.GetComponentsInChildren<ToggleController>().Where(x => x.IsChecked()).Select(y => y.Label.text);

        var amount = int.Parse(ExpInput.text);

        this.AddExperienceNotication(_stats, ch.ToArray(), amount);
    }
}
