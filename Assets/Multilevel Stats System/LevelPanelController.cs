using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OrichalcumGames.Statistics.MultiLevelStatistics;
using OrichalcumGames.Statistics;
using System;

public class LevelPanelController : MonoBehaviour
{
    [SerializeField]
    public Image Icon;
    public Text CurrentLevel;
    public Text MaxLevel;
    public Text NextExperience;
    public Text TotalExperience;

    private MultiLevelStatistics _stats;
    private string _statName;

    public void Init(MultiLevelStatistics s, LevelStatInformation st)
    {
        _stats = s;
        _statName = st.StatName;

        Icon.sprite = st.Sprite;

        CurrentLevel.text = _stats[_statName].ToString();
        MaxLevel.text = _stats[MultiLevelStatistics.Max(_statName)].ToString();
        TotalExperience.text = _stats[MultiLevelStatistics.Exp(_statName)].ToString();
        NextExperience.text = _stats.ExperiencePointsToNextLevel(_statName).ToString();

        this.AddObserver(OnExperienceChanged, MultiLevelStatistics.ExperienceDidChangeNotification(_statName), _stats);
        this.AddObserver(OnMaxLevelChanged, MultiLevelStatistics.MaxLevelDidChangeNotification(_statName), _stats);
        this.AddObserver(OnCurrentLevelChanged, MultiLevelStatistics.CurrentLevelDidChangeNotification(_statName), _stats);
    }

    public void OnDisable()
    {
        this.RemoveObserver(OnExperienceChanged, MultiLevelStatistics.ExperienceDidChangeNotification(_statName), _stats);
        this.RemoveObserver(OnMaxLevelChanged, MultiLevelStatistics.MaxLevelDidChangeNotification(_statName), _stats);
        this.RemoveObserver(OnCurrentLevelChanged, MultiLevelStatistics.CurrentLevelDidChangeNotification(_statName), _stats);
    }

    private void OnCurrentLevelChanged(object sender, object args)
    {
        var did = args as DidChangeArgs;
        CurrentLevel.text = did.NewValue.ToString();
    }

    private void OnMaxLevelChanged(object sender, object args)
    {
        var did = args as DidChangeArgs;
        MaxLevel.text = did.NewValue.ToString();
    }

    private void OnExperienceChanged(object sender, object args)
    {
        var did = args as DidChangeArgs;
        TotalExperience.text = did.NewValue.ToString();
        NextExperience.text = _stats.ExperiencePointsToNextLevel(_statName).ToString();
    }
}
