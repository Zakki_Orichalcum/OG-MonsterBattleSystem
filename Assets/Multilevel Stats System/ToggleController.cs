using OrichalcumGames.Statistics.MultiLevelStatistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleController : MonoBehaviour
{
    public Toggle Toggle;
    public Image Image;
    public Text Label;

    public void Init(LevelStatInformation lsi)
    {
        Label.text = lsi.StatName;
        Image.sprite = lsi.Sprite;
    }

    public bool IsChecked()
    {
        return Toggle.isOn;
    }
}
