﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectTransformAnchorPositionTweener : Vector3Tweener
{
    private RectTransform RT;

    void Awake()
    {
        RT = transform as RectTransform;
    }

    protected override void OnUpdate()
    {
        base.OnUpdate();
        RT.anchoredPosition = currentTweenValue;
    }
}
