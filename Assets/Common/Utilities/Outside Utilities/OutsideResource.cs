﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OutsideResource
{
    public static T Load<T>(string key) where T : OutsideResource
    {
        var or = (T)Activator.CreateInstance(typeof(T));

        return (T)or.LoadResource(key);
    }
    public abstract OutsideResource LoadResource(string key);

    public static Dictionary<string, OutsideResource> LoadAll<T>() where T : OutsideResource
    {
        var or = (T)Activator.CreateInstance(typeof(T));

        return or.LoadAllResources();
    }
    public abstract Dictionary<string, OutsideResource> LoadAllResources();

    //public abstract IEnumerator Load<T>(string key);
}
