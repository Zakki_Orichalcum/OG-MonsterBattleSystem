﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RPG.Utility
{
    public class XElements
    {
        public static DateTime? SafeDate(XElement element)
        {
            if (element == null || string.IsNullOrEmpty(element.Value))
                return null;

            return element.Value.SafeDateNullable();
        }

        public static string XElementSafeValueString(XElement element)
        {
            return (element != null) ? element.Value ?? string.Empty : string.Empty;
        }
    }
}
