﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class MouseActions : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private float DoubleClickDelay = 0.25f;

    public UnityAction<PointerEventData> OnLeftClick;
    public UnityAction<PointerEventData> OnLeftClickDown;
    public UnityAction<PointerEventData> OnLeftClickUp;
    public UnityAction<PointerEventData> OnMiddleClick;
    public UnityAction<PointerEventData> OnMiddleClickDown;
    public UnityAction<PointerEventData> OnMiddleClickUp;
    public UnityAction<PointerEventData> OnRightClick;
    public UnityAction<PointerEventData> OnRightClickDown;
    public UnityAction<PointerEventData> OnRightClickUp;
    public UnityAction<PointerEventData> OnDoubleClick;
    public UnityAction<PointerEventData> OnMouseEnter;
    public UnityAction<PointerEventData> OnMouseExit;
    public UnityAction<Single> OnMouseScroll;
    public UnityAction<Single> OnMouseScrollUp;
    public UnityAction<Single> OnMouseScrollDown;

    private bool _firstClick { get; set; }
    private float _lastClickTime { get; set; }
    private bool _mouseIsOnObject = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        if(eventData.button == PointerEventData.InputButton.Left)
        {
            if (OnLeftClick != null)
            {
                OnLeftClick(eventData);
            }
            CheckDoubleClick(eventData);
        }
        else if(eventData.button == PointerEventData.InputButton.Middle)
        {
            if (OnMiddleClick != null)
            {
                OnMiddleClick(eventData);
            }
        }
        else
        {
            if (OnRightClick != null)
            {
                OnRightClick(eventData);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (OnLeftClickDown != null)
            {
                OnLeftClickDown(eventData);
            }
        }
        else if (eventData.button == PointerEventData.InputButton.Middle)
        {
            if (OnMiddleClickDown != null)
            {
                OnMiddleClickDown(eventData);
            }
        }
        else
        {
            if (OnRightClickDown != null)
            {
                OnRightClickDown(eventData);
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (OnLeftClickUp != null)
            {
                OnLeftClickUp(eventData);
            }
        }
        else if (eventData.button == PointerEventData.InputButton.Middle)
        {
            if (OnMiddleClickUp != null)
            {
                OnMiddleClickUp(eventData);
            }
        }
        else
        {
            if (OnRightClickUp != null)
            {
                OnRightClickUp(eventData);
            }
        }
    }

    public void CheckDoubleClick(PointerEventData eventData)
    {
        if (OnDoubleClick == null || eventData.button != PointerEventData.InputButton.Left)
            return;

        if (_firstClick && (Time.time - _lastClickTime) > DoubleClickDelay)
        {
            _firstClick = false;
        }

        if (!_firstClick)
        {
            _firstClick = true;
            _lastClickTime = Time.time;
        }
        else
        {
            _firstClick = false;
            OnDoubleClick(eventData);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(OnMouseEnter != null)
        {
            OnMouseEnter(eventData);
        }
        _mouseIsOnObject = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (OnMouseExit != null)
        {
            OnMouseExit(eventData);
        }
        _mouseIsOnObject = false;
    }

    private void Update()
    {
        if (_mouseIsOnObject)
        {
            var d = Input.GetAxis("Mouse ScrollWheel");
            if(d != 0)
            {
                if (OnMouseScroll != null)
                {
                    OnMouseScroll(d);
                }
                if (d < 0)
                {
                    if (OnMouseScrollDown != null)
                    {
                        OnMouseScrollDown(d);
                    }
                }
                else if(d > 0)
                {
                    if(OnMouseScrollUp != null)
                    {
                        OnMouseScrollUp(d);
                    }
                }
            }
            
        }
    }
}
