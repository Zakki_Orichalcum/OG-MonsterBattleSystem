﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MonobehaviorMouseActions : MonoBehaviour {

    [SerializeField]
    private float DoubleClickDelay = 0.25f;

    public UnityAction<Vector3> OnLeftClick;
    public UnityAction<Vector3> OnLeftClickDown;
    public UnityAction<Vector3> OnLeftClickUp;
    public UnityAction<Vector3> OnMiddleClick;
    public UnityAction<Vector3> OnMiddleClickDown;
    public UnityAction<Vector3> OnMiddleClickUp;
    public UnityAction<Vector3> OnRightClick;
    public UnityAction<Vector3> OnRightClickDown;
    public UnityAction<Vector3> OnRightClickUp;
    public UnityAction<Vector3> OnDoubleClick;
    public UnityAction OnMousePointerEnter;
    public UnityAction OnMousePointerExit;
    public UnityAction<Single> OnMouseScroll;
    public UnityAction<Single> OnMouseScrollUp;
    public UnityAction<Single> OnMouseScrollDown;

    private bool _firstClick { get; set; }
    private float _lastClickTime { get; set; }
    private bool _mouseIsOnObject = false;

    private bool _leftMouseDownFirst = false;
    private bool _rightMouseDownFirst = false;
    private bool _middleMouseDownFirst = false;

    private void OnMouseOver()
    {
        //LeftClick
        if (Input.GetMouseButtonDown(0))
        {
            if (OnLeftClickDown != null)
            {
                OnLeftClickDown(Input.mousePosition);
            }
            if (OnLeftClick != null)
            {
                _leftMouseDownFirst = true;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (OnLeftClickUp != null)
            {
                OnLeftClickUp(Input.mousePosition);
            }
            if (OnLeftClick != null && _leftMouseDownFirst)
            {
                OnMouseClick();
            }
            _leftMouseDownFirst = false;
        }

        //RightClick
        if (Input.GetMouseButtonDown(1))
        {
            if (OnRightClickDown != null)
            {
                OnRightClickDown(Input.mousePosition);
            }
            if (OnRightClick != null)
            {
                _rightMouseDownFirst = true;
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            if (OnRightClickUp != null)
            {
                OnRightClickUp(Input.mousePosition);
            }
            if (OnRightClick != null && _rightMouseDownFirst)
            {
                OnRightClick(Input.mousePosition);
            }
            _rightMouseDownFirst = false;
        }

        //MiddleClick
        if (Input.GetMouseButtonDown(2))
        {
            if (OnMiddleClickDown != null)
            {
                OnMiddleClickDown(Input.mousePosition);
            }
            if (OnMiddleClick != null)
            {
                _middleMouseDownFirst = true;
            }
        }
        if (Input.GetMouseButtonUp(2))
        {
            if (OnMiddleClickUp != null)
            {
                OnMiddleClickUp(Input.mousePosition);
            }
            if (OnMiddleClick != null && _middleMouseDownFirst)
            {
                OnMiddleClick(Input.mousePosition);
            }
            _middleMouseDownFirst = false;
        }
    }

    public void OnMouseClick()
    {
        if (OnLeftClick != null)
        {
            OnLeftClick(Input.mousePosition);
        }
        CheckDoubleClick();
    }

    public void CheckDoubleClick()
    {
        if (OnDoubleClick == null)
            return;

        if (_firstClick && (Time.time - _lastClickTime) > DoubleClickDelay)
        {
            _firstClick = false;
        }

        if (!_firstClick)
        {
            _firstClick = true;
            _lastClickTime = Time.time;
        }
        else
        {
            _firstClick = false;
            OnDoubleClick(Input.mousePosition);
        }
    }

    public void OnMouseEnter()
    {
        if (OnMousePointerEnter != null)
        {
            OnMousePointerEnter();
        }
        _mouseIsOnObject = true;
    }

    public void OnMouseExit()
    {
        if (OnMousePointerExit != null)
        {
            OnMousePointerExit();
        }
        _mouseIsOnObject = false;
    }

    private void Update()
    {
        if (_mouseIsOnObject)
        {
            var d = Input.GetAxis("Mouse ScrollWheel");
            if (d != 0)
            {
                if (OnMouseScroll != null)
                {
                    OnMouseScroll(d);
                }
                if (d < 0)
                {
                    if (OnMouseScrollDown != null)
                    {
                        OnMouseScrollDown(d);
                    }
                }
                else if (d > 0)
                {
                    if (OnMouseScrollUp != null)
                    {
                        OnMouseScrollUp(d);
                    }
                }
            }

        }
    }
}
