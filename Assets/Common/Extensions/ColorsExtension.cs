using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ColorsExtension
{
    // closed match in RGB space
    public static int ClosestColor(this List<Color> colors, Color target)
    {
        var colorDiffs = colors.Select(n => ColorDiff(n, target)).Min(n => n);
        return colors.FindIndex(n => ColorDiff(n, target) == colorDiffs);
    }

    public static Color Combine(this Color a, Color b)
    {
        return new Color((a.r + b.r) / 2f, (a.g + b.g) / 2f, (a.b + b.b) / 2f);
    }

    private static int ColorDiff(this Color c1, Color c2)
    {
        return (int)Math.Sqrt((c1.r - c2.r) * (c1.r - c2.r)
                                + (c1.g - c2.g) * (c1.g - c2.g)
                                + (c1.b - c2.b) * (c1.b - c2.b));
    }

    public static string ToHex(this Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static Color HexToColor(this string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }

    public static Color ColorFromWord(this string name, Color seed)
    {
        var substrings = name.ToLower().Split(3).ToArray();
        var plus = StringExtensions.LowerAlphabetString.IndexOf(name[name.Length - 1]) < 13;

        var r = 0;
        foreach(char c in substrings[0])
        {
            var n = StringExtensions.LowerAlphabetString.IndexOf(name[name.Length - 1]);
            r += plus ? n : -n;
        }

        var g = 0;
        foreach (char c in substrings[0])
        {
            var n = StringExtensions.LowerAlphabetString.IndexOf(name[name.Length - 1]);
            g += plus ? n : -n;
        }

        var b = 0;
        foreach (char c in substrings[0])
        {
            var n = StringExtensions.LowerAlphabetString.IndexOf(name[name.Length - 1]);
            b += plus ? n : -n;
        }

        var fr = r / 255f;
        var fg = g / 255f;
        var fb = b / 255f;

        return new Color(Mathf.Clamp(seed.r + fr, 0f, 1f), Mathf.Clamp(seed.g + fg, 0f, 1f), Mathf.Clamp(seed.b + fb, 0f, 1f));
    }
}