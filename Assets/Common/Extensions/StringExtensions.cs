﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

public static class StringExtensions
{
    public static string Format(this string str, params string[] args)
    {
        return string.Format(str, args);
    }

    public static string ToExpandedString(this IEnumerable<object> l)
    {
        var sb = new StringBuilder("[ ");

        for(var i = 0; i < l.Count(); i++)
        {
            var s = l.ElementAt(i);
            sb.Append(s.ToString());
            if (i < l.Count() - 1)
                sb.Append(", ");
        }
        sb.Append(" ]");

        return sb.ToString();
    }

    public static IEnumerable<string> Split(this string str, int n)
    {
        if (String.IsNullOrEmpty(str) || n < 1)
        {
            throw new ArgumentException();
        }

        return Enumerable.Range(0, str.Length / n)
                        .Select(i => str.Substring(i * n, n));
    }

    /// <summary>
    /// Replaces the format item in a specified System.String with the text 
    /// equivelent of the value from a corresponding System.Object reference in a specified array
    /// </summary>
    /// <param name="formatProvider">a composite format string</param>
    /// <param name="t">An System.Object to format</param>
    /// <exception cref="System.ArgumentNullException" />
    /// <exception cref="System.FormatException" />
    /// <returns>a compiled string</returns>
    public static string Combine(this string formatProvider, params object[] args)
    {
        return String.Format(formatProvider, args);
    }

    /// <summary>
    /// Executes a passed Regular Expression against a string and returns the result of the match
    /// </summary>
    /// <param name="s">the target string</param>
    /// <param name="RegEx">a System.String containing a regular expression</param>
    /// <returns>true if the regular expression matches the target string and false if it does not</returns>
    public static bool IsMatch(this string s, string RegEx)
    {
        return new Regex(RegEx).IsMatch(s);
    }

    /// <summary>
    /// Executes a passed Regular Expression against a string and returns the result of the match
    /// </summary>
    /// <param name="s">the target string</param>
    /// <param name="RegEx">any Regular Expression object</param>
    /// <returns>true if the regular expression matches the target string and false if it does not</returns>
    public static bool IsMatch(this string s, Regex RegEx)
    {
        return RegEx.IsMatch(s);
    }

    /// <summary>
    /// Checks to see if the string contains a valid email address.
    /// </summary>
    /// <param name="s">the target string</param>
    /// <returns>Returns TRUE if the string contains an email address and it is valid or FALSE if the string 
    /// has no email address or it is invalid</returns>
    public static bool IsValidEmail(this string s)
    {
        return s.IsMatch(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
    }

    /// <summary>
    /// Checks to see if the string is all numbers
    /// </summary>
    /// <param name="s">the target string</param>
    /// <returns>Returns TRUE if the string is all numbers or FALSE if the string has no numbers or is all alpha characters.</returns>
    public static bool IsInteger(this string s)
    {
        return !s.IsMatch("[^0-9-]") && s.IsMatch("^-[0-9]+$|^[0-9]+$");
    }

    /// <summary>
    /// Tests a string to see if it is a Positive Integer
    /// </summary>
    /// <param name="strNumber"></param>
    /// <returns></returns>
    public static bool IsNaturalNumber(this string strNumber)
    {
        return !strNumber.IsMatch("[^0-9]") && strNumber.IsMatch("0*[1-9][0-9]*");
    }

    /// <summary>
    /// Tests a string to see if it is a Positive Integer with zero inclusive
    /// </summary>
    /// <param name="strNumber"></param>
    /// <returns></returns>
    public static bool IsWholeNumber(this string strNumber)
    {
        return !strNumber.IsMatch("[^0-9]");
    }

    /// <summary>
    /// Tests a string to see if it a Positive number both Integer & Real
    /// </summary>
    /// <param name="strNumber"></param>
    /// <returns></returns>
    public static bool IsPositiveNumber(this string strNumber)
    {
        return !strNumber.IsMatch("[^0-9.]") && strNumber.IsMatch("^[.][0-9]+$|[0-9]*[.]*[0-9]+$") && !strNumber.IsMatch("[0-9]*[.][0-9]*[.][0-9]*");
    }

    /// <summary>
    /// Tests a string to see if it is a number
    /// </summary>
    /// <param name="strNumber"></param>
    /// <returns></returns>
    public static bool IsNumber(this string strNumber)
    {
        string strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
        string strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
        return !strNumber.IsMatch("[^0-9.-]") && !strNumber.IsMatch("[0-9]*[.][0-9]*[.][0-9]*") &&
            !strNumber.IsMatch("[0-9]*[-][0-9]*[-][0-9]*") && strNumber.IsMatch("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");
    }

    /// <summary>
    /// Tests a string to see if it contains alpha characters
    /// </summary>
    /// <param name="strToCheck"></param>
    /// <returns></returns>
    public static bool IsAlpha(this string strToCheck)
    {
        return !strToCheck.IsMatch("[^a-zA-Z]");
    }

    /// <summary>
    /// Tests a string to see if it contains alpha and numeric characters
    /// </summary>
    /// <param name="strToCheck"></param>
    /// <returns></returns>
    public static bool IsAlphaNumeric(this string strToCheck)
    {
        return !strToCheck.IsMatch("[^a-zA-Z0-9]");
    }

    /// <summary>
    /// Checks to see if a System.String value is a string representation of a boolean value.
    /// </summary>
    /// <param name="s">the target string</param>
    /// <returns>true if the System.String value is truthy or false if not</returns>
    public static bool IsBoolean(this string s)
    {
        string _lower = s.ToLower();
        return _lower.IsMatch("[true]|[false]|[0]|[1]");
    }

    /// <summary>
    /// Replicates a System.String the passed number of times and returns the value
    /// </summary>
    /// <param name="s">the target System.String</param>
    /// <param name="times">the number of times to replicate the string</param>
    /// <returns>the replicated string</returns>
    public static string Replicate(this string s, int times)
    {
        string hold = String.Empty;

        times.Times(delegate (int i)
        {
            hold += s;
        });

        return hold;
    }

    /// <summary>
    /// Converts a System.String value to a boolean type.
    /// </summary>
    /// <param name="s"></param>
    /// <returns>Returns the boolean representation of the strings value or FALSE if the string is not a valid boolean.</returns>
    /// <exception cref="System.FormatException" />
    /// <remarks>Calls extension IsBoolean()</remarks>
    public static bool InterpretAsBoolean(this string s)
    {
        if (s.IsBoolean())
        {
            return Convert.ToBoolean(s);
        }
        return false;
    }

    /// <summary>
    /// Converts a System.String value to an integer type.
    /// </summary>
    /// <param name="s"></param>
    /// <returns>Returns the integer representation of the strings value or -1 if the string is not a valid integer.</returns>
    /// <exception cref="System.FormatException" />
    /// <remarks>Calls extension IsNumeric()</remarks>
    public static int InterpretAsInteger(this string s)
    {
        if (s.IsInteger())
        {
            return Convert.ToInt32(s);
        }
        return -1;
    }

    /// <summary>
    /// Converts a System.String to proper case.
    /// </summary>
    /// <param name="s">the target string</param>
    /// <exception cref="System.FormatException" />
    /// <returns>a properly cased System.String</returns>
    public static string Capitalize(this string s)
    {
        string result = String.Empty;
        foreach (string p in s.Split(' '))
        {
            if (p != String.Empty)
                result += p.Substring(0, 1).ToUpper() + p.Substring(1) + " ";
        }
        return result;
    }

    /// <summary>
    /// Searches a given System.String for the next to last occurance of another System.String
    /// </summary>
    /// <param name="s">the string to search</param>
    /// <param name="index">the string to look for</param>
    /// <returns>the position of the occurance</returns>
    public static int NextToLastIndexOf(this string s, string index)
    {
        int i = 0;
        int end = s.LastIndexOf(index);

        if (end == -1) return end;

        do
        {
            i = s.IndexOf(index);
        }
        while (i > -1 && i < end);

        return i;
    }

    /// <summary>
    /// Allows iterating over every occurrence of the given pattern (which can be a string or a regular expression). Returns the original string.
    /// </summary>
    /// <param name="s">the string to scan</param>
    /// <param name="RegularExpression"></param>
    /// <param name="Delegate">function delegate that will process the matched items</param>
    /// <returns>The original string.</returns>
    public static string Scan(this string s, string RegularExpression, Action<string> Delegate)
    {
        if (s.IsMatch(RegularExpression))
        {
            Regex regex = new Regex(RegularExpression);
            MatchCollection mc = regex.Matches(s, 0);
            foreach (Match m in mc)
            {
                Delegate(m.Content);
            }
        }

        return s;
    }

    /// <summary>
    /// Allows iterating over every occurrence of the given pattern (which can be a string or a regular expression). Returns the original string.
    /// </summary>
    /// <param name="s">the strig to scan</param>
    /// <param name="RegularExpression">The pattern to match against</param>
    /// <param name="Delegate">function delegate that will process the matched items</param>
    /// <returns>The original string</returns>
    public static string Scan(this string s, Regex RegularExpression, Action<string> Delegate)
    {
        if (RegularExpression.IsMatch(s))
        {
            MatchCollection mc = RegularExpression.Matches(s, 0);
            foreach (Match m in mc)
            {
                Delegate(m.Content);
            }
        }

        return s;
    }

    /// <summary>
    /// Checks if a string is empty or null. Returns false if the string has data
    /// </summary>
    /// <param name="s">the string to check</param>
    /// <returns>false if the string has data</returns>
    public static bool HasValue(this string s)
    {
        if (!s.IsNull() || s.Trim().Equals(String.Empty)) return false;
        return true;
    }

    /// <summary>
    /// Checks if a string has any trailing or leading whitespace
    /// </summary>
    /// <param name="s">the string to check</param>
    /// <returns>true if the string can be trimmed</returns>
    public static bool CanBeTrimmed(this string s)
    {
        return !s.Equals(s.Trim());
    }

    public static Guid SafeGuid(this string stringIn)
    {
        return SafeGuid(stringIn, Guid.Empty);
    }
    public static Guid SafeGuid(this string stringIn, Guid defaultVal)
    {
        try
        {
            return new Guid(stringIn);
        }
        catch (Exception)
        {
            //log exception e
            return defaultVal;
        }

    }
    public static bool SafeBool(this string strIn, bool defaultVal = false)
    {
        if (string.IsNullOrEmpty(strIn))
            return defaultVal;

        if (strIn.Equals("On", StringComparison.CurrentCultureIgnoreCase)) strIn = "true";
        if (strIn.Equals("Off", StringComparison.CurrentCultureIgnoreCase)) strIn = "false";

        bool returnOut;
        return (bool.TryParse(strIn, out returnOut)) ? returnOut : defaultVal;
    }

    public static int SafeInt(this string stringIn, int defaultVal = -1)
    {
        int outVal;
        return (Int32.TryParse(stringIn, out outVal)) ? outVal : defaultVal;
    }
    public static double SafeDouble(this string strIn, double defaultVal = -1)
    {
        if (string.IsNullOrEmpty(strIn))
            return defaultVal;

        double returnOut;
        return (double.TryParse(strIn, out returnOut)) ? returnOut : defaultVal;
    }
    public static double SafeProbabilityBaseOnBool(this string strIn, double defaultOut = 0)
    {
        if (string.IsNullOrEmpty(strIn))
            return defaultOut;

        switch (strIn.ToLower())
        {
            case "on":
            case "true":
                return 1;
            case "off":
            case "false":
                return 0;
            case "half":
                return .5;

            default:
                var doubleVal = SafeDouble(strIn, defaultOut);
                if (doubleVal > 1 && doubleVal <= 100)
                    doubleVal = doubleVal / (double)100;

                return doubleVal;
        }
    }

    public static List<int> SafeIntList(this IEnumerable<string> stringsIn, int defaultValue = -1)
    {
        return stringsIn.Select(s => SafeInt(s)).Where(safeInt => safeInt >= 0).ToList();
    }

    public static string PadString(this string target, char paddingCharacter, int paddingLength, bool padLeftSide = true)
    {
        if (target.Length >= paddingLength)
        {
            return target;
        }

        var output = target;
        var difference = paddingLength - target.Length;
        for (var i = 0; i < difference; i++)
        {
            output = ((padLeftSide) ? paddingCharacter + output : output + paddingCharacter);
        }

        return output;
    }

    #region MATCHING AND GET BETWEEN

    public const string START_SIGNIFIER = "ST4RT";
    public const string END_SIGNIFIER = "3ND1NG";

    #region GET MATCHES (LAZY)

    public static Match GetMatchBetween(this string source, string start, string end, int startingPoint = 0)
    {
        var match = new Match();
        var checkIndex = source.IndexOf(start, startingPoint);
        match.BeginningTag = start;
        match.EndingTag = end;
        match.StartIndex = (checkIndex > -1) ? checkIndex + start.Length : -1;
        match.EndIndex = source.IndexOf(end, match.StartIndex);

        if (match.ContainsStart() && match.ContainsEnd())
        {
            match.Content = source.Substring(match.StartIndex, match.EndIndex - match.StartIndex);
        }
        else if (match.ContainsStart())
            match = GetMatchFromPartToTheEnd(source, start, end, startingPoint);
        else if (match.ContainsEnd())
            match = GetMatchFromBeginningToPart(source, end, start, startingPoint);

        return match;
    }
    public static Match GetMatchBetween(this string source, string[] starts, string[] ends, int startingPoint = 0)
    {
        Match earliestMatch = new Match();
        earliestMatch.Max();

        foreach (var start in starts)
        {
            foreach (var end in ends)
            {
                var nextMatch = GetMatchBetween(source, start, end, startingPoint);

                if (nextMatch.ContainsStart() && nextMatch.StartIndex < earliestMatch.StartIndex)
                    earliestMatch = nextMatch;
                else if (nextMatch.ContainsEnd() && nextMatch.StartIndex <= earliestMatch.StartIndex && nextMatch.EndIndex < earliestMatch.EndIndex)
                    earliestMatch = nextMatch;
            }
        }

        return earliestMatch;
    }
    public static Match GetMatchBetween(this string source, string start, string[] ends, int startingPoint = 0)
    {
        return GetMatchBetween(source, new string[] { start }, ends, startingPoint);
    }
    public static Match GetMatchBetween(this string source, string[] starts, string end, int startingPoint = 0)
    {
        return GetMatchBetween(source, starts, new string[] { end }, startingPoint);
    }

    public static Match GetMatchFromPartToTheEnd(this string source, string start, string originalEnd = "", int startingPoint = 0)
    {
        var match = GetMatchBetween(source + END_SIGNIFIER, start, END_SIGNIFIER, startingPoint);
        match.SetOriginalTags(start, (string.IsNullOrEmpty(originalEnd)) ? END_SIGNIFIER : originalEnd);

        return match;
    }

    public static Match GetMatchFromBeginningToPart(this string source, string end, string originalStart = "", int startingPoint = 0)
    {
        var match = GetMatchBetween(source.Insert(startingPoint, START_SIGNIFIER), START_SIGNIFIER, end, startingPoint);
        match.SetOriginalTags((string.IsNullOrEmpty(originalStart)) ? START_SIGNIFIER : originalStart, end);

        return match;
    }
    #endregion

    #region GET MATCHES (RIGOROUS)

    public static Match GetMatchBetweenAMatchingAndClosingTag(this string source, string start, string end, int startingPoint = 0)
    {
        var match = new Match() { BeginningTag = start, EndingTag = end };
        var toMatchFromStart = "";
        var toMatchFromEnd = "";

        var indexOfStart = source.IndexOf(start, startingPoint);
        if (indexOfStart == -1)
            return null;

        var numberOfMatchedStarts = 1;
        var numberOfMatchedEnds = 0;
        var indexOfEnd = 0;
        var index = match.StartIndex = indexOfStart + start.Length;
        while (numberOfMatchedStarts != numberOfMatchedEnds && index < source.Length)
        {
            var c = source[index];
            if (start.Contains(toMatchFromStart + c))
            {
                toMatchFromStart += c;
                if (toMatchFromStart.Equals(start))
                {
                    toMatchFromStart = "";
                    numberOfMatchedStarts++;
                }
            }

            if (end.Contains(toMatchFromEnd + c))
            {
                toMatchFromEnd += c;
                if (toMatchFromEnd.Equals(end))
                {
                    toMatchFromEnd = "";
                    numberOfMatchedEnds++;
                    indexOfEnd = index - end.Length;
                }
            }

            index++;
        }

        if (numberOfMatchedStarts != numberOfMatchedEnds)
            return null;

        match.EndIndex = indexOfEnd;
        match.Content = source.Substring(match.StartIndex, match.EndIndex - match.StartIndex);

        return match;
    }

    #endregion

    #region GET STRINGS
    public static string GetStringBetween(this string source, string start, string end, int startingPoint = 0)
    {
        return GetMatchBetween(source, start, end, startingPoint).ToString();
    }
    public static string GetStringBetween(this string source, string[] starts, string[] ends, int startingPoint = 0)
    {
        return GetMatchBetween(source, starts, ends, startingPoint).ToString();
    }
    public static string GetStringBetween(this string source, string start, string[] ends, int startingPoint = 0)
    {
        return GetMatchBetween(source, start, ends, startingPoint).ToString();
    }
    public static string GetStringBetween(this string source, string[] starts, string end, int startingPoint = 0)
    {
        return GetMatchBetween(source, starts, end, startingPoint).ToString();
    }

    public static string GetStringFromPartToTheEnd(this string source, string start, string originalEnd = "", int startingPoint = 0)
    {
        return GetMatchFromPartToTheEnd(source, start, originalEnd, startingPoint).ToString();
    }

    public static string GetStringFromBeginningToPart(this string source, string end, string originalStart = "", int startingPoint = 0)
    {
        return GetMatchFromBeginningToPart(source, end, originalStart, startingPoint).ToString();
    }
    #endregion

    #endregion

    #region PARSING
    public static int TimeStringToInt(this string timeStringIn)
    {
        var timeString = timeStringIn.Replace(".", string.Empty).Replace(":", string.Empty).ToLower().Trim();
        int firstTime = GetFirstIntFromStringStartingAt(timeString, 0);
        if (firstTime < 30)
            firstTime = firstTime * 100;  //for scenarios where they've entered something like '7 PM' rather than '7:00 PM'

        if (firstTime == 0)
            return 0;

        int indexOfAM = timeString.IndexOf("am"), indexOfPM = timeString.IndexOf("pm");
        var timeOfDay = (indexOfAM < indexOfPM && indexOfAM > -1) ? TimeOfDay.AM : TimeOfDay.PM;
        if (timeOfDay == TimeOfDay.PM)
            firstTime = firstTime + 1200;

        //they're on eastern daylight time, but some of their events are on central (CDT, CT)
        if (timeString.Contains("cdt") || timeString.Contains("ct"))
            firstTime = firstTime - 100;

        return firstTime;
    }
    private static int GetFirstIntFromStringStartingAt(this string stringIn, int index)
    {
        if (string.IsNullOrEmpty(stringIn))
            return 0;

        var sb = new StringBuilder();
        while (index < stringIn.Length && Char.IsDigit(stringIn[index]))
        {
            sb.Append(stringIn[index]);
            index++;
        }

        int intOut;
        return int.TryParse(sb.ToString(), out intOut) ? intOut : 0;
    }

    public enum TimeOfDay { AM = 1, PM = 2 };

    #endregion

    #region ADDITIONAL

    public static string CamelCaseToSpaces(this string stringIn, bool firstLetterToUpperCase = true)
    {
        if (string.IsNullOrEmpty(stringIn)) return string.Empty;

        if (firstLetterToUpperCase)
        {
            stringIn = Char.ToUpper(stringIn[0]) + stringIn.Substring(1);
        }

        return Regex.Replace(stringIn, "(\\B[A-Z])", " $1");
    }
    public static string GetFriendlyTitleFromCamelCaseFileName(this string itemName)
    {
        var replacedTitle = itemName.Replace("-", " ").Replace("_", " ");
        var extensionlessTitle = (replacedTitle.LastIndexOf('.') > 0) ? replacedTitle.Substring(0, replacedTitle.LastIndexOf('.')) : replacedTitle;

        return extensionlessTitle;
    }

    public static string GetMD5HashForString(this string stringIn)
    {
        using (var md5 = MD5.Create())
        {
            var stringBytes = Encoding.ASCII.GetBytes(stringIn);
            var md5Hash = md5.ComputeHash(stringBytes);

            return StringToHex(md5Hash);
        }
    }

    public static string StringToHex(this byte[] bytes, bool upperCase = true)
    {
        var result = new StringBuilder(bytes.Length * 2);

        for (var i = 0; i < bytes.Length; i++)
            result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

        return result.ToString();
    }

    #endregion


    public const string AlphabetString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public const string LowerAlphabetString = "abcdefghijklmnopqrstuvwxyz";
}

public class Match
{
    public string _content;
    public string Content
    {
        get { return (_content != null) ? _content : string.Empty; }
        set { _content = value; }
    }

    private int _startIndex = -1;
    public int StartIndex
    {
        get { return (_startIndex > -1) ? _startIndex : 0; }
        set { _startIndex = value; }
    }

    private int _endIndex = -1;
    public int EndIndex
    {
        get { return (_endIndex > -1) ? _endIndex : 0; }
        set { _endIndex = value; }
    }

    private string _beginningTag;
    public string BeginningTag
    {
        get { return (_originalBeginningTag != null) ? _originalBeginningTag : (_beginningTag != null) ? _beginningTag : string.Empty; }
        set { _beginningTag = value; }
    }

    private string _endingTag;
    public string EndingTag
    {
        get { return (_originalEndingTag != null) ? _originalEndingTag : (_endingTag != null) ? _endingTag : string.Empty; }
        set { _endingTag = value; }
    }

    private string _originalBeginningTag;
    private string _originalEndingTag;

    public void SetOriginalTags(string originalBeginning, string originalEnding)
    {
        _originalBeginningTag = originalBeginning;
        _originalEndingTag = originalEnding;
    }

    public string CompleteContent
    {
        get { return BeginningTag + Content + EndingTag; }
    }

    public int Length
    {
        get { return Content.Length; }
    }
    public int TotalLength
    {
        get { return BeginningTag.Length + Length + EndingTag.Length; }
    }

    public void Max()
    {
        StartIndex = Int32.MaxValue;
        EndIndex = Int32.MaxValue;
    }

    public bool ContainsStart()
    {
        return _startIndex > -1 && ((_originalBeginningTag != null) ? _originalBeginningTag == _beginningTag : true);
    }
    public bool ContainsEnd()
    {
        return _endIndex > -1 && ((_originalEndingTag != null) ? _originalEndingTag == _endingTag : true);
    }

    public int BeginningOfStartIndex
    {
        get { return (ContainsStart()) ? StartIndex - BeginningTag.Length : 0; }
    }
    public int BeginningOfEndIndex
    {
        get { return EndIndex; }
    }

    public int CloseOfStartIndex
    {
        get { return StartIndex; }
    }
    public int CloseOfEndIndex
    {
        get { return EndIndex + EndingTag.Length; }
    }

    public override string ToString()
    {
        return Content;
    }

    public void ChangeContentWhileRetainingPlacementInformation(string newContent)
    {
        Content = newContent;
    }
}