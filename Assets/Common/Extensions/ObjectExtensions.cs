﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class ObjectExtensions
{
    /// <summary>
    /// Returns TRUE if an objects value is <cref="System.null" /> and FALSE if the object has been initiated.
    /// </summary>
    /// <param name="o"></param>
    /// <returns></returns>
    public static bool IsNull(this object o)
    {
        return o == null;
    }

    public static bool IsNumeric(this object o)
    {
        if (o == null)
            return false;

        return o.ToString().IsNumber();
    }

    /// <summary>
    /// Based off of the Javascript truthy methods
    /// </summary>
    /// <param name="o"></param>
    /// <returns></returns>
    public static bool IsTruthy(this object o)
    {
        if (o == null)
            return false;

        if (o is bool)
            return (bool)o;

        if (o is string && string.IsNullOrEmpty((string)o))
            return false;

        if (o.IsNumeric() && o.Equals(0))
            return false;

        return true;
    }

    public static object ConvertToAny(this string input, Type target)
    {
        if (target == typeof(string))
            return input;
        // handle common types
        if (target == typeof(int))
            return int.Parse(input);
        if (target == typeof(double))
            return double.Parse(input);
        // handle enums
        if (target.BaseType == typeof(Enum))
            return Enum.Parse(target, input);
        // handle anything with a static Parse(string) function
        var parse = target.GetMethod("Parse", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, null, new[] { typeof(string) }, null);
        if (parse != null)
            return parse.Invoke(null, new object[] { input });
        // handle types with constructors that take a string
        var constructor = target.GetConstructor(new[] { typeof(string) });
        if (constructor != null)
            return constructor.Invoke(new object[] { input });
        else
            throw new ArgumentOutOfRangeException(string.Format("The input {0} could not be converted to type {1}", input, target.BaseType));
    }
    public static object ConvertAny<T>(this string input)
    {
        return ConvertToAny(input, typeof(T));
    }

    public static byte[] ToByteArray(this object obj)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (var ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    public static object ByteArrayToObject(this byte[] arrBytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);

            return obj;
        }
    }

    public static T ByteArrayToObject<T>(this byte[] arrBytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (T)binForm.Deserialize(memStream);

            return obj;
        }
    }
}