﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public static class IEnumerableExtensions
{
    public static int IndexOf<T>(this IEnumerable<T> list, Func<T, bool> func)
    {
        var s = -1; var i = 0;
        while (s == -1 && i < list.Count())
        {
            if (func(list.ElementAt(i)))
                s = i;
            else
                i++;
        }

        return s;
    }

    public static string Join(this IEnumerable<object> objs, string joiner = ", ")
    {
        if (objs is IEnumerable<string>)
        {
            var s = "";
            for (var i = 0; i < objs.Count(); i++)
            {
                s += ((string)objs.ElementAt(i)) + (i < objs.Count() - 1 ? joiner : "");
            }

            return s;
        }

        List<string> ls = new List<string>();

        foreach (var o in objs)
        {
            if (o == null)
                ls.Add("null");
            else
                ls.Add(o.ToString());
        }

        return ls.Join(joiner);
    }

    public static List<T> CloneIntList<T>(this List<T> intList)
    {
        var retArray = new T[intList.Count];
        for (var i = 0; i < intList.Count; i++)
        {
            retArray[i] = intList[i];
        }

        return retArray.ToList();
    }

    public static T[] ShuffleArray<T>(this T[] array, int seed)
    {
        var prng = new System.Random(seed);

        for (int i = 0; i < array.Length - 1; i++)
        {
            int randomIndex = prng.Next(i, array.Length);
            T tempItem = array[randomIndex];
            array[randomIndex] = array[i];
            array[i] = tempItem;
        }

        return array;
    }
}
