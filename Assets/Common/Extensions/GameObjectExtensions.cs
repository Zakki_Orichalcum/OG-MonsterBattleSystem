﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GameObjectExtensions
{
    public static T AddChildComponent<T>(this GameObject obj) where T : MonoBehaviour
    {
        GameObject child = new GameObject(typeof(T).Name);
        child.transform.SetParent(obj.transform);

        return child.AddComponent<T>();
    }

    public static void Destroy(this GameObject obj)
    {
        //AddToPoolingLater

        GameObject.DestroyImmediate(obj);
    }

    public static void DestroyChildren(this GameObject parent)
    {
        parent.transform.DestroyChildren();
    }

    public static void DestroyChildren(this Transform parent)
    {
        var children = new List<GameObject>();
        foreach (Transform child in parent) { children.Add(child.gameObject); }
        children.ForEach(x => Destroy(x));
    }
}

public static class Log
{
    public static void Write(params object[] objs)
    {
        Debug.Log(objs.Join());
    }
}
