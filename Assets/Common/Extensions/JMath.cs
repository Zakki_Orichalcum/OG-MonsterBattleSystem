﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

public static class IntExtension
{
    public static int Clamp(this int i, int min, int max)
    {
        if (i < min)
            return min;
        else if (i > max)
            return max;
        else
            return i;
    }

    /// <summary>
    /// Executes a delegate method the specified number of times
    /// </summary>
    /// <param name="i">the number of times to execute the delegate</param>
    /// <param name="function">the delegate to execute</param>
    public static void Times(this int i, Action function)
    {
        for (int ii = 0; ii <= i - 1; ii++)
        {
            function();
        }
    }

    /// <summary>
    /// Executes a delegate method the specified number of times and passes the iteration counter to the delegate
    /// </summary>
    /// <param name="i">the number of times to execute the delegate</param>
    /// <param name="function">the delegate to execute</param>
    public static void Times(this int i, Action<int> function)
    {
        for (int ii = 0; ii <= i - 1; ii++)
        {
            function(ii);
        }
    }

    /// <summary>
    /// Converts a number to it's comma seperated representation
    /// </summary>
    /// <param name="i">the number to convert</param>
    /// <returns>a System.String containing the comma seperated representation</returns>
    public static string Commaify(this int i)
    {
        string number = i.ToString();
        string natLangNum = String.Empty;
        string numberPieces = String.Empty;
        int backPos = 0, counter = 1;
        for (int ii = 0; ii <= number.Length - 1; ii++)
        {
            backPos = number.Length - ii;

            numberPieces = number.Substring(backPos - 1, 1) + numberPieces;

            if (counter % 3 == 0)
            {
                natLangNum += "," + numberPieces;
                numberPieces = String.Empty;
                counter = 1;
            }
            else
            {
                counter++;
            }
        }

        if (numberPieces != String.Empty)
            natLangNum += "," + numberPieces;

        return natLangNum.Substring(1);
    }

    /// <summary>
    /// Converts a number to it's natural language equivelent
    /// </summary>
    /// <param name="i">the number to convert</param>
    /// <returns>a System.String containing the converted representation</returns>
    public static string ToNaturalLanguage(this int i)
    {
        if (i == 0) return "zero";

        string[] int2str1 = new string[] { String.Empty, "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
        string[] int2str2 = new string[] { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        string[] int2str3 = new string[] { String.Empty, String.Empty, "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
        string[] int2str4 = new string[] { "hundred", "thousand", "million", "billion", "trillion", "quadrillion" };

        string natLangNum = String.Empty;

        if (i <= int2str1.Length - 1) return int2str1[i];
        if (i > 9 && i < 20) return int2str2[i - 10];

        string number = i.ToString();
        string[] Arr = i.Commaify().Split(',');

        for (int ii = Arr.Length - 1; ii >= 0; --ii)
        {
            string result = String.Empty, piece = Arr[ii], suffix = int2str4[Arr.Length];

            if (piece.Length == 3 && Arr.Length >= 1)
            {
                int firstPos = piece.Substring(0, 1).InterpretAsInteger(); // Convert.ToInt32(piece.Substring(0, 1));
                int secondPos = piece.Substring(1, 1).InterpretAsInteger(); // Convert.ToInt32(piece.Substring(1, 1));
                int thirdPos = piece.Substring(2, 1).InterpretAsInteger(); // Convert.ToInt32(piece.Substring(2, 1));
                int secondthirdPos = piece.Substring(1, 2).InterpretAsInteger(); // Convert.ToInt32(piece.Substring(1, 2));

                string firstPosStr = firstPos == 0 ? String.Empty : int2str1[firstPos] + " " + int2str4[0];
                string secondPosStr = secondPos == 0 ? String.Empty : int2str1[secondPos] == String.Empty ? int2str2[(secondthirdPos < 11 ? secondthirdPos + 10 : secondthirdPos) - 10] : int2str3[secondPos];
                string thirdPosStr = thirdPos == 0 ? String.Empty : int2str1[secondPos] == String.Empty ? String.Empty : "-" + int2str1[thirdPos];
                result = firstPosStr.Trim() + (secondPosStr != String.Empty ? " " : String.Empty) + secondPosStr.Trim() + thirdPosStr.Trim() + " " + (ii == 0 ? String.Empty : int2str4[ii].Trim()) + " ";
            }
            else if (piece.Length < 3 && Arr.Length > 1 && ii > 0)
            {
                int firstPos = Convert.ToInt32(piece);
                string firstPosStr = firstPos < 10 ? int2str1[firstPos] : firstPos > 19 ? int2str3[firstPos / 10] : int2str2[firstPos - 10];
                result = firstPosStr.Trim() + " " + int2str4[ii].Trim() + " ";
            }

            natLangNum += result;
        }

        return natLangNum.Trim();
    }

    /// <summary>
    /// Generates a TimeSpan representing a set number of Minutes
    /// </summary>
    /// <param name="i">the number of minutes to set in the timespan</param>
    /// <returns>a System.TimeSpan object</returns>
    public static TimeSpan Minutes(this int i)
    {
        return new TimeSpan(0, i, 0);
    }

    /// <summary>
    /// Generates a TimeSpan representing a set number of Hours
    /// </summary>
    /// <param name="i">the number of hours to set in the timespan</param>
    /// <returns>a System.TimeSpan object</returns>
    public static TimeSpan Hours(this int i)
    {
        return new TimeSpan(i, 0, 0);
    }

    /// <summary>
    /// Generates a TimeSpan representing a set number of Seconds
    /// </summary>
    /// <param name="i">the number of seconds to set in the timespan</param>
    /// <returns>a System.TimeSpan object</returns>
    public static TimeSpan Seconds(this int i)
    {
        return new TimeSpan(0, 0, i);
    }

    /// <summary>
    /// Generates a TimeSpan representing a set number of Days
    /// </summary>
    /// <param name="i">the number of days to set in the timespan</param>
    /// <returns>a System.TimeSpan object</returns>
    public static TimeSpan Days(this int i)
    {
        return new TimeSpan(i, 0, 0, 0);
    }

    public static string ConvertIntToHex(this int number)
    {
        if (number < 16)
        {
            switch (number)
            {
                case (10):
                    return "A";
                case (11):
                    return "B";
                case (12):
                    return "C";
                case (13):
                    return "D";
                case (14):
                    return "E";
                case (15):
                    return "F";
                default:
                    return "" + number;
            }
        }
        else
        {
            return ConvertIntToHex(number / 16) + ConvertIntToHex(number % 16);
        }
    }

    public static int ConvertHexToInt(this string hex)
    {
        var hexArr = hex.ToCharArray();

        var output = 0;
        for (var i = 0; i < hexArr.Length; i++)
        {
            var unitPlace = (hexArr.Length - 1) - i;
            output += ChangeSingleHexToNumber("" + hexArr[i]) * (16 * unitPlace);
        }

        return output;
    }

    private static int ChangeSingleHexToNumber(this string hex)
    {
        switch (hex)
        {
            case ("F"):
                return 15;
            case ("E"):
                return 14;
            case ("D"):
                return 13;
            case ("C"):
                return 12;
            case ("B"):
                return 11;
            case ("A"):
                return 10;
            default:
                return int.Parse(hex);
        }
    }
}

/// <summary>
/// Timespan extension methods
/// </summary>
public static class TimespanExt
{
    /// <summary>
    /// Determines a date of time from a timespan
    /// </summary>
    /// <param name="val">the target timespan</param>
    /// <example>
    /// TimeSpan.FromHours(2.0).Ago(); //two hours in the past
    /// </example>
    /// <returns>a datetime object</returns>
    public static DateTime Ago(this TimeSpan val)
    {
        return DateTime.Now.Subtract(val);
    }

    /// <summary>
    /// Determines a date of time from a timespan
    /// </summary>
    /// <param name="val">the target timespan</param>
    /// <example>
    /// TimeSpan.FromHours(2.0).FromNow(); //two hours in the future
    /// </example>
    /// <returns>a datetime object</returns>
    public static DateTime FromNow(this TimeSpan val)
    {
        return DateTime.Now.Add(val);
    }

    public static DateTime SafeDate(this string stringIn, DateTime defaultVal)
    {
        DateTime outVal;
        return (DateTime.TryParse(stringIn, out outVal)) ? outVal : defaultVal;
    }
    public static DateTime? SafeDateNullable(this string stringIn)
    {
        DateTime outVal;
        return (DateTime.TryParse(stringIn, out outVal)) ? (DateTime?)outVal : null;
    }
    public static string UserFriendlyTime(this decimal totalSeconds, bool shortLabels = false)
    {
        return UserFriendlyTime((int)Math.Round(totalSeconds, 0), shortLabels);
    }
    public static string UserFriendlyTime(this int totalSeconds, bool shortLabels = false)
    {
        var timeSpan = TimeSpan.FromSeconds(totalSeconds);

        var days = timeSpan.Days;
        var hours = timeSpan.Hours;
        var minutes = timeSpan.Minutes;
        var seconds = timeSpan.Seconds;

        var sb = new StringBuilder();
        if (!shortLabels)
        {
            if (days > 0)
                sb.AppendFormat("{0} day{1}, ", days, (days > 1) ? "s" : "");
            if (hours > 0)
                sb.AppendFormat("{0} hour{1}, ", hours, (hours > 1) ? "s" : "");
            if (minutes > 0)
                sb.AppendFormat("{0} minute{1}{2}", minutes, (minutes > 1) ? "s" : "", (days == 0) ? ", " : "");
            if (days == 0)
                sb.AppendFormat("{0} second{1}", seconds, (seconds > 1) ? "s" : "");
        }
        else
        {
            if (days > 0)
                sb.AppendFormat("{0}d, ", days);
            if (hours > 0)
                sb.AppendFormat("{0}h, ", hours);
            if (minutes > 0)
                sb.AppendFormat("{0}m{1}", minutes, (days == 0) ? ", " : "");
            if (days == 0)
                sb.AppendFormat("{0}s", seconds);
        }

        return sb.ToString();
    }

    public static int TimeUnitToMinutesMultiplier(this string unit)
    {
        return (unit == "h") ? 60 : (unit == "d") ? 1440 : (unit == "w") ? 10080 : 1;
    }
}

public struct MinMax
{
    public float Min { get; set; }
    public float Max { get; set; }

    public MinMax(float min, float max)
    {
        this.Min = min;
        this.Max = max;
    }
}