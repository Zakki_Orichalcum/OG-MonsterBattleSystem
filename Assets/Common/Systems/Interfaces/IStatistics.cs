using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OrichalcumGames.Statistics
{
    public interface IStatistics
    {
        GameObject GameObject { get; }

        StatInformation GetStat(string name);
        int GetStatId(string name);
        string GetStatName(int statId);

        int this[string name] { get; set; }
        int GetValue(string name);
        void SetValue(string name, int value);
    }

    [System.Serializable]
    public class StatInformation
    {
        public string Name;
        public bool HasMinimun;
        public int MinimumValue;
        public bool HasMaximum;
        public int MaximumValue;
        public bool HasNonstandardStages;

        public override string ToString()
        {
            return Name;
        }
    }
}
