﻿using UnityEngine;
using System.Collections;

public interface IJSON
{
    void InitializeFromJSON(JSONNode json);
    JSONNode ToJSON();
}
