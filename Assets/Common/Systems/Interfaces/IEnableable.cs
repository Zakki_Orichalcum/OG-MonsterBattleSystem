﻿using UnityEngine;
using System.Collections;

public interface IEnableable
{
    void Enable();
    void Disable();
}
