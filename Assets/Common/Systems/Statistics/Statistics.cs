﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace OrichalcumGames.Statistics
{
    public abstract class Statistics : MonoBehaviour, IStatistics, IEnableable, IJSON
    {
        public GameObject GameObject => gameObject;

        public StatInformation[] Stats { get; set; }

        protected int[] _data;
        protected static Dictionary<string, string> _willChangeNotifications = new Dictionary<string, string>();
        protected static Dictionary<string, string> _didChangeNotifications = new Dictionary<string, string>();

        public const string StatisticsWillChangeNotification = "Statistics.{0}WillChange";
        public const string StatisticsDidChangeNotification = "Statistics.{0}DidChange";

        public StatInformation GetStat(string name) => Stats.FirstOrDefault(x => x.Name.Equals(name));
        public int GetStatId(string name) => Stats.IndexOf(x => x.Name.Equals(name));
        public string GetStatName(int statId) => Stats[statId].Name;

        public int this[string s]
        {
            get { return _data[GetStatId(s)]; }
            set { SetValue(s, value, true); }
        }

        public void Awake()
        {
            OnAwake();
        }

        public virtual void OnAwake()
        {
            _data = new int[Stats.Length];
        }

        public static string WillChangeNotification(string type)
        {
            if (!_willChangeNotifications.ContainsKey(type))
                _willChangeNotifications.Add(type, StatisticsWillChangeNotification.Format(type));

            return _willChangeNotifications[type];
        }

        public static string DidChangeNotification(string type)
        {
            if (!_didChangeNotifications.ContainsKey(type))
                _didChangeNotifications.Add(type, StatisticsDidChangeNotification.Format(type));

            return _didChangeNotifications[type];
        }

        public void InitializeValues()
        {
            for(var i = 0; i < Stats.Length; i++)
            {
                var st = Stats[i];
                _data[i] = st.HasMinimun ? st.MinimumValue : 0;
            }
        }

        public int GetValue(string name)
        {
            return _data[GetStatId(name)];
        }

        public void SetValue(string name, int value, bool allowExceptions)
        {
            var id = GetStatId(name);

            int oldValue = _data[id];
            if (oldValue == value)
                return;

            if (allowExceptions)
            {
                //Allow exceptions to the rule here
                ValueChangeException ex = new ValueChangeException(id, oldValue, value);

                // the Notification is unique per stat type
                this.PostNotification(WillChangeNotification(name), ex);

                // Did anything modify the value?
                value = Mathf.FloorToInt(ex.GetModifiedValue());

                if (ex.Toggle == false || value == oldValue)
                    return;
            }

            _data[id] = value;
            this.PostNotification(DidChangeNotification(name),
                new DidChangeArgs
                {
                    StatId = id,
                    StatName = name,
                    OldValue = oldValue,
                    NewValue = value
                });
        }

        public void SetValue(string type, int value)
        {
            var id = GetStatId(name);

            int oldValue = _data[id];
            if (oldValue == value)
                return;

            _data[id] = value;
            this.PostNotification(DidChangeNotification(type), new DidChangeArgs
            {
                StatId = id,
                OldValue = oldValue,
                NewValue = value
            });
        }

        public void OnEnable()
        {
            Enable();
        }

        public void OnDisable()
        {
            Disable();
        }

        public virtual void Disable(){ }

        public virtual void Enable(){ }

        public virtual JSONNode ToJSON()
        {
            var output = new JSONClass();

            for(var i =0; i < Stats.Length; i++)
            {
                var j = Stats[i];
                output.Add(j.Name, new JSONData(this[j.Name]));
            }
            

            return output;
        }

        public virtual void InitializeFromJSON(JSONNode json)
        {
            var jObj = json as JSONClass;

            foreach(var i in jObj.Keys)
            {
                this[i] = jObj[i].AsInt;
            }
        }
    }

    public class DidChangeArgs
    {
        public int StatId;
        public string StatName;
        public int OldValue;
        public int NewValue;
    }
}
