﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class BasicStatus : Statistics
{
    public new StatInformation[] Stats;

    void OnEnable()
    {
        foreach (var s in Stats)
        {
            if (s.HasMaximum || s.HasMinimun)
                this.AddObserver(OnStatException, Statistics.WillChangeNotification(s.Name));
        }
    }
    void OnDisable()
    {
        foreach (var s in Stats)
        {
            if (s.HasMaximum || s.HasMinimun)
                this.RemoveObserver(OnStatException, Statistics.WillChangeNotification(s.Name));
        }
    }

    void OnStatException(object sender, object args)
    {
        GameObject actor = (sender as Statistics).gameObject;
        ValueChangeException vce = args as ValueChangeException;

        var stat = Stats[vce.StatId];
        if (stat.HasMaximum && vce.toValue > stat.MaximumValue)
        {
            vce.AddModifier(new MaxValueModifier(int.MaxValue, stat.MaximumValue));
        }
        if (stat.HasMinimun && vce.toValue < stat.MinimumValue)
        {
            vce.AddModifier(new MinValueModifier(int.MaxValue, stat.MinimumValue));
        }
    }
}
