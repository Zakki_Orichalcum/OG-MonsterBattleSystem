﻿using UnityEngine;
using System.Collections;

namespace OrichalcumGames.Statistics
{
    public interface IHealth
    {
        int HP { get; set; }
        int MHP { get; set; }

        void Reset();
    }

    public abstract class Health : MonoBehaviour, IHealth, IEnableable
    {
        #region Fields

        public int HP
        {
            get { return stats[HealthStatName]; }
            set { stats[HealthStatName] = value; }
        }

        public int MHP
        {
            get { return stats[MaximumHealthName]; }
            set { stats[MaximumHealthName] = value; }
        }

        public string MaximumHealthName => $"Maximum{HealthStatName}";
        public string HealthStatName;
        public int MinHP = 0;
        IStatistics stats;

        #endregion

        #region MonoBehaviour

        void Awake()
        {
            stats = GetComponent<IStatistics>();
        }

        void OnEnable()
        {
            Enable();
        }

        void OnDisable()
        {
            Disable();
        }


        public void Enable()
        {
            this.AddObserver(OnHPWillChange, Statistics.WillChangeNotification(HealthStatName), stats);
            this.AddObserver(OnHPDidChange, Statistics.DidChangeNotification(HealthStatName), stats);
            this.AddObserver(OnMHPDidChange, Statistics.DidChangeNotification(MaximumHealthName), stats);
        }

        public void Disable()
        {
            this.RemoveObserver(OnHPWillChange, Statistics.WillChangeNotification(HealthStatName), stats);
            this.RemoveObserver(OnHPDidChange, Statistics.DidChangeNotification(HealthStatName), stats);
            this.RemoveObserver(OnMHPDidChange, Statistics.DidChangeNotification(MaximumHealthName), stats);
        }

        #endregion

        #region Event Handlers

        protected virtual void OnHPWillChange(object sender, object args)
        {
            ValueChangeException vce = args as ValueChangeException;
            vce.AddModifier(new ClampValueModifier(int.MaxValue, MinHP, stats[MaximumHealthName]));
        }

        protected virtual void OnHPDidChange(object sender, object args)
        {
            var stats = sender as IStatistics;
        }

        void OnMHPDidChange(object sender, object args)
        {
            DidChangeArgs s = args as DidChangeArgs;
            int oldMHP = s.OldValue;
            if (MHP > oldMHP)
                HP += MHP - oldMHP;
            else
                HP = Mathf.Clamp(HP, MinHP, MHP);
        }

        #endregion

        public void Reset()
        {
            HP = MHP;
        }
    }
}