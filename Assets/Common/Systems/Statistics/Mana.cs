﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

namespace OrichalcumGames.Statistics
{
    public interface IMana
    {
        int MP { get; set; }
        int MMP { get; set; }

        void Reset();
    }

    public abstract class Mana : MonoBehaviour, IMana, IEnableable
    {
        #region Fields

        public int MP
        {
            get { return _stats[ManaStatName]; }
            set { _stats[ManaStatName] = value; }
        }

        public int MMP
        {
            get { return _stats[MaximumManaName]; }
            set { _stats[MaximumManaName] = value; }
        }

        public string MaximumManaName => $"Maximum{ManaStatName}";
        public string ManaStatName;
        public int MinMP = 0;
        
        private IStatistics _stats;
        #endregion

        #region MonoBehaviour
        void Awake()
        {
            _stats = GetComponent<IStatistics>();
        }

        void OnEnable()
        {
            Enable();
        }

        void OnDisable()
        {
            Disable();
        }

        public void Enable()
        {
            this.AddObserver(OnMPWillChange, Statistics.WillChangeNotification(ManaStatName), _stats);
            this.AddObserver(OnMMPDidChange, Statistics.DidChangeNotification(MaximumManaName), _stats);
        }

        public void Disable()
        {
            this.RemoveObserver(OnMPWillChange, Statistics.WillChangeNotification(ManaStatName), _stats);
            this.RemoveObserver(OnMMPDidChange, Statistics.DidChangeNotification(MaximumManaName), _stats);
        }
        #endregion

        #region Event Handlers
        void OnMPWillChange(object sender, object args)
        {
            ValueChangeException vce = args as ValueChangeException;
            vce.AddModifier(new ClampValueModifier(int.MaxValue, MinMP, _stats[MaximumManaName]));
        }

        void OnMMPDidChange(object sender, object args)
        {
            DidChangeArgs s = args as DidChangeArgs;
            int oldMMP = s.OldValue;
            if (MMP > oldMMP)
                MP += MMP - oldMMP;
            else
                MP = Mathf.Clamp(MP, MinMP, MMP);
        }

        void OnTurnBegan(object sender, object args)
        {
            if (MP < MMP)
                MP += Mathf.Max(Mathf.FloorToInt(MMP * 0.1f), 1);
        }
        #endregion

        public void Reset()
        {
            MP = MMP;
        }
    }
}