﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

namespace OrichalcumGames.Statistics
{
    public interface ILevel
    {
        int LVL { get; }
        int EXP { get; set; }

        float PercentageToNextLevel { get; }
        int ExperiencePointsToNextLevel { get; }
        int CurrentLevelExperience { get; }

        void AdjustStats();
    }

    public abstract class Level : MonoBehaviour, ILevel, IEnableable
    {
        #region Constants
        public const int MIN_LEVEL = 1;
        public const int MAX_LEVEL = 100;
        public const int MAX_EXPERIENCE = 999999;
        #endregion

        #region Fields / Properties
        public string LevelStat;
        public string ExperienceStat;

        private Statistics stats;
        private Function levelFunct;

        public int LVL
        {
            get { return stats[LevelStat]; }
        }

        public int EXP
        {
            get { return stats[ExperienceStat]; }
            set { stats[ExperienceStat] = value; }
        }

        public float LevelPercent
        {
            get { return (float)(LVL - MIN_LEVEL) / (float)(MAX_LEVEL - MIN_LEVEL); }
        }

        public float PercentageToNextLevel
        {
            get { return (float)(EXP - ExperienceForLevel(LVL)) / (float)(ExperienceForLevel(LVL + 1) - ExperienceForLevel(LVL)); }
        }

        public int CurrentLevelExperience
        {
            get { return EXP - ExperienceForLevel(LVL); }
        }

        public int ExperiencePointsToNextLevel
        {
            get { return ExperienceForLevel(LVL + 1) - ExperienceForLevel(LVL); }
        }

        #endregion


        #region MonoBehaviour
        void Awake()
        {
            stats = GetComponent<Statistics>();
        }

        void OnEnable()
        {
            Enable();
        }

        void OnDisable()
        {
            Disable();
        }

        public virtual void Enable()
        {
            this.AddObserver(OnExpWillChange, Statistics.WillChangeNotification(ExperienceStat), stats);
            this.AddObserver(OnExpDidChange, Statistics.DidChangeNotification(ExperienceStat), stats);
            this.AddObserver(OnLvlDidChange, Statistics.DidChangeNotification(LevelStat), stats);
        }

        public virtual void Disable()
        {
            this.RemoveObserver(OnExpWillChange, Statistics.WillChangeNotification(ExperienceStat), stats);
            this.RemoveObserver(OnExpDidChange, Statistics.DidChangeNotification(ExperienceStat), stats);
            this.RemoveObserver(OnLvlDidChange, Statistics.DidChangeNotification(LevelStat), stats);
        }

        #endregion

        #region Event Handlers
        void OnExpWillChange(object sender, object args)
        {
            ValueChangeException vce = args as ValueChangeException;
            vce.AddModifier(new ClampValueModifier(int.MaxValue, EXP, MAX_EXPERIENCE));
        }

        void OnExpDidChange(object sender, object args)
        {
            stats.SetValue(LevelStat, LevelForExperience(EXP), false);
        }

        void OnLvlDidChange(object sender, object args)
        {
            AdjustStats();
        }

        #endregion

        #region Public
        public int ExperienceForLevel(int level)
        {
            return levelFunct.Invoke(level);
        }

        public static int ExperienceForLevel(int level, string expGainType)
        {
            var func = AssignExperienceFunction(expGainType);
            return func(level);
        }

        public int LevelForExperience(int exp)
        {
            int lvl = MAX_LEVEL;
            for (; lvl >= MIN_LEVEL; --lvl)
                if (exp >= ExperienceForLevel(lvl))
                    break;
            return lvl;
        }

        public void Init(int level, int experience)
        {
            stats.SetValue(LevelStat, level, false);
            stats.SetValue(ExperienceStat, experience, false);
            AdjustStats();
        }

        public static Func<int, int> AssignExperienceFunction(string experienceGainFunctionName)
        {
            switch (experienceGainFunctionName)
            {
                case "Erratic":
                    return ErraticGain;
                case "Fast":
                    return FastGain;
                case "MediumSlow":
                    return MediumSlowGain;
                case "Slow":
                    return SlowGain;
                case "Fluctuating":
                    return FluctuatingGain;
                case "Medium":
                default:
                    return MediumGain;

            }
        }
        #endregion

        public abstract void AdjustStats();

        #region ExperienceFunctions

        public static int ErraticGain(int l)
        {
            if (l <= 50)
            {
                return Mathf.RoundToInt((Mathf.Pow(l, 3) * (100 - l)) / 50f);
            }
            else if (l <= 68)
            {
                return Mathf.RoundToInt((Mathf.Pow(l, 3) * (150 - l)) / 100f);
            }
            else if (l <= 98)
            {
                return Mathf.RoundToInt((Mathf.Pow(l, 3) * Mathf.Floor((1911f - (10f * l)) / 3)) / 500f);
            }
            else
            {
                return Mathf.RoundToInt((Mathf.Pow(l, 3) * (160 - l)) / 100f);
            }
        }

        public static int FastGain(int l)
        {
            return Mathf.RoundToInt(4 * Mathf.Pow(l, 3) / 5);
        }

        public static int MediumGain(int l)
        {
            return (int)Math.Pow(l, 3);
        }

        public static int MediumSlowGain(int l)
        {
            return Mathf.RoundToInt((4 * Mathf.Pow(l, 3) / 5) - (15 * Mathf.Pow(l, 2)) + (100 * l) - 140);
        }

        public static int SlowGain(int l)
        {
            return Mathf.RoundToInt(5 * Mathf.Pow(l, 3) / 4);
        }

        public static int FluctuatingGain(int l)
        {
            if (l <= 15)
            {
                return Mathf.RoundToInt(Mathf.Pow(l, 3) * ((Mathf.Floor((l + 1f) / 3) + 24) / 50f));
            }
            else if (l <= 36)
            {
                return Mathf.RoundToInt(Mathf.Pow(l, 3) * ((l + 14) / 50f));
            }
            else
            {
                return Mathf.RoundToInt(Mathf.Pow(l, 3) * ((Mathf.Floor(l / 2f) + 32) / 50f));
            }
        }

        #endregion
    }
}
