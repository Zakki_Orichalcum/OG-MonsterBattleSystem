﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnrichedCoroutine
{
    public Coroutine coroutine { get; private set; }
    public object result;
    private IEnumerator target;
    public EnrichedCoroutine(MonoBehaviour owner, IEnumerator target)
    {
        this.target = target;
        this.coroutine = owner.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        while (target.MoveNext())
        {
            result = target.Current;
            yield return result;
        }
    }

    public bool IsDone()
    {
        return !target.MoveNext();
    }
}

public class BatchCoroutineMonitor
{
    public List<EnrichedCoroutine> Targets { get; set; }

    public BatchCoroutineMonitor()
    {
        Targets = new List<EnrichedCoroutine>();
    }

    public bool IsDone()
    {
        var output = true;

        foreach(var i in Targets)
        {
            if (!i.IsDone())
                output = false;
        }

        return output;
    }

    public void Add(EnrichedCoroutine s)
    {
        Targets.Add(s);
    }
}