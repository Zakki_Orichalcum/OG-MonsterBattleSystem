﻿using UnityEngine;
using System.Collections;
using System;

public interface IFunction
{
    int Invoke(int input);
    float Invoke(float input);
}


public abstract class Function : MonoBehaviour, IFunction
{
    public abstract float Invoke(float input);

    public virtual int Invoke(int input)
    {
        return Mathf.RoundToInt(Invoke(input));
    }
}
