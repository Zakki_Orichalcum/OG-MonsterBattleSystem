﻿using UnityEngine;
using System.Collections;

public class BaseException
{

    public bool Toggle { get; private set; }
    public readonly bool DefaultToggle;

    public BaseException( bool defaultToggle)
    {
        this.DefaultToggle = defaultToggle;
        Toggle = defaultToggle;
    }

    public void FlipToggle()
    {
        Toggle = !DefaultToggle;
    }
}
