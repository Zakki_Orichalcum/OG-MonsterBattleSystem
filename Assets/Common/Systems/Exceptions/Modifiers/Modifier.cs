﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Modifier
{
    public readonly int SortOrder;

    public Modifier(int sortOrder)
    {
        SortOrder = sortOrder;
    }
}

public abstract class ValueModifier : Modifier
{
    public ValueModifier(int sortOrder) : base(sortOrder) { }
    public abstract float Modify(float fromValue, float toValue);
}

public class AddValueModifier : ValueModifier
{
    public readonly float ToAdd;

    public AddValueModifier(int sortOrder, float toAdd) : base(sortOrder)
    {
        ToAdd = toAdd;
    }

    public override float Modify(float fromValue, float toValue)
    {
        return toValue + ToAdd;
    }
}

public class ClampValueModifier : ValueModifier
{
    public readonly float Min;
    public readonly float Max;

    public ClampValueModifier(int sortOrder, float min, float max) : base(sortOrder)
    {
        Min = min;
        Max = max;
    }

    public override float Modify(float fromValue, float toValue)
    {
        return Mathf.Clamp(toValue, Min, Max);
    }
}

public class MinValueModifier : ValueModifier
{
    public float Min;
    public MinValueModifier(int sortOrder, float min) : base(sortOrder)
    {
        this.Min = min;
    }

    public override float Modify(float fromValue, float toValue)
    {
        return Mathf.Max(Min, toValue);
    }

}

public class MaxValueModifier : ValueModifier
{
    public float Max;
    public MaxValueModifier(int sortOrder, float max) : base(sortOrder)
    {
        this.Max = max;
    }

    public override float Modify(float fromValue, float toValue)
    {
        return Mathf.Min(Max, toValue);
    }

}

public class MultValueModifier : ValueModifier
{
    public readonly float ToMultiply;

    public MultValueModifier(int sortOrder, float toMultiply) : base(sortOrder)
    {
        this.ToMultiply = toMultiply;
    }

    public override float Modify(float fromValue, float toValue)
    {
        return toValue * ToMultiply;
    }
}

public class MultDeltaModifier : ValueModifier
{
    public readonly float ToMultiply;

    public MultDeltaModifier(int sortOrder, float toMultiply) : base(sortOrder)
    {
        this.ToMultiply = toMultiply;
    }

    public override float Modify(float fromValue, float toValue)
    {
        float delta = toValue - fromValue;

        return fromValue + delta * ToMultiply;
    }
}