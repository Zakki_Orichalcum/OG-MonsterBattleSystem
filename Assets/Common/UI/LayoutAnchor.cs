﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class LayoutAnchor : MonoBehaviour
{
    private RectTransform RT;
    private RectTransform ParentRT;

    private void Awake()
    {
        RT = transform as RectTransform;
        ParentRT = transform.parent as RectTransform;
        if(ParentRT == null)
        {
            Debug.LogError("This component require a RectTransform parent to work.");
        }
    }

    private Vector2 GetPosition(RectTransform rt, TextAnchor anchor)
    {
        Vector2 retValue = Vector2.zero;

        switch (anchor)
        {
            case TextAnchor.LowerCenter:
            case TextAnchor.MiddleCenter:
            case TextAnchor.UpperCenter:
                retValue.x += rt.rect.width * 0.5f;
                break;
            case TextAnchor.LowerRight:
            case TextAnchor.MiddleRight:
            case TextAnchor.UpperRight:
                retValue.x += rt.rect.width;
                break;
        }
        switch (anchor)
        {
            case TextAnchor.MiddleLeft:
            case TextAnchor.MiddleCenter:
            case TextAnchor.MiddleRight:
                retValue.y += rt.rect.height * 0.5f;
                break;
            case TextAnchor.UpperLeft:
            case TextAnchor.UpperCenter:
            case TextAnchor.UpperRight:
                retValue.y += rt.rect.height;
                break;
        }
        return retValue;
    }

    public Vector2 AnchorPosition(TextAnchor myAnchor, TextAnchor parentAnchor, Vector2 offset)
    {
        Vector2 myOffset = GetPosition(RT, myAnchor);
        Vector2 parentOffset = GetPosition(ParentRT, parentAnchor);
        Vector2 anchorCenter = new Vector2(Mathf.Lerp(RT.anchorMin.x, RT.anchorMax.x, RT.pivot.x), 
                                            Mathf.Lerp(RT.anchorMin.y, RT.anchorMax.y, RT.pivot.y));
        Vector2 myAnchorOffset = new Vector2(ParentRT.rect.width * anchorCenter.x, ParentRT.rect.height * anchorCenter.y);
        Vector2 myPivotOffset = new Vector2(RT.rect.width * RT.pivot.x, RT.rect.height * RT.pivot.y);
        Vector2 pos = parentOffset - myAnchorOffset - myOffset + myPivotOffset + offset;
        pos.x = Mathf.RoundToInt(pos.x);
        pos.y = Mathf.RoundToInt(pos.y);

        return pos;
    }

    public void SnapToAnchorPosition(TextAnchor myAnchor, TextAnchor parentAnchor, Vector2 offset)
    {
        RT.anchoredPosition = AnchorPosition(myAnchor, parentAnchor, offset);
    }

    public Tweener MoveToAnchorPosition(TextAnchor myAnchor, TextAnchor parentAnchor, Vector2 offset)
    {
        return RT.AnchorTo(AnchorPosition(myAnchor, parentAnchor, offset));
    }
}
