﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InformationCache : MonoBehaviour
{
    public Dictionary<string, object> ObjectCache = new Dictionary<string, object>();

    private static InformationCache informationCache;

    public static InformationCache Instance
    {
        get
        {
            if (!informationCache)
            {
                informationCache = FindObjectOfType(typeof(InformationCache)) as InformationCache;

                if (!informationCache)
                {
                    Debug.LogError("There needs to be one active InformationCache script on a GameObject in your scene.");
                }
            }

            return informationCache;
        }
    }

    public static bool ContainsKey(string key)
    {
        return Instance.ObjectCache.ContainsKey(key);
    }

    public static T Get<T>(string key)
    {
        if(Instance.ObjectCache.ContainsKey(key))
        {
            return (T)Instance.ObjectCache[key];
        }
        else
        {
            Debug.LogError($"The Cache contains no key under name {key}");
            return default(T);
        }
    }

    public static void Store<T>(string key, T obj)
    {
        if (Instance.ObjectCache.ContainsKey(key))
            Instance.ObjectCache[key] = obj;
        else
            Instance.ObjectCache.Add(key, obj);
    }
}
