﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour {

    public float Speed = 3f;
    public Transform Follow;
    private Transform _transform;
    public Vector3 FollowOffset;
    public Camera Camera;

    private Vector3 _originalCameraPosition;
    private Vector3 _originalCameraRotation;

    void Awake () {
        _transform = transform;
        _originalCameraPosition = transform.position;
        _originalCameraRotation = Camera.transform.rotation.eulerAngles;
	}
	
	void Update () {
        if (Follow)
        {
            _transform.position = Vector3.Lerp(_transform.position, Follow.position + FollowOffset, Speed * Time.deltaTime);
        }
	}

    public void ResetToDefaultPosition()
    {
        transform.position = _originalCameraPosition;
        Camera.transform.rotation = Quaternion.Euler(_originalCameraRotation);
    }

    public void SetPostion(Vector3 pos)
    {
        transform.position = pos;
    }

    public void LookAt(Vector3 pos)
    {
        Camera.transform.LookAt(pos);
    }

    public void LookAtTargetUnit(Unit u)
    {
        var pos = u.transform.position;
        pos.y += u.ModelHeight * 0.6f;
        SetPostion(new Vector3(pos.x + (pos.x > 0 ? -2 : 2), pos.y, pos.z + (pos.z > 0 ? 2 : -2)));
        LookAt(pos);
    }
}
