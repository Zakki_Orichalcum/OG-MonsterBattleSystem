using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterInformation
{
    public BattleType BattleType;
    public UnitInformation[] Units;
}
