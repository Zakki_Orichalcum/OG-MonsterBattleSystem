﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpeakerData {

    public List<string> Messages;
    public Sprite Speaker;
    public TextAnchor anchor;
}
