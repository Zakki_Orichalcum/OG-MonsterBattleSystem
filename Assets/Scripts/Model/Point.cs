﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// QR/Axial coordinate points
[System.Serializable]
public struct Point : IEquatable<Point>
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static Point operator +(Point a, Point b)
    {
        return new Point(a.x + b.x, a.y + b.y);
    }

    public static Point operator -(Point p1, Point p2)
    {
        return new Point(p1.x - p2.x, p1.y - p2.y);
    }

    public static bool operator ==(Point a, Point b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Point a, Point b)
    {
        return !(a == b);
    }

    public static implicit operator Vector2(Point p)
    {
        return new Vector2(p.x, p.y);
    }

    public override bool Equals(object obj)
    {
        if(obj is Point)
        {
            Point p = (Point)obj;
            return x == p.x && y == p.y;
        }

        return false;
    }

    public bool Equals(Point p)
    {
        return x == p.x && y == p.y;
    }

    public override int GetHashCode()
    {
        return x ^ y;
    }

    public override string ToString()
    {
        return string.Format("({0},{1})", x, y);
    }
}

// Cube coordinates
[System.Serializable]
public struct HexPoint : IEquatable<HexPoint>
{
    public int x;
    public int y;
    public int z;

    public HexPoint(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public HexPoint(float x, float y, float z)
    {
        this.x = Mathf.RoundToInt(x);
        this.y = Mathf.RoundToInt(y);
        this.z = Mathf.RoundToInt(z);
    }


    public static HexPoint operator +(HexPoint a, HexPoint b)
    {
        return new HexPoint(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static HexPoint operator -(HexPoint p1, HexPoint p2)
    {
        return new HexPoint(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
    }

    public static bool operator ==(HexPoint a, HexPoint b)
    {
        return a.Equals(b);
    }

    public static bool operator !=(HexPoint a, HexPoint b)
    {
        return !(a == b);
    }

    public static implicit operator Vector3(HexPoint p)
    {
        return new Vector3(p.x, p.y, p.z);
    }

    public override bool Equals(object obj)
    {
        if (obj is HexPoint)
        {
            HexPoint p = (HexPoint)obj;
            return x == p.x && y == p.y && z == p.z;
        }

        return false;
    }

    public bool Equals(HexPoint p)
    {
        return x == p.x && y == p.y && z == p.z;
    }

    public override int GetHashCode()
    {
        return x ^ y;
    }

    public override string ToString()
    {
        return string.Format("({0},{1},{2})", x, y, z);
    }

    public static (float x, float y, float z) RoundToHex(float x, float y, float z)
    {
        var rx = Mathf.Round(x);
        var ry = Mathf.Round(y);
        var rz = Mathf.Round(z);

        var x_diff = Mathf.Abs(rx - x);
        var y_diff = Mathf.Abs(ry - y);
        var z_diff = Mathf.Abs(rz - z);

        if (x_diff > y_diff && x_diff > z_diff)
            rx = -ry - rz;
        else if (y_diff > z_diff)
            ry = -rx - rz;
        else
            rz = -rx - ry;

        return (rx, ry, rz);
    }

    public static HexPoint HexRound(float x, float y, float z)
    {
        var r = RoundToHex(x, y, z);

        return new HexPoint(r.x, r.y, r.z);
    }

    public Vector2 ToVector2(float size = 1f)
    {
        var p = this.ToAxialPoint();

        var x = size * (3.0f / 2.0f) * p.x + 0;
        var y = size * (Mathf.Sqrt(3.0f) / 2.0f) * p.x + size * Mathf.Sqrt(3.0f) * p.y;

        return new Vector2(x, y);
    }

    public Vector3 ToVector3(float height = 0f, float size = 1f)
    {
        var v = ToVector2(size);

        return new Vector3(v.x, height, v.y);
    }

    public static (float x, float y, float z) FromTwoPoints(float x, float y, float size = 1f)
    {
        var q = (((2.0f/3.0f) * x) +                         0 ) / size;
        var r = (((1.0f/3.0f) * x) + ((Mathf.Sqrt(3.0f)/3) * y)) / size;

        var dx = q;
        var dy = -q - r;
        var dz = r;

        var max = Mathf.Max(Mathf.Abs(dx), Mathf.Abs(dy), Mathf.Abs(dz));
        var ratio = 1f / max;

        return (dx * ratio, dy * ratio, dz * ratio);
    }

    public static HexPoint FromPixelPoints(float x, float y, float size = 1f)
    {
        var d = FromTwoPoints(x, y, size);

        var hex = HexPoint.HexRound(d.x, d.y, d.z);

        return new HexPoint(hex.x.Clamp(-1, 1), hex.y.Clamp(-1, 1), hex.z.Clamp(-1, 1));
    }
}