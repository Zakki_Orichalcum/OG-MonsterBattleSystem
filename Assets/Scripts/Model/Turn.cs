﻿using OrichalcumGames.Statistics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn : IComparable<Turn>
{
    public Unit Actor;
    public ITurnAction Action;
    public List<BattlePlacement> Targets;
    public Player Player;

    public void SetTarget(BattlePlacement target)
    {
        Targets = new List<BattlePlacement>();
        Targets.Add(target);
    }

    public void SetTargets(IEnumerable<BattlePlacement> targets)
    {
        Targets = new List<BattlePlacement>();
        Targets.AddRange(targets);
    }

    public int CompareTo(Turn other)
    {
        var difference = Action.TurnActionPriority - other.Action.TurnActionPriority;

        if (difference != 0)
        {
            return -difference;
        }

        if (Action is SwitchTurnAction || Action is UseItemAction)
        {
            return Player is ComputerPlayer ? 1 : -1;
        }

        var a1 = (Ability)Action;
        var a2 = (Ability)other.Action;

        var p1 = a1.GetComponent<Priority>() != null ? a1.GetComponent<Priority>().Value : 0;
        var p2 = a2.GetComponent<Priority>() != null ? a2.GetComponent<Priority>().Value : 0;

        var priorityDiff = p1 - p2;

        if (priorityDiff != 0)
        {
            return -priorityDiff;
        }

        var speed1 = Actor.GetComponent<IStatistics>()[BattleController.Instance.TurnOrderComparisonStat];
        var speed2 = other.Actor.GetComponent<IStatistics>()[BattleController.Instance.TurnOrderComparisonStat];

        var spdDiff = speed1 - speed2;
        if (spdDiff != 0)
            return -spdDiff;

        //speed tie
        System.Random r = new System.Random();
        var rd = new int[] { -1, 1 };
        return rd[r.Next(2)];
    }
}
