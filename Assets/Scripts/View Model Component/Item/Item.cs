﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item : MonoBehaviour, IMenuItem
{
    public bool IsLocked => !IsConsumable;
    public bool IsConsumable => GetComponent<Consumable>();

    public string ImageName;
    public string Name;
    public int Quantity;
    public string Description;

    public string Category;

    public bool IsPlaying { get; set; }
    protected Animation Animation { get { return GetComponent<Animation>(); } }


    public List<BattlePlacement> Targets;
    public Unit Unit;

    public void PerformUse(Unit u, List<BattlePlacement> targets)
    {
        Targets = targets;
        Unit = u;
        StartCoroutine("PerformAction");
    }

    public const string BattleMessage1 = "{0}'s {1} uses";
    public const string BattleMessage2 = "{0}!";

    private IEnumerator PerformAction()
    {
        IsPlaying = true;
        var unit = GetComponentInParent<Unit>();

        BattleController.Instance.ShowBattleMessage(BattleMessage1.Format(unit.Player.Name, unit.name), BattleMessage2.Format(name));

        yield return new WaitForEndOfFrame();

        if (Animation != null)
        {
            Animation.ConnectionEvent += PerformAbility;
            Animation.Play();
            while (Animation.IsPlaying)
                yield return null;
            Animation.ConnectionEvent -= PerformAbility;
        }
        else
        {
           PerformAbility(Unit);
        }
            
        IsPlaying = false;
    }

    void PerformAbility(Unit u)
    {
        //for (int i = 0; i < transform.childCount; ++i)
        //{
        //    Transform child = transform.GetChild(i);
        //    BaseAbilityEffect effect = child.GetComponent<BaseAbilityEffect>();
        //    AbilityEffectTarget targeter = child.GetComponent<AbilityEffectTarget>();
        //    PercentChance pc = child.GetComponent<PercentChance>();
        //    var t = (targeter is SelfAbilityEffectTarget) ? GetComponentInParent<Unit>() : target;

        //    if (pc == null || pc.RollForHit(t))
        //    {
        //        effect.Apply(target);
        //    }
        //}
    }
}
