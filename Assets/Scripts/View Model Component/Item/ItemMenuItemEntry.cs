﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemMenuItemEntry : MenuItem
{
    public Image ItemIcon;
    public Text ItemName;
    public Text ItemQuantity;
    public Image Background;

    public Color SelectedColor;
    public Color SelectedTextColor;
    public Color UnuseableColor;
    public Color UnuseableTextColor;

    private Color UnselectedColor;
    private Color UnselectedTextColor;

    private void Awake()
    {
        UnselectedColor = Background.color;
        UnselectedTextColor = ItemName.color;
    }

    public override void Set(IMenuItem i)
    {
        var imi = i as Item;

        //TODO ItemImage
        ItemName.text = imi.Name;
        ItemQuantity.text = $"x{imi.Quantity}";
    }

    protected override void SetLocked()
    {
        Background.color = UnuseableColor;
        ItemName.color = UnuseableTextColor;
    }

    protected override void SetSelected()
    {
        Background.color = SelectedColor;
        ItemName.color = SelectedTextColor;
    }

    protected override void SetUnselected()
    {
        Background.color = UnselectedColor;
        ItemName.color = UnselectedTextColor;
    }
}