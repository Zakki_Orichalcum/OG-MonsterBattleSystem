﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class StatModifierFeature : Feature
{
    #region Fields / Properties
    public string type;
    public int amount;

    IStatistics stats
    {
        get
        {
            return _target.GetComponentInParent<IStatistics>();
        }
    }
    #endregion

    #region Protected
    protected override void OnApply()
    {
        stats[type] += amount;
    }

    protected override void OnRemove()
    {
        stats[type] -= amount;
    }
    #endregion
}
