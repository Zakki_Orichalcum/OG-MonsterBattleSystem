﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysFaceTheCamera : MonoBehaviour
{
    public Camera m_Camera;
    public bool Active = false;

    //Orient the camera after all movement is completed this frame to avoid jittering
    void LateUpdate()
    {
        if(Active)
            transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
                m_Camera.transform.rotation * Vector3.up);
    }
}
