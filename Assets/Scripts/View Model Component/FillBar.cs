﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillBar : MonoBehaviour
{
    public Image Bar;

    public Color BarHigh;
    public Color BarMedium;
    public Color BarLow;

    public float MediumCutOff;
    public float LowCutOff;

    private EasingControl ec;
    private float _startingPercent;
    private float _endingPercent;

    public void Awake()
    {
        ec = gameObject.AddComponent<EasingControl>();
        ec.equation = EasingEquations.EaseInCubic;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;
    }

    public void SetPercent(float percent, float time)
    {
        ec.duration = time;
        _startingPercent = Bar.fillAmount;
        _endingPercent = percent;
        ec.Play();
    }

    public void SetPercent(float percent)
    {
        Bar.fillAmount = percent;
        Bar.color = percent <= LowCutOff ? BarLow : percent <= MediumCutOff ? BarMedium : BarHigh;
    }

    void OnUpdateEvent(object sender, EventArgs e)
    {
        var math = _endingPercent + (_startingPercent - _endingPercent) * (1f - ec.currentValue);
        SetPercent(math);
    }
}
