﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileTemp : MonoBehaviour {

    public const float StepHeight = 0.25f;

    public HexPoint Pos;
    public int Height;
    public GameObject Content;
    public Vector3 Center {
        get
        {
            var axial = Pos.ToVector2();

            return new Vector3(axial.x, Height * StepHeight, axial.y);
        }
    }

    [HideInInspector] public TileTemp Prev;
    [HideInInspector] public int Distance;

    private void Match()
    {
        var v = Pos.ToVector2();
        transform.localPosition = new Vector3(v.x, Height * StepHeight / 2f, v.y);
        transform.localScale = new Vector3(1, Height * StepHeight, 1);
    }

    public void Grow()
    {
        Height++;
        Match();
    }

    public void Shrink()
    {
        Height--;
        Match();
    }

    public void Load(Point p, int h)
    {
        Pos = p.ToCubePoint();
        Height = h;
        Match();
    }

    public void Load(Vector3 v)
    {
        Load(new Point((int)v.x, (int)v.z), (int)v.y);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override string ToString()
    {
        return string.Format("Tile [ Pos : {0} ]", Pos);
    }

    public Vector3 ToVector3()
    {
        var v = Pos.ToVector2();
        return new Vector3(v.x, Height, v.y);
    }
}
