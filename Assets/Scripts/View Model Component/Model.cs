﻿using UnityEngine;
using System.Collections;

public class Model : MonoBehaviour
{
    public Animator Animator => GetComponent<Animator>();
    public GameObject ModelObject;
    public MeshRenderer Mesh => ModelObject.GetComponentInChildren<MeshRenderer>();
    public const float ModelHeightOffset = 0f;
    public float ModelHeight => 2f;

    public void SetModel(Transform t)
    {
        ModelObject = t.gameObject;
        t.SetParent(transform);
        t.localPosition = new Vector3(0, ModelHeightOffset, 0);
    }
}
