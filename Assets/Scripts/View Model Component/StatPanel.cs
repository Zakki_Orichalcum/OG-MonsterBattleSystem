﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using OrichalcumGames.Statistics;

public class StatPanel : MonoBehaviour, IStatPanel
{
    public Panel Panel { get; set; }
    public Image Background;
    public FillBar HealthBar;
    public FillBar ManaBar;
    public FillBar ExpBar;
    public Text NameLabel;
    public Text HpLabel;
    public Text MpLabel;
    public Text LvlLabel;

    private void Awake()
    {
        Panel = GetComponentInChildren<Panel>();
    }

    public void Display(GameObject obj)
    {
        NameLabel.text = obj.name;
        IHealth h = obj.GetComponent<IHealth>();
        IMana m = obj.GetComponent<IMana>();
        ILevel r = obj.GetComponent<ILevel>();

        HpLabel.text = $"{h.HP}/{h.MHP}";
        HealthBar.SetPercent(1f * h.HP / h.MHP);
        LvlLabel.text = $"{r.LVL}";
        if (m != null)
        {
            ManaBar.SetPercent(1f * m.MP / m.MMP);
            MpLabel.text = $"{m.MP}/{m.MMP}";
        }

        ExpBar.SetPercent(r.PercentageToNextLevel);
    }
    
}
