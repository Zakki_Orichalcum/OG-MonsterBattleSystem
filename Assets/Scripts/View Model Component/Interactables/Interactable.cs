﻿using UnityEngine;
using System.Collections;

public abstract class Interactable : MonoBehaviour
{
    public abstract void OnPossibleInteraction();
    public abstract void OnStopPossibleInteraction();
    public abstract void Interact();

}
