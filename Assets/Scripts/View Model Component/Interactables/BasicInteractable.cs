﻿using UnityEngine;
using System.Collections;
using System;

public class BasicInteractable : Interactable
{
    private EasingControl ec;
    [SerializeField]
    private GameObject Interactable;
    private Vector3 iStarting;
    private Vector3 iOffset = new Vector3(0, 1.5f, 0);

    public void Start()
    {
        iStarting = Interactable.transform.position;
        Interactable.SetActive(false);
        ec = gameObject.AddComponent<EasingControl>();
        ec.equation = EasingEquations.EaseInCubic;
        ec.duration = 0.5f;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;
    }

    private void OnUpdateEvent(object sender, EventArgs e)
    {
        Interactable.transform.position = iStarting + (iOffset * ec.currentValue);
    }

    public override void Interact()
    {
        //FieldController.Instance.StartEncounter(null);
    }

    public override void OnPossibleInteraction()
    {
        Interactable.SetActive(true);
        ec.Play();
    }

    public override void OnStopPossibleInteraction()
    {
        StartCoroutine(stopping());
    }

    IEnumerator stopping()
    {
        ec.Reverse();
        while (ec.IsPlaying)
            yield return null;

        Interactable.SetActive(false);
    }
}
