﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TopMenuEntry : MonoBehaviour
{
    public Text Name;
    public Image MainBackground;

    public RectTransform Rect => GetComponent<RectTransform>();

    public Color SelectedColor;
    public Color SelectedTextColor;
    public Color UnuseableColor;
    public Color UnuseableTextColor;
    private Color UnselectedColor;
    private Color UnselectedTextColor;

    [System.Flags]
    private enum States
    {
        None = 0,
        Selected = 1 << 0,
        Locked = 1 << 1
    }

    private void Awake()
    {
        UnselectedColor = MainBackground.color;
        UnselectedTextColor = Name.color;
    }

    public bool IsLocked
    {
        get { return (State & States.Locked) != States.None; }
        set
        {
            if (value)
                State |= States.Locked;
            else
                State &= ~States.Locked;
        }
    }

    public bool IsSelected
    {
        get { return (State & States.Selected) != States.None; }
        set
        {
            if (value)
                State |= States.Selected;
            else
                State &= ~States.Selected;
        }
    }

    private States _state;
    private States State
    {
        get { return _state; }
        set
        {
            if (_state == value)
                return;

            _state = value;

            if (IsLocked)
            {
                MainBackground.color = UnuseableColor;
                Name.color = UnuseableTextColor;
                //Outline.effectColor = new Color32(20, 36, 44, 255);
                Rect.localScale = Vector3.one * 0.85f;
            }
            else if (IsSelected)
            {
                MainBackground.color = SelectedColor;
                Name.color = SelectedTextColor;
                //Outline.effectColor = new Color32(255, 160, 72, 255);
                Rect.localScale = Vector3.one * 1.1f;
            }
            else
            {
                MainBackground.color = UnselectedColor;
                Name.color = UnselectedTextColor;
                //Outline.effectColor = new Color32(20, 36, 44, 255);
                Rect.localScale = Vector3.one;
            }
        }
    }

    public void Reset()
    {
        State = States.None;
    }
}