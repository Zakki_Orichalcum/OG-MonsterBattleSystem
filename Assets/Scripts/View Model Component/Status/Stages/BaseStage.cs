﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseStage : MonoBehaviour
{
    protected Unit _owner;
    public int Stages = 0;

    public const int MIN_MAX_NUMBER_OF_STAGES = 6;
    
    public string Stat { get; set; }

    public virtual void AddAdjustment(Info<Unit, Unit, List<ValueModifier>>info)
    {
        var stagesMod = 3 + Mathf.Abs(Stages);

        var s = (Stages < 0) ? 3.0f / stagesMod : stagesMod / 3.0f;

        MultValueModifier mod = new MultValueModifier(1, s);
        info.arg2.Add(mod);
    }

    public virtual int AddStages(int i)
    {
        Stages += i;

        if (Stages > MIN_MAX_NUMBER_OF_STAGES)
        {
            Stages = MIN_MAX_NUMBER_OF_STAGES;
            return 0;
        }
        else
            return 1;
    }

    public virtual int SubtractStages(int i)
    {
        Stages -= i;

        if (Stages < -MIN_MAX_NUMBER_OF_STAGES)
        {
            Stages = -MIN_MAX_NUMBER_OF_STAGES;
        return 0;
        }
        else
            return -1;
    }
}
