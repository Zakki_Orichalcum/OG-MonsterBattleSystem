﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NonstandardStage : BaseStage
{
    public override void AddAdjustment(Info<Unit, Unit, List<ValueModifier>> info)
    {
        var stagesMod = 3 + Mathf.Abs(Stages);

        var s = (Stages < 0) ? 3.0f / stagesMod : stagesMod / 3.0f;

        MultValueModifier mod = new MultValueModifier(1, s);
        info.arg2.Add(mod);
    }
}
