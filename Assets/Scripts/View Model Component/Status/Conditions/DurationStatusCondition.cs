﻿using UnityEngine;
using System.Collections;

public class DurationStatusCondition : StatusCondition
{
    public int Duration = 10;

    //private void OnEnable()
    //{
    //    this.AddObserver(OnNewTurn, TurnOrderController.RoundBeganNotification);
    //}

    //private void OnDisable()
    //{
    //    this.RemoveObserver(OnNewTurn, TurnOrderController.RoundBeganNotification);
    //}

    private void OnNewTurn(object sender, object args)
    {
        Duration--;
        if (Duration <= 0)
            Remove();
    }
}
