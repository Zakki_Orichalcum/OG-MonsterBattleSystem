﻿using UnityEngine;
using System;
using System.Collections;
using OrichalcumGames.Statistics;

public class StatComparisonCondition : StatusCondition
{
    #region Fields
    public string Type { get; private set; }
    public int Value { get; private set; }
    public Func<bool> Condition { get; private set; }
    private IStatistics _stats;
    #endregion

    #region MonoBehaviour
    void Awake()
    {
        _stats = GetComponentInParent<IStatistics>();
    }

    void OnDisable()
    {
        this.RemoveObserver(OnStatChanged, Statistics.DidChangeNotification(Type), _stats);
    }
    #endregion

    #region Public
    public void Init(string type, int value, Func<bool> condition)
    {
        this.Type = type;
        this.Value = value;
        this.Condition = condition;
        this.AddObserver(OnStatChanged, Statistics.DidChangeNotification(type), _stats);
    }

    public bool EqualTo()
    {
        return _stats[Type] == Value;
    }

    public bool LessThan()
    {
        return _stats[Type] < Value;
    }

    public bool LessThanOrEqualTo()
    {
        return _stats[Type] <= Value;
    }

    public bool GreaterThan()
    {
        return _stats[Type] > Value;
    }

    public bool GreaterThanOrEqualTo()
    {
        return _stats[Type] >= Value;
    }
    #endregion

    #region Notification Handlers
    void OnStatChanged(object sender, object args)
    {
        if (Condition != null && !Condition())
            Remove();
    }
    #endregion
}