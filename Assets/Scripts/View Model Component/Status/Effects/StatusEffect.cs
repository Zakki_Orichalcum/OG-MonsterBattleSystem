﻿using UnityEngine;
using System.Collections;

public abstract class StatusEffect : MonoBehaviour
{
    public abstract string StatusName { get; }
}
