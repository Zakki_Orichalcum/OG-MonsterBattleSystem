﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class PoisonStatusEffect : StatusEffect, IEndOfTurnTriggable
{
    public override string StatusName => "Poison";

    public int Priority => 11;

    private Unit _owner;

    private void OnEnable()
    {
        _owner = GetComponentInParent<Unit>();
        if (_owner)
            this.AddObserver(OnTrigger, EndOfTurnState.CanPerformCheck);

        var instance = BattleController.InstantiatePrefab("GenericAnimations/PoisonStatusAnimation");
        var s = instance.GetComponent<PoisonStatusAnimation>();
        BattleController.Instance.AddToAnimationQueue(s);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnTrigger, EndOfTurnState.CanPerformCheck);
    }

    void OnTrigger(object sender, object args)
    {
        EndOfTurnState s = (EndOfTurnState)sender;

        s.AddTrigger(new EndOfTurnTrigger {
            Action = this,
            Unit = _owner
        });
    }

    public IEnumerator Apply()
    {
        IHealth s = GetComponentInParent<IHealth>();

        yield return null;

        int currentHP = s.HP;
        int maxHP = s.MHP;
        int reduce = Mathf.Min(currentHP, Mathf.FloorToInt(maxHP * 0.1f));
        s.HP -= reduce;
    }

}
