﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HitSuccessIndicator : MonoBehaviour
{
    private const string ShowKey = "Show";
    private const string HideKey = "Hide";

    [SerializeField] private Canvas Canvas;
    [SerializeField] private Panel Panel;
    [SerializeField] private Image Arrow;
    [SerializeField] private Text Label;
    private Tweener transition;

    void Start()
    {
        Panel.SetPosition(HideKey, false);
        Canvas.gameObject.SetActive(false);
    }

    public void SetStats(int chance, int amount)
    {
        Arrow.fillAmount = (chance / 100f);
        Label.text = string.Format("{0}% {1}pt(s)", chance, amount);
    }

    public void Show()
    {
        Canvas.gameObject.SetActive(true);
        SetPanelPos(ShowKey);
    }

    public void Hide()
    {
        SetPanelPos(HideKey);
        transition.completedEvent += delegate (object sender, System.EventArgs e) {
            Canvas.gameObject.SetActive(false);
        };
    }

    void SetPanelPos(string pos)
    {
        if (transition != null && transition.IsPlaying)
            transition.Stop();

        transition = Panel.SetPosition(pos, true);
        transition.duration = 0.5f;
        transition.equation = EasingEquations.EaseInOutQuad;
    }
}