﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationPanel : MonoBehaviour
{
    public Text Message;
    public Image Speaker;
    public GameObject Arrow;
    public Panel Panel;

	// Use this for initialization
	void Start () {
        Vector3 pos = Arrow.transform.localPosition;
        Arrow.transform.localPosition = new Vector3(pos.x, pos.y + 5, pos.z);
        Tweener t = Arrow.transform.MoveToLocal(new Vector3(pos.x, pos.y - 5, pos.z));
        t.loopType = EasingControl.LoopType.PingPong;
        t.loopCount = -1;
	}

    public IEnumerator Display(SpeakerData sd)
    {
        Speaker.sprite = sd.Speaker;
        Speaker.SetNativeSize();

        for(int i = 0; i < sd.Messages.Count; ++i)
        {
            Message.text = sd.Messages[i];
            Arrow.SetActive(i + 1 < sd.Messages.Count);
            yield return null;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
