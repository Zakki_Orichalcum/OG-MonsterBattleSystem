﻿using UnityEngine;
using System.Collections;

public class PlanOfAttack
{
    public Ability Ability;
    public Targets Target;
    public HexPoint MoveLocation;
    public HexPoint FireLocation;
    public Directions AttackDirection;
}
