﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class ComputerPlayer : Player
{
    public BattleController owner;
    

    void Awake()
    {
        owner = GetComponent<BattleController>();
        Units = new List<Unit>();
        ParticipatingUnits = new List<Unit>();
    }

    public abstract Turn Evaluate(Unit u);

    private void DefaultAttackPattern(Turn t, Unit u)
    {
        // Just get the first "Attack" ability
        t.Action = u.GetComponentInChildren<Ability>();
        t.Actor = u;
        t.Targets = new List<BattlePlacement> { BattleController.Instance.BattlePlacements[0] };
    }

    public virtual SwitchTurnAction SwitchInNext()
    {
        var swi = new SwitchTurnAction();
        swi.Player = this;
        swi.SwitchFrom = 0;
        swi.SwitchTo = Units.FindIndex(x => x.GetComponentInChildren<KnockOutStatusEffect>() == null);

        return swi;
    }
}