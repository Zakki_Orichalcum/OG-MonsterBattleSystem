﻿using UnityEngine;
using System.Collections;

public class WildComputerPlayer : ComputerPlayer
{
    public override string Name => "Wild";
    public override string PossessiveName => Name;

    public override Turn Evaluate(Unit u)
    {
        var abCatalog = u.GetComponentInChildren<AbilityCatalog>();
        var random = Random.Range(0, abCatalog.AbilityCount());

        var turn = new Turn();
        turn.Action = abCatalog.GetAbility(random);
        turn.Actor = owner.Opponent.Units[0];
        turn.SetTarget(owner.BattlePlacements[1]);
        turn.Player = owner.Opponent;

        return turn;
    }
}
