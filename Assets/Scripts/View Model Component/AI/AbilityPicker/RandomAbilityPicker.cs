﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomAbilityPicker : BaseAbilityPicker
{
    public List<BaseAbilityPicker> Pickers;

    public override void Pick(PlanOfAttack plan)
    {
        int index = Random.Range(0, Pickers.Count);
        BaseAbilityPicker p = Pickers[index];
        p.Pick(plan);
    }
}