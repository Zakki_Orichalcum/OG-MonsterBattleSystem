﻿using UnityEngine;
using System.Collections;

public abstract class BaseAbilityPicker : MonoBehaviour
{
    #region Fields
    protected Unit Owner;
    protected AbilityCatalog AC;
    #endregion

    #region MonoBehaviour
    void Start()
    {
        Owner = GetComponentInParent<Unit>();
        AC = Owner.GetComponentInChildren<AbilityCatalog>();
    }
    #endregion

    #region Public
    public abstract void Pick(PlanOfAttack plan);
    #endregion

    #region Protected
    protected Ability Find(string abilityName)
    {
        for (int i = 0; i < AC.transform.childCount; ++i)
        {
            Transform category = AC.transform.GetChild(i);
            Transform child = category.Find(abilityName);
            if (child != null)
                return child.GetComponent<Ability>();
        }
        return null;
    }

    protected Ability Default()
    {
        return Owner.GetComponentInChildren<Ability>();
    }
    #endregion
}