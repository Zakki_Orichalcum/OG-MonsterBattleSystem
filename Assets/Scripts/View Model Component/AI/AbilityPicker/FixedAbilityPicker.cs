﻿using UnityEngine;
using System.Collections;
public class FixedAbilityPicker : BaseAbilityPicker
{
    public Targets Target;
    public string Ability;

    public override void Pick(PlanOfAttack plan)
    {
        plan.Target = Target;
        plan.Ability = Find(Ability);

        if (plan.Ability == null)
        {
            plan.Ability = Default();
            plan.Target = Targets.Foe;
        }
    }
}