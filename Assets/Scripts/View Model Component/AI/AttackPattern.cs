﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackPattern : MonoBehaviour
{
    public List<BaseAbilityPicker> Pickers;
    private int index;

    public void Pick(PlanOfAttack plan)
    {
        Pickers[index].Pick(plan);
        index++;
        if (index >= Pickers.Count)
            index = 0;
    }
}