﻿using UnityEngine;
using System.Collections;

public abstract class MenuItem : MonoBehaviour
{
    public abstract void Set(IMenuItem i);

    [System.Flags]
    private enum States
    {
        None = 0,
        Selected = 1 << 0,
        Locked = 1 << 1
    }

    public bool IsLocked
    {
        get { return (State & States.Locked) != States.None; }
        set
        {
            if (value)
                State |= States.Locked;
            else
                State &= ~States.Locked;
        }
    }

    public bool IsSelected
    {
        get { return (State & States.Selected) != States.None; }
        set
        {
            if (value)
                State |= States.Selected;
            else
                State &= ~States.Selected;
        }
    }

    private States _state;
    private States State
    {
        get { return _state; }
        set
        {
            if (_state == value)
                return;

            _state = value;

            if (IsLocked)
            {
                SetLocked();
            }
            else if (IsSelected)
            {
                SetSelected();
            }
            else
            {
                SetUnselected();
            }
        }
    }

    protected abstract void SetLocked();
    protected abstract void SetUnselected();
    protected abstract void SetSelected();

    public virtual void Reset()
    {
        State = States.None;
    }
}
