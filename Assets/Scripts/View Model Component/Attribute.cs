﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Collections.Specialized;

//public class Attribute //: OutsideResource
//{
//    public string Name;
//    public string[] Weaknesses;
//    public string[] Resistances;
//    public string[] Immunities;
//    public Color Color;

//    //public const string AttributePath = "{0}/Settings/MonsterGameData_Attributes.csv";
//    //public const string AttributeCacheKey = "Attributes";

//    //public static IEnumerator LoadAllResources()
//    //{
//    //    var continuing = true;
//    //    if (InformationCache.ContainsKey(AttributeCacheKey))
//    //    {
//    //        var o = InformationCache.Get<Dictionary<string, Attribute>>(AttributeCacheKey);
//    //        yield return o;
//    //        continuing = false;
//    //    }

//    //    if(continuing)
//    //    {
//    //        Dictionary<string, Attribute> resources = new Dictionary<string, Attribute>();
//    //        var webRequest = new WebRequest();
//    //        var url = "http://localhost:13522/api/attributes";
//    //        var headers = new NameValueCollection();

//    //        var ec = new EnrichedCoroutine(GameManager.Instance, webRequest.Get(url, headers));
//    //        yield return ec.coroutine;

//    //        var request = (string)ec.result;
//    //        var json = JSON.Parse(request);
//    //        var arr = json["attributes"].AsArray;
//    //        for (int i = 0; i < arr.Count; ++i)
//    //        {
//    //            var j = arr[i];
//    //            var t = ParseAttributes(j.AsObject);
//    //            resources.Add(t.Item1, t.Item2);
//    //        }

//    //        InformationCache.Store<Dictionary<string, Attribute>>(AttributeCacheKey, resources);

//    //        yield return resources;
//    //    }
//    //}

//    public static IEnumerator LoadResource(string key)
//    {
//        var ec = new EnrichedCoroutine(GameManager.Instance, LoadAllResources());

//        yield return ec.coroutine;

//        var dict = (Dictionary<string, Attribute>)ec.result;

//        yield return dict[key];
//    }

//    public static Attribute Get(string key)
//    {
//        if (InformationCache.ContainsKey(AttributeCacheKey))
//        {
//            var o = InformationCache.Get<Dictionary<string, Attribute>>(AttributeCacheKey);

//            return o[key];
//        }

//        throw new System.Exception("Trying to get Attribute before load!");
//    }

//    static (string, Attribute) ParseAttributes(JSONClass json)
//    {
//        Attribute obj = new Attribute();
//        var weaknesses = new List<string>();
//        var resistances = new List<string>();
//        var immunities = new List<string>();

//        foreach(JSONNode w in json["weaknesses"].AsArray)
//        {
//            weaknesses.Add(w.QuotelessValue);
//        }

//        foreach (JSONNode w in json["resistances"].AsArray)
//        {
//            weaknesses.Add(w.QuotelessValue);
//        }

//        foreach (JSONNode w in json["immunities"].AsArray)
//        {
//            weaknesses.Add(w.QuotelessValue);
//        }

//        obj.Name = json["name"];
//        obj.Weaknesses = weaknesses.ToArray();
//        obj.Resistances = resistances.ToArray();
//        obj.Immunities = immunities.ToArray();

//        return (obj.Name, obj);
//    }

//    public override string ToString()
//    {
//        return Name;
//    }
//}
