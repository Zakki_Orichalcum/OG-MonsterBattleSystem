﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ITurnAction
{
    int TurnActionPriority { get; }
    bool IsPlaying { get; set; }
    void Perform(List<BattlePlacement> targets);
}

public class UseItemAction : ITurnAction
{
    public int TurnActionPriority => 99;

    public bool IsPlaying { get; set; }

    public Player Player;
    public List<BattlePlacement> Targets;

    public Item ItemUsed;
    public int UseOnTarget;

    public void Perform(List<BattlePlacement> targets)
    {
        Targets = targets;
        BattleController.Instance.LaunchCoroutine(PerformAction());
    }

    private IEnumerator PerformAction()
    {
        IsPlaying = true;

        ItemUsed.PerformUse(Player.Units[UseOnTarget], Targets);
        while (ItemUsed.IsPlaying)
            yield return null;

        IsPlaying = false;
    }
}

public class SwitchTurnAction : ITurnAction
{
    public int TurnActionPriority => 50;

    public bool IsPlaying { get; set; }

    public Player Player;

    public int SwitchFrom;
    public int SwitchTo;

    public void Perform(List<BattlePlacement> targets)
    {
        BattleController.Instance.LaunchCoroutine(PerformAction());
    }

    private IEnumerator PerformAction()
    {
        IsPlaying = true;

        Unit unitFrom = Player.Units[SwitchFrom];

        var switchOut = BattleController.InstantiatePrefab<SwitchOutAnimation>("GenericAnimations/SwitchOutAnimation", unitFrom.transform);

        switchOut.Play();
        while (switchOut.IsPlaying)
            yield return null;

        yield return BattleController.Instance.LaunchCoroutine(SwitchIn());

        switchOut.gameObject.Destroy();

        IsPlaying = false;
    }

    public IEnumerator SwitchIn()
    {
        var battlePlacement = BattleController.Instance.BattlePlacements[Player is ComputerPlayer ? 1 : 0];

        Unit unitFrom = Player.Units[SwitchFrom];
        Unit unitTo = Player.Units[SwitchTo];

        Player.Units[SwitchFrom] = unitTo;
        Player.Units[SwitchTo] = unitFrom;
        battlePlacement.Content = unitTo;

        BattleController.Instance.RemoveUnitFromView(unitFrom);
        BattleController.Instance.RetrieveUnit(unitTo, BattleController.Instance.ObjectPool.transform.position, BattleController.Instance.ObjectPool.transform.rotation);

        var switchIn = BattleController.InstantiatePrefab<SwitchInAnimation>("GenericAnimations/SwitchInAnimation", unitTo.transform);
        switchIn.Position = battlePlacement.transform.position;
        switchIn.Rotation = battlePlacement.transform.rotation;

        switchIn.Play();
        while (switchIn.IsPlaying)
            yield return null;

        switchIn.gameObject.Destroy();
    }
}