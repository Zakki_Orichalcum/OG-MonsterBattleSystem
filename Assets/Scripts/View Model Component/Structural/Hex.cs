﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hex : MonoBehaviour
{
    public static HexPoint[] Neighbors = new HexPoint[]
    {
        new HexPoint(0, 1, -1), //North
        new HexPoint(1, 0, -1), //North East
        new HexPoint(1, -1, 0), //South East
        new HexPoint(0, -1, 1), //South
        new HexPoint(-1, 0, 1), //South West
        new HexPoint(-1, 1, 0), //North West
    };

    public HexModel Model { get { return GetComponentInChildren<HexModel>(); } }
    public Material Material;
    public bool HasModel { get { return Model != null; } }
    public Renderer Renderer
    { 
        get { 
            if(Model == null) 
                InitializeModel();
            return Model.GetComponent<Renderer>();
        }
    }

    private readonly Vector3 PosOffset = new Vector3(0, 0.4f, 0);
    private readonly float RotationOffset = 30;
    private readonly float ScaleOffset = 0.5f;

    private void Start()
    {
        Render();
    }

    public void Render()
    {
        if(Model == null)
            InitializeModel();

        Model.Render();
    }

    public void InitializeModel()
    {
        var hex = new GameObject("Model");
        MeshRenderer renderer = hex.AddComponent<MeshRenderer>();
        hex.AddComponent<MeshFilter>();
        HexModel hm = hex.AddComponent<HexModel>();
        hex.transform.parent = transform;
        hex.transform.localPosition = PosOffset;
        hex.transform.rotation = Quaternion.Euler(new Vector3(0, RotationOffset, 0));
        hex.transform.localScale = new Vector3(ScaleOffset, 1, ScaleOffset);
        renderer.material = Material;
    }

    private void OnWillRenderObject()
    {
        if (!HasModel)
            InitializeModel();

        Model.Render();
    }
    
}
