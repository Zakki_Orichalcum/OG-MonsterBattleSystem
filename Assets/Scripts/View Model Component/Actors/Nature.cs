﻿using UnityEngine;
using System.Collections;

public class Nature : MonoBehaviour
{
    public NatureType NatureType;
    public string Name => NatureType.Name;

    public float GetNatureModifier(StatTypes st) => (NatureType.Benefited == st) ? 1.1f : 
                                                    (NatureType.Detrimented == st) ? 0.9f:
                                                     1.0f;
}
