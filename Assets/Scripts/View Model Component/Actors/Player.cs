﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class Player : MonoBehaviour
{
    public List<Unit> Units;
    public List<Item> Items;
    public abstract string Name { get; }
    public virtual string PossessiveName => $"{Name}'s";

    public List<Unit> ParticipatingUnits;

    private void Awake()
    {
        Units = new List<Unit>();
        ParticipatingUnits = new List<Unit>();
    }
}
