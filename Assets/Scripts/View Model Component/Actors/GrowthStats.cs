﻿using UnityEngine;
using System.Collections;
using System;

//public class GrowthStats : MonoBehaviour
//{
//    #region Constants
//    public const int MIN_GROWTH_PER_STAT = 0;
//    public const int MAX_GROWTH_PER_STAT = 250;
//    public const int MAX_GROWTH = 600;
//    #endregion

//    #region Fields / Properties
//    private IStatistics stats;

//    public int HP
//    {
//        get { return stats[StatTypes.EV_HP]; }
//        set { stats[StatTypes.EV_HP] = value; }
//    }

//    public int MP
//    {
//        get { return stats[StatTypes.EV_MP]; }
//        set { stats[StatTypes.EV_MP] = value; }
//    }

//    public int ATK
//    {
//        get { return stats[StatTypes.EV_ATK]; }
//        set { stats[StatTypes.EV_ATK] = value; }
//    }

//    public int DEF
//    {
//        get { return stats[StatTypes.EV_DEF]; }
//        set { stats[StatTypes.EV_DEF] = value; }
//    }

//    public int MAT
//    {
//        get { return stats[StatTypes.EV_MAT]; }
//        set { stats[StatTypes.EV_MAT] = value; }
//    }

//    public int MDF
//    {
//        get { return stats[StatTypes.EV_MDF]; }
//        set { stats[StatTypes.EV_MDF] = value; }
//    }

//    public int SPD
//    {
//        get { return stats[StatTypes.EV_SPD]; }
//        set { stats[StatTypes.EV_SPD] = value; }
//    }

//    public int GrowthTotal => HP + MP + ATK + DEF + MAT + MDF + SPD;

//    public float GrowthPercent
//    {
//        get { return (float)GrowthTotal / (float)MAX_GROWTH; }
//    }

//    #endregion

//    #region MonoBehaviour
//    void Awake()
//    {
//        stats = GetComponent<Stats>();
//    }

//    void OnEnable()
//    {
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_HP), stats);
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_MP), stats);
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_ATK), stats);
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_DEF), stats);
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_MAT), stats);
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_MDF), stats);
//        this.AddObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_SPD), stats);
//    }

//    void OnDisable()
//    {
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_HP), stats);
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_MP), stats);
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_ATK), stats);
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_DEF), stats);
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_MAT), stats);
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_MDF), stats);
//        this.RemoveObserver(OnGrowthStatWillChange, Stats.WillChangeNotification(StatTypes.EV_SPD), stats);
//    }
//    #endregion

//    #region Event Handlers
//    void OnGrowthStatWillChange(object sender, object args)
//    {
//        ValueChangeException vce = args as ValueChangeException;
//        var allowedMaximum = Math.Min(MAX_GROWTH_PER_STAT, MAX_GROWTH - GrowthTotal);

//        vce.AddModifier(new ClampValueModifier(int.MaxValue, stats[vce.Stat], allowedMaximum));
//    }
//    #endregion
//}
