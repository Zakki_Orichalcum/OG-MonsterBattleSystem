﻿using UnityEngine;
using System.Collections;

public class Driver : MonoBehaviour
{
    public Drivers Normal;
    public Drivers Special;

    public Drivers Current
    {
        get
        {
            return Special != Drivers.None ? Special : Normal;
        }
    }
}
