﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UnitAttributes : MonoBehaviour
{
    public AttributeScriptable[] Attributes;

    #region MonoBehaviour
    void Awake()
    {
        //_effect = GetTrackedEffect();
    }

    #endregion

    #region Event Handlers
    void OnEffectHit(object sender, object args)
    {
        var send = (BaseAbilityEffect)sender;
        var abilityTyping = send.GetComponentInParent<AbilityAttribute>();
        var info = (Info<Unit, Unit, List<ValueModifier>>)args;

        var effectiveness = GetEffectiveness(abilityTyping);

        if (effectiveness != 1)
        {
            info.arg2.Add(new MultValueModifier(1, effectiveness));
            var effectivenessMessage = effectiveness > 1 ? "It's super effective!" : (effectiveness < 1 && effectiveness > 0) ? "It's not very effective." : "It had no effect.";
            BattleController.Instance.ShowSmallBattleMessage(effectivenessMessage, 2.5f);
        }
    }
    #endregion

    public float GetEffectiveness(AbilityAttribute aTyping)
    {
        var output = 1f;
        foreach(var type in Attributes)
        {
            foreach(var e in aTyping.Attribute.AttributeEffectivenesses)
            {
                if(e.AttributeName == type.name)
                {
                    if (e.IsStronglyEffective)
                    {
                        output *= 2f;
                    }
                    else if (e.IsNotVeryEffective)
                    {
                        output *= 0.5f;
                    }
                    else if (e.WillNotAffect)
                    {
                        output *= 0f;
                    }
                }
                
            }
        }

        return output;
    }

    public override string ToString()
    {
        return $"{{ {(Attributes.Length > 1 ? Attributes[0].DisplayName + ", " + Attributes[1].DisplayName : Attributes[0].DisplayName)} }}";
    }
}
