﻿using OrichalcumGames.Statistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is all things based out of a hypothetical system
/// that would be used for display purposes only
/// </summary>
public class StatBundle : MonoBehaviour
{
    public Text StatName;
    public Text IVs;
    public Text EVs;
    public Text Current;

    public Color NatureBenefit;
    public Color NatureDetriment;
    public Color NatureDoesNotEffect;

    public string Stat;
    public bool HasAMaximumStat;
    public string MaximumStat;
    public string BaseStat;
    public string IVStat;
    public string EVStat;

    public void UpdateFromStats(IStatistics s, Nature n)
    {
        int current = s[Stat];
        int b = s[BaseStat];
        int ev = s[EVStat];
        int iv = s[IVStat];

        if (HasAMaximumStat)
        {
            Current.text = $"{current}/{s["MaximumStat"]}";
        }
        else
        {
            Current.text = current.ToString();
        }

        IVs.text = iv.ToString();
        EVs.text = ev.ToString();

        //if(n.NatureType.Benefited == Stat)
        //{
        //    ChangeOutlineColor(StatName.gameObject, NatureBenefit);
        //    ChangeOutlineColor(IVs.gameObject, NatureBenefit);
        //    ChangeOutlineColor(EVs.gameObject, NatureBenefit);
        //    ChangeOutlineColor(Current.gameObject, NatureBenefit);
        //}
        //else if (n.NatureType.Detrimented == Stat)
        //{
        //    ChangeOutlineColor(StatName.gameObject, NatureDetriment);
        //    ChangeOutlineColor(IVs.gameObject, NatureDetriment);
        //    ChangeOutlineColor(EVs.gameObject, NatureDetriment);
        //    ChangeOutlineColor(Current.gameObject, NatureDetriment);
        //}
        //else
        //{
        //    ChangeOutlineColor(StatName.gameObject, NatureDoesNotEffect);
        //    ChangeOutlineColor(IVs.gameObject, NatureDoesNotEffect);
        //    ChangeOutlineColor(EVs.gameObject, NatureDoesNotEffect);
        //    ChangeOutlineColor(Current.gameObject, NatureDoesNotEffect);
        //}
    }

    private void ChangeOutlineColor(GameObject go, Color c)
    {
        //var outline = go.GetComponent<Outline>();
        //outline.OutlineColor = c;
    }
}
