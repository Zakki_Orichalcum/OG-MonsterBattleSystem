﻿using UnityEngine;
using System.Collections;

public class Trait : MonoBehaviour
{
    public string Name;
    public string Description;

    public BaseTraitEffect Effect => GetComponent<BaseTraitEffect>();
    public BaseTraitCondition Condition => GetComponent<BaseTraitCondition>();
    public BaseOutsideBattleTraitEffect OutsideBattle => GetComponent<BaseOutsideBattleTraitEffect>();
}
