﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PreventDamageEffect : BaseTraitEffect
{
    public string DamagePreventedFrom;

    private void OnEnable()
    {
        this.AddObserver(OnTrigger, DamageAbilityEffect.GetEffectivenessModification);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnTrigger, DamageAbilityEffect.GetEffectivenessModification);
    }

    void OnTrigger(object sender, object args)
    {
        var info = args as Info<Unit, List<Unit>, List<ValueModifier>>;
        if (!info.arg1.Any(x => x == owner))
            return;

        var attr = ((GameObject)sender).GetComponentInParent<AbilityAttribute>();

        if (attr.Attribute.name == DamagePreventedFrom.ToUpper())
        {
            info.arg2.Add(new MultValueModifier(3, 0));
        }

    }
}
