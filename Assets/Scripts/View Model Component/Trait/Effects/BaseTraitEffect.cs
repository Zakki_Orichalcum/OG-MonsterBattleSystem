﻿using UnityEngine;
using System.Collections;

public abstract class BaseTraitEffect : MonoBehaviour
{
    protected Unit owner;

    public void Awake()
    {
        owner = GetComponentInParent<Unit>();
    }
}
