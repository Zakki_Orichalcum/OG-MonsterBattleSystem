﻿using UnityEngine;
using System.Collections;

public class PreventStatChangeEffect : BaseTraitEffect
{
    public StatTypes Stat;

    private void OnEnable()
    {
        this.AddObserver(OnTrigger, InflictAbilityEffect.InflictStatus);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnTrigger, InflictAbilityEffect.InflictStatus);
    }

    void OnTrigger(object sender, object args)
    {
        var info = args as Info<Unit, StatTypes, bool, string>;
        if (info.arg0 != owner)
            return;

        if (info.arg1 == Stat)
        {
            var trait = GetComponent<Trait>();
            info.arg2 = false;
            info.arg3 = $"{owner.name}'s {trait.Name} prevents a {Stat} change!";
        }

    }
}
