﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class HealAbilityEffect : BaseAbilityEffect
{
    public const string HealNotification = "HealAbilityEffect.HealNotification";

    public override int Predict(Unit defender)
    {
        Unit attacker = GetComponentInParent<Unit>();
        return GetStat(attacker, defender, GetPowerNotification, 0);
    }

    protected override int OnApply(Unit target)
    {
        // Start with the predicted value
        int value = Predict(target);

        // Add some random variance
        value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

        // Clamp the amount to a range
        value = Mathf.Clamp(value, MIN_DAMAGE, MAX_DAMAGE);

        // Apply the amount to the target
        Health s = target.GetComponent<Health>();
        s.HP += value;
        this.PostNotification(HealNotification, new AbilityNotificationData(target, value));
        return value;
    }
}