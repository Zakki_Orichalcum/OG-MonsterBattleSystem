﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using OrichalcumGames.Statistics;

public class ChangeStageEffect : BaseAbilityEffect
{
    public string StatName;
    public bool IsIncrease;
    public int NumberOfStagesChange;

    public override int Predict(Unit target)
    {
        return 0;
    }

    protected override int OnApply(Unit target)
    {
        Type statusType = GetType(target, StatName);

        var t = target.GetComponents(statusType);
        BaseStage st = null;
        foreach(BaseStage s in t)
        {
            if (s.Stat == StatName)
                st = s;
        }
        BaseStage status = st as BaseStage;
        if(status == null)
        {
            status = target.gameObject.AddComponent(statusType) as BaseStage;
            status.Stat = StatName;
        }

        var info = new Info<Unit, string, bool, string>(target, status.Stat, true, "");
        if(!info.arg2)
        {
            BattleController.Instance.ShowSmallBattleMessage(info.arg3, DamagedAnimation.TimeAmount);

            return 0;
        }

        int output;
        if (IsIncrease)
            output = status.AddStages(NumberOfStagesChange);
        else
            output = status.SubtractStages(NumberOfStagesChange);


        if(output > 0)
        {
            var stat = BattleController.InstantiatePrefab<StatIncreaseStageAnimation>("GenericAnimations/StatIncreaseStageAnimation", target.transform);
            stat.DisplayText = string.Format("{0}'s {1}'s {2} {3}rose!", target.Player.Name, target.name, StatName, (NumberOfStagesChange > 1 ? "sharply " : ""));
            BattleController.Instance.AddToAnimationQueue(stat);
        }
        else if(output < 0)
        {
            var stat = BattleController.InstantiatePrefab<StatDecreaseStageAnimation>("GenericAnimations/StatDecreaseStageAnimation", target.transform);
            stat.DisplayText = string.Format("{0}'s {1}'s {2} {3}fell!", target.Player.Name, target.name, StatName, (NumberOfStagesChange > 1 ? "sharply " : ""));
            BattleController.Instance.AddToAnimationQueue(stat);
        }
        else
        {
            var instance = BattleController.InstantiatePrefab("GenericAnimations/StatFailedStageAnimation");
            var stat = instance.GetComponent<StatIncreaseStageAnimation>();
            stat.DisplayText = "Stage Change Failed";
            BattleController.Instance.AddToAnimationQueue(stat);
        }

        return 0;
    }

    public Type GetType(Unit target, string st)
    {
        var s = target.GetComponent<IStatistics>();
        var stat = s.GetStat(st);

        if (stat.HasNonstandardStages)
            return typeof(NonstandardStage);
        else
            return typeof(StandardStage);
    }
}