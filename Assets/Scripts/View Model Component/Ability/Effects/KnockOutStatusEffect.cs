﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class KnockOutStatusEffect : StatusEffect
{
    public override string StatusName => "KnockedOut";

    private Unit _owner;
    private IStatistics _stats;

    void Awake()
    {
        _owner = GetComponentInParent<Unit>();
        _stats = _owner.GetComponent<IStatistics>();
        var status = _owner.GetComponent<Status>();
        var statusEffects = GetComponentsInChildren<StatusEffect>();
        for(var i = 0; i < statusEffects.Length; i++)
        {
            if (statusEffects[i] != this)
                status.Remove(statusEffects[i]);
        }
    }

    void OnEnable()
    {
        //_owner.transform.localScale = new Vector3(0.75f, 0.1f, 0.75f);
        //this.AddObserver(OnTurnCheck, TurnOrderController.TurnCheckNotification, _owner);
        //this.AddObserver(OnStatCounterWillChange, Stats.WillChangeNotification(StatTypes.CTR), _stats);
    }

    void OnDisable()
    {
        //_owner.transform.localScale = Vector3.one;
        //this.RemoveObserver(OnTurnCheck, TurnOrderController.TurnCheckNotification, _owner);
        //this.RemoveObserver(OnStatCounterWillChange, Stats.WillChangeNotification(StatTypes.CTR), _stats);
    }

    void OnTurnCheck(object sender, object args)
    {
        // Dont allow a KO'd unit to take turns
        BaseException exc = args as BaseException;
        if (exc.DefaultToggle == true)
            exc.FlipToggle();
    }

    void OnStatCounterWillChange(object sender, object args)
    {
        // Dont allow a KO'd unit to increment the turn order counter
        ValueChangeException exc = args as ValueChangeException;
        if (exc.toValue > exc.fromValue)
            exc.FlipToggle();
    }
}