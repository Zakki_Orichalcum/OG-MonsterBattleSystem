﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OrichalcumGames.Statistics;

public abstract class BaseAbilityEffect : MonoBehaviour
{
    #region Consts & Notifications
    protected const int MIN_DAMAGE = -999;
    protected const int MAX_DAMAGE = 999;

    public const string GetAttackNotification = "BaseAbilityEffect.GetAttackNotification";
    public const string GetDefenseNotification = "BaseAbilityEffect.GetDefenseNotification";
    public const string GetPowerNotification = "BaseAbilityEffect.GetPowerNotification";
    public const string TweakDamageNotification = "BaseAbilityEffect.TweakDamageNotification";
    public const string GetCritRateNotification = "BaseAbilityEffect.CriticalNotification";

    public const string MissedNotification = "BaseAbilityEffect.MissedNotification";
    public const string HitNotification = "BaseAbilityEffect.HitNotification";

    //protected string HealthStat => StatisticsMaster.Instance.HealthStat;
    #endregion

    #region Public
    protected Ability Ability { get { return GetComponentInParent<Ability>(); } }

    public abstract int Predict(Unit target);

    public void Apply(Unit target)
    {
        if (GetComponent<AbilityEffectTarget>().IsTarget(target) == false)
            return;

        this.PostNotification(HitNotification, new AbilityNotificationData(target, OnApply(target)));
    }
    #endregion

    #region Protected
    protected abstract int OnApply(Unit target);

    protected virtual int GetStat(Unit attacker, Unit target, string notification, int startValue)
    {
        var mods = new List<ValueModifier>();
        var info = new Info<Unit, Unit, List<ValueModifier>>(attacker, target, mods);
        this.PostNotification(notification, info);
        mods.Sort(Compare);

        float value = startValue;
        for (int i = 0; i < mods.Count; ++i)
            value = mods[i].Modify(startValue, value);

        int retValue = Mathf.FloorToInt(value);
        retValue = Mathf.Clamp(retValue, MIN_DAMAGE, MAX_DAMAGE);
        return retValue;
    }

    int Compare(ValueModifier x, ValueModifier y)
    {
        return x.SortOrder.CompareTo(y.SortOrder);
    }
    #endregion
}

public class AbilityNotificationData
{
    public Unit Target;
    public int Amount;

    public AbilityNotificationData(Unit t, int a)
    {
        Target = t;
        Amount = a;
    }
}