﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class ReviveAbilityEffect : BaseAbilityEffect
{
    public float Percent;

    public override int Predict(Unit target)
    {
        Health s = target.GetComponent<Health>();
        return Mathf.FloorToInt(s.MHP * Percent);
    }

    protected override int OnApply(Unit target)
    {
        Health s = target.GetComponent<Health>();
        int value = s.HP = Predict(target);
        return value;
    }
}