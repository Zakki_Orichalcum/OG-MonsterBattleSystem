﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Assumes that all direct children are categories
/// and that the direct children of categories
/// are abilities
/// </summary>
public class AbilityCatalog : MonoBehaviour
{
    public const int MaxNumberOfAbilities = 4;

    public int AbilityCount()
    {
        return transform.childCount;
    }

    public Ability GetAbility(int abilityIndex)
    {
        if (abilityIndex < 0 || abilityIndex >= transform.childCount)
            return null;
        return transform.GetChild(abilityIndex).GetComponent<Ability>();
    }
}