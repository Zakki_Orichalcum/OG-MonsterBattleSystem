﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

public class InflictAbilityEffect : BaseAbilityEffect
{
    public const string InflictStatus = "InflictAbilityEffect.InflictStatus";

    public string StatusName;
    public int Duration;

    public override int Predict(Unit target)
    {
        return 0;
    }

    protected override int OnApply(Unit target)
    {
        var info = new Info<Unit, string, bool, string>(target, StatusName, true, "");
        this.PostNotification(InflictStatus, info);

        if(!info.arg2)
        {
            BattleController.Instance.ShowSmallBattleMessage(info.arg3, DamagedAnimation.TimeAmount);

            return 0;
        }

        if (target.GetComponentInChildren<StatusEffect>() != null)
            return 0;

        Type statusType = Type.GetType(StatusName);
        if (statusType == null || !statusType.IsSubclassOf(typeof(StatusEffect)))
        {
            Debug.LogError("Invalid Status Type");
            return 0;
        }
        
        MethodInfo mi = typeof(Status).GetMethod("Add");
        Type[] types = new Type[] { statusType, typeof(DurationStatusCondition) };
        MethodInfo constructed = mi.MakeGenericMethod(types);

        Status status = target.GetComponent<Status>();
        object retValue = constructed.Invoke(status, null);

        DurationStatusCondition condition = retValue as DurationStatusCondition;
        condition.Duration = Duration;
        return 0;
    }
}