﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OrichalcumGames.Statistics;

public class DamageAbilityEffect : BaseAbilityEffect
{
    public const string GetTargettingModifierNotification = "DamageAbilityEffect.GetTargettingModifierNotification";
    public const string GetRandomModifierNotification = "DamageAbilityEffect.GetRandomModifierNotification";
    public const string GetSTABModifierNotification = "DamageAbilityEffect.GetSTABModifierNotification";
    public const string GetEffectivenessModification = "DamageAbilityEffect.GetEffectivennessModification";

    #region Public
    public override int Predict(Unit target)
    {
        Unit attacker = GetComponentInParent<Unit>();
        IStatistics stats = GetComponentInParent<IStatistics>();
        Unit defender = target;
        ILevel attackerLevel = GetComponentInParent<ILevel>();

        int attack = GetStat(attacker, defender, GetAttackNotification, 0);
        int defense = GetStat(attacker, defender, GetDefenseNotification, 0);
        
        float levelModifier = (2f * attackerLevel.LVL) / 5f + 2;

        float aOverD = (1.0f * attack) / defense;
        int power = GetStat(attacker, defender, GetPowerNotification, 0);
        float top = levelModifier * power * aOverD;

        int damage = Mathf.RoundToInt(top / 50 + 2);

        return damage;
    }

    protected override int OnApply(Unit defender)
    {
        Unit attacker = GetComponentInParent<Unit>();

        // Start with the predicted damage value
        float value = Predict(defender);

        var targets = Ability.Targets.Count;

        var targetModifier = GetModifiers(attacker, Ability.Targets.GetContents(), GetTargettingModifierNotification, (targets > 1) ? 0.75f : 1f);
        value = value * targetModifier;

        //Weather

        //CritRate
        var critRate = (int)(100 * (1.0f/GetStat(attacker, defender, GetCritRateNotification, 24)));
        var crit = Roll(critRate);
        if (crit)
        {
            value = 2 * value;
        }

        // Add some random variance
        var randomModifier = GetModifiers(attacker, Ability.Targets.GetContents(), GetRandomModifierNotification, UnityEngine.Random.Range(0.85f, 1f));
        value = value * randomModifier;

        var attr = Ability.GetComponent<AbilityAttribute>();
        var atk_attr = attacker.GetComponent<UnitAttributes>();
        var def_attr = defender.GetComponent<UnitAttributes>();

        //STAB
        var stm = (atk_attr.Attributes.Any(x => x.name == attr.Attribute.name) ? 1.5f : 1f);
        var stab = GetModifiers(attacker, Ability.Targets.GetContents(), GetSTABModifierNotification, stm);
        value = value * stab;

        //Attribute Effectiveness
        var effectiveness = def_attr.GetEffectiveness(attr);
        effectiveness = GetModifiers(attacker, Ability.Targets.GetContents(), GetEffectivenessModification, effectiveness);
        value *= effectiveness;

        var effectivenessMessage = new List<string>();
        if(effectiveness != 1)
            effectivenessMessage.Add(effectiveness > 1 ? "It's super effective!" : (effectiveness < 1 && effectiveness > 0) ? "It's not very effective." : "It had no effect.");

        if (crit && effectiveness != 0)
            effectivenessMessage.Add("It was a Critical Hit!");

        // Apply the damage to the target
        var damage = Mathf.FloorToInt(Mathf.Max(value, 1f));
        Health s = defender.GetComponent<Health>();
        
        var instance = BattleController.InstantiatePrefab<DamagedAnimation>("GenericAnimations/DamagedAnimation", defender.transform);
        instance.StartingPercent = (1.0f * s.HP) / s.MHP;
        instance.DamagePercent = (1.0f * s.HP - damage) / s.MHP;
        instance.DamageTypeText = effectivenessMessage.ToArray();

        instance.Play();

        s.HP -= damage;

        return damage;
    }
    #endregion

    public virtual bool Roll(int chance)
    {
        int roll = UnityEngine.Random.Range(0, 101);

        return roll <= chance;
    }

    public virtual float GetModifiers(Unit attacker, List<Unit> targets, string notification, float startValue)
    {
        var mods = new List<ValueModifier>();
        var info = new Info<Unit, List<Unit>, List<ValueModifier>>(attacker, targets, mods);
        this.PostNotification(notification, info);
        mods.Sort(Compare);

        float value = startValue;
        for (int i = 0; i < mods.Count; ++i)
            value = mods[i].Modify(startValue, value);

        return value;
    }

    int Compare(ValueModifier x, ValueModifier y)
    {
        return x.SortOrder.CompareTo(y.SortOrder);
    }
}