﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class AbilityMagicCost : MonoBehaviour
{
    #region Fields
    public string Stat;
    public int Amount;
    private Ability _owner;
    #endregion

    #region MonoBehaviour
    void Awake()
    {
        _owner = GetComponent<Ability>();
    }

    void OnEnable()
    {
        this.AddObserver(OnCanPerformCheck, Ability.CanPerformCheck, _owner);
        this.AddObserver(OnDidPerformNotification, Ability.DidPerformNotification, _owner);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnCanPerformCheck, Ability.CanPerformCheck, _owner);
        this.RemoveObserver(OnDidPerformNotification, Ability.DidPerformNotification, _owner);
    }
    #endregion

    #region Notification Handlers
    void OnCanPerformCheck(object sender, object args)
    {
        IStatistics s = GetComponentInParent<IStatistics>();
        if (s[Stat] < Amount)
        {
            BaseException exc = (BaseException)args;
            exc.FlipToggle();
        }
    }

    void OnDidPerformNotification(object sender, object args)
    {
        IStatistics s = GetComponentInParent<IStatistics>();
        s[Stat] -= Amount;
    }
    #endregion
}