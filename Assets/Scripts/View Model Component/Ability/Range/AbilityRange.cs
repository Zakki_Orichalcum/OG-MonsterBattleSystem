﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AbilityRange : MonoBehaviour
{
    public int Horizontal = 1;
    public int Vertical = int.MaxValue;
    public virtual bool DirectionOriented { get { return false; } }
    public virtual bool PositionOriented { get { return true; } }
    protected Unit Unit { get { return GetComponentInParent<Unit>(); } }

    //public abstract List<Tile> GetTilesInRange(Board board);
}
