﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class AbilityPowerPoints : MonoBehaviour
{
    #region Fields
    public int Maximum;

    public int Current { get; set; }
    private Ability _owner;
    #endregion

    #region MonoBehaviour
    void Awake()
    {
        _owner = GetComponent<Ability>();
    }

    public void SetCurrent(int value)
    {
        Current = value;
    }

    void OnEnable()
    {
        this.AddObserver(OnCanPerformCheck, Ability.CanPerformCheck, _owner);
        this.AddObserver(OnDidPerformNotification, Ability.DidPerformNotification, _owner);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnCanPerformCheck, Ability.CanPerformCheck, _owner);
        this.RemoveObserver(OnDidPerformNotification, Ability.DidPerformNotification, _owner);
    }
    #endregion

    #region Notification Handlers
    void OnCanPerformCheck(object sender, object args)
    {
        IStatistics s = GetComponentInParent<IStatistics>();
        if (!CanStillUse())
        {
            BaseException exc = (BaseException)args;
            exc.FlipToggle();
        }
    }

    void OnDidPerformNotification(object sender, object args)
    {
        Current--;
    }
    #endregion

    public bool CanStillUse()
    {
        return Current > 0;
    }
}
