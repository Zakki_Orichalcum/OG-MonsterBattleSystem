﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class StandardAbilityPower : BaseAbilityPower
{
    public int Strength;
    public string AttackingStat;
    public string DefendingStat;

    protected override int GetBaseAttack()
    {
        return GetComponentInParent<IStatistics>()[AttackingStat];
    }

    protected override int GetBaseDefense(Unit target)
    {
        return target.GetComponent<IStatistics>()[DefendingStat];
    }

    public override int GetPower()
    {
        return Strength;
    }
}