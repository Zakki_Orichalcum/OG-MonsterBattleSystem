﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PercentChance : MonoBehaviour
{
    public int Chance;

    public virtual bool RollForHit(Unit target)
    {
        int roll = UnityEngine.Random.Range(0, 101);
        return roll <= Chance;
    }
}
