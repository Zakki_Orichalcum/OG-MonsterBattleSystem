﻿using UnityEngine;
using System.Collections;

public class FullTypeHitRate : HitRate
{
    public override int Calculate(Unit target)
    {
        Unit defender = target.GetComponent<Unit>();
        if (AutomaticMiss(defender))
            return 0;

        return 100;
    }
}