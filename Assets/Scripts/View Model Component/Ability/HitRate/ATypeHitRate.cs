﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class ATypeHitRate : HitRate
{
    public string EvasionStat;
    public int HitRate;

    public override int Calculate(Unit target)
    {
        Unit defender = target.GetComponent<Unit>();
        if (AutomaticHit(defender))
            return Final(0);

        if (AutomaticMiss(defender))
            return Final(100);
        int evade = GetEvade(defender);
        evade = AdjustForStatusEffects(defender, evade);
        return HitRate+-evade;
    }

    private int GetEvade(Unit target)
    {
        IStatistics s = target.GetComponentInParent<IStatistics>();
        return Mathf.Clamp(s[EvasionStat], 0, 100);
    }
}