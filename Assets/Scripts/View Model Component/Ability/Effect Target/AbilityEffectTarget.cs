﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public abstract class AbilityEffectTarget : MonoBehaviour
{
    //public string HealthStat => StatisticsMaster.Instance.HealthStat;

    public abstract bool IsTarget(Unit tile);
}
