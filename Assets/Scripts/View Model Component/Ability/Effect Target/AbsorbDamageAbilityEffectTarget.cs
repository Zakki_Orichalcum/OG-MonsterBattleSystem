﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class AbsorbDamageAbilityEffectTarget : BaseAbilityEffect
{
    #region Fields
    public string Stat;
    public int TrackedSiblingIndex;
    private BaseAbilityEffect _effect;
    private int _amount;
    #endregion

    #region MonoBehaviour
    void Awake()
    {
        _effect = GetTrackedEffect();
    }

    void OnEnable()
    {
        this.AddObserver(OnEffectHit, BaseAbilityEffect.HitNotification, _effect);
        this.AddObserver(OnEffectMiss, BaseAbilityEffect.MissedNotification, _effect);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnEffectHit, BaseAbilityEffect.HitNotification, _effect);
        this.RemoveObserver(OnEffectMiss, BaseAbilityEffect.MissedNotification, _effect);
    }
    #endregion

    #region Base Ability Effect
    public override int Predict(Unit target)
    {
        return 0;
    }

    protected override int OnApply(Unit target)
    {
        IStatistics s = GetComponentInParent<IStatistics>();
        s[Stat] += _amount;
        return _amount;
    }
    #endregion

    #region Event Handlers
    void OnEffectHit(object sender, object args)
    {
        _amount = (int)args;
    }

    void OnEffectMiss(object sender, object args)
    {
        _amount = 0;
    }
    #endregion

    #region Private
    BaseAbilityEffect GetTrackedEffect()
    {
        Transform owner = GetComponentInParent<Ability>().transform;
        if (TrackedSiblingIndex >= 0 && TrackedSiblingIndex < owner.childCount)
        {
            Transform sibling = owner.GetChild(TrackedSiblingIndex);
            return sibling.GetComponent<BaseAbilityEffect>();
        }
        return null;
    }
    #endregion
}