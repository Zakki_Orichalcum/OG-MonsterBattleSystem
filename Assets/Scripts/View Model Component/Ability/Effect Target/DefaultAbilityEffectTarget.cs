﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class DefaultAbilityEffectTarget : AbilityEffectTarget
{
    public override bool IsTarget(Unit tile)
    {
        if (tile == null)
            return false;

        Health s = tile.GetComponent<Health>();
        return s != null && s.HP > 0;
    }
}
