﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ShootAnimation : Animation
{
    public Transform Bullet;

    public override IEnumerator PlayAnimation()
    {
        //Play Aiming

        //Play Shooting

        //Shoot Bullet
        var offset = new Vector3(0, 0.3f, 0);
        var timeSet = 0.5f;
        var unit = Targets[0].Content;

        var instance = Instantiate(Bullet);
        instance.position = Owner.DefaultAttackPoint.position;
        instance.LookAt(unit.transform);

        //Play Audio of Shoot

        var pos = unit.transform.position;
        pos.y = unit.transform.position.y + unit.ModelHeight / 2.0f;

        var t = instance.MoveTo(pos, timeSet, EasingEquations.Linear);
        while (t != null)
            yield return null;

        ConnectionEvent(unit);

        Destroy(instance.gameObject);

        //Play Audio of Connection
        Finish();
    }
}
