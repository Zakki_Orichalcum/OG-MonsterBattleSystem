﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SwitchInAnimation : Animation
{
    public Vector3 Position;
    public Quaternion Rotation;

    [SerializeField]
    private GameObject BurstingOutParticles;
    [SerializeField]
    private float TotalDuration;
    [SerializeField]
    private string Message = "{0}! Go!";

    public override IEnumerator PlayAnimation()
    {
        Init();
        var cameraRig = BattleController.Instance.CameraRig;

        Owner.transform.SetPositionAndRotation(Position, Rotation);
        Model.ModelObject.transform.localScale = Vector3.zero;

        cameraRig.LookAtTargetUnit(Owner);

        var particles = Instantiate(BurstingOutParticles, Owner.transform);
        Animator.Play("Idle");
        Model.Mesh.enabled = true;

        BattleController.Instance.ShowSmallBattleMessage(string.Format(Message, Owner.name));

        var oldPosition = Model.ModelObject.transform.position;
        Model.ModelObject.transform.position = new Vector3(oldPosition.x, 0, oldPosition.z);

        Tweener t = Model.ModelObject.transform.ScaleTo(Vector3.one, TotalDuration);
        Tweener t2 = Model.ModelObject.transform.MoveTo(oldPosition, TotalDuration);

        while (t != null && t2 != null)
            yield return null;

        BattleController.Instance.HideSmallBattleMessage();

        yield return new WaitForSeconds(0.25f * TotalDuration);

        var opponent = BattleController.Instance.GetOpponent(Owner.Player);
        opponent.ParticipatingUnits.Clear();
        opponent.ParticipatingUnits.Add(opponent.Units.First());
        Owner.Player.ParticipatingUnits.Add(Owner);

        particles.Destroy();
        cameraRig.ResetToDefaultPosition();

        Finish();
    }
}
