﻿using UnityEngine;
using System.Collections;

public class MagicAnimation : Animation
{
    public Transform Bullet;
    public GameObject Explosion;

    public override IEnumerator PlayAnimation()
    {
        //Play Aiming

        //Play Shooting

        //Shoot Bullet
        var timeSet = 0.5f;
        var unit = Targets[0].Content;

        var instance = Instantiate(Bullet);
        instance.position = Owner.DefaultAttackPoint.position;
        instance.LookAt(unit.transform);

        //Play Audio of Shoot
        
        var pos = unit.transform.position;
        pos.y = unit.transform.position.y + unit.ModelHeight / 2.0f;

        var t = instance.MoveTo(pos, timeSet, EasingEquations.Linear);
        while (t != null)
            yield return null;

        var instance2 = Instantiate(Bullet);
        instance2.position = instance.position;

        //Audio of Connection

        ConnectionEvent(unit);
        Destroy(instance.gameObject);
        yield return new WaitForSeconds(1);

        instance2.gameObject.Destroy();

        Finish();
    }
}
