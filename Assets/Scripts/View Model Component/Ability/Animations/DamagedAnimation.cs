﻿using UnityEngine;
using System.Collections;
using System;

public class DamagedAnimation : Animation
{
    public float StartingPercent;
    public float DamagePercent;
    public string[] DamageTypeText;

    public HealthBar HealthBar;
    public const float TimeAmount = 2f;

    private EasingControl ec;

    public void Awake()
    {
        ec = gameObject.AddComponent<EasingControl>();
        ec.duration = TimeAmount;
        ec.equation = EasingEquations.EaseInCubic;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;
    }

    public override IEnumerator PlayAnimation()
    {
        Init();
        HealthBar.Init(Owner.name, StartingPercent);
        HealthBar.Show();

        Animator.Play("Damaged");
        var time = Time.time;

        if (DamageTypeText != null && DamageTypeText.Length > 0)
        {
            if(DamageTypeText.Length == 1)
                BattleController.Instance.ShowBattleMessage(DamageTypeText[0]);
            else
                BattleController.Instance.ShowBattleMessage(DamageTypeText[0], DamageTypeText[1]);
        }
        
        while(Time.time < time + Animator.GetCurrentAnimatorStateInfo(0).length)
            yield return null;

        Animator.Play("Idle");
        yield return new WaitForSeconds(HealthBar.TimeAmount);

        ec.Play();
        while (ec.IsPlaying)
            yield return null;

        yield return new WaitForSeconds(0.25f * TimeAmount);

        HealthBar.Hide();
        yield return new WaitForSeconds(HealthBar.TimeAmount);

        Finish();
    }

    protected override void OnComplete()
    {
        base.OnComplete();
        this.gameObject.Destroy();
    }

    void OnUpdateEvent(object sender, EventArgs e)
    {
        var math = DamagePercent + (StartingPercent - DamagePercent)*(1f - ec.currentValue);
        HealthBar.SetPercent(math);
    }
}
