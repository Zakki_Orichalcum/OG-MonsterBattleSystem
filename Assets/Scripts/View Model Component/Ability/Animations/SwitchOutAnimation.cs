﻿using UnityEngine;
using System.Collections;

public class SwitchOutAnimation : Animation
{
    public Vector3 Position;

    [SerializeField]
    private GameObject BurstingInParticles;
    [SerializeField]
    private float TotalDuration;
    [SerializeField]
    private string Message = "{0}'s {1} was switched out!";

    public override IEnumerator PlayAnimation()
    {
        Init();

        var cameraRig = BattleController.Instance.CameraRig;
        cameraRig.LookAtTargetUnit(Owner);

        Model.ModelObject.transform.localScale = Vector3.one;

        var particles = Instantiate(BurstingInParticles, Owner.transform);

        BattleController.Instance.ShowSmallBattleMessage(string.Format(Message, Owner.Player.Name, Owner.name));

        var oldPosition = Model.ModelObject.transform.position;
        var newPos = new Vector3(oldPosition.x, 0, oldPosition.z);

        Tweener t = Model.ModelObject.transform.ScaleTo(Vector3.zero, TotalDuration);
        Tweener t2 = Model.ModelObject.transform.MoveTo(newPos, TotalDuration);

        while (t != null && t2 != null)
            yield return null;

        BattleController.Instance.HideSmallBattleMessage();

        yield return new WaitForSeconds(0.25f * TotalDuration);

        particles.Destroy();
        cameraRig.ResetToDefaultPosition();

        Model.Mesh.enabled = false;

        Finish();
    }
}
