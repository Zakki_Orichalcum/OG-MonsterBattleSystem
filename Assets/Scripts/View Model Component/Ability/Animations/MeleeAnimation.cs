﻿using UnityEngine;
using System.Collections;

public class MeleeAnimation : Animation
{
    public float Runtime = 1.5f;

    public override IEnumerator PlayAnimation()
    {
        Vector3 originalPos = Owner.transform.position;
        Vector3 originalRot = Owner.transform.rotation.eulerAngles;

        foreach (var target in Targets)
        {
            var unit = target.Content;
            Animator.Play("Moving");
            var tPos = unit.transform.position + (unit.transform.position.x < originalPos.x ? new Vector3(1, 0, 0) : new Vector3(-1, 0, 0) );

            Tweener t = Owner.transform.MoveTo(tPos, Runtime * 0.60f);

            while (t.IsPlaying)
                yield return null;

            //Melee Animation
            Animator.Play("Headbutt");

            yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
            ConnectionEvent(unit);

            Animator.Play("Moving");

            Owner.transform.Rotate(new Vector3(0, 180, 0));
            t = Owner.transform.MoveTo(originalPos, Runtime * 0.30f);

            while (t.IsPlaying)
                yield return null;

            t = Owner.transform.RotateToLocal(originalRot, Runtime * 0.1f, EasingEquations.Linear);

            while (t.IsPlaying)
                yield return null;

            Animator.Play("Idle");
        }

        


        Finish();
    }
}
