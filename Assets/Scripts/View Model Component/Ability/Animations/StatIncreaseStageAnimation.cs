﻿using UnityEngine;
using System.Collections;

public class StatIncreaseStageAnimation : Animation
{
    public GameObject StatRing;

    [SerializeField]
    private Color IncreaseColor;
    [SerializeField]
    private float TotalDuration = 1.5f;
    private float _startingFactor = 0.5f;

    public string DisplayText;

    public override IEnumerator PlayAnimation()
    {
        yield return null;

        var sr1 = Instantiate(StatRing, Owner.transform);
        var sr2 = Instantiate(StatRing, Owner.transform);
        var sr3 = Instantiate(StatRing, Owner.transform);

        ChangeRingColor(sr1);
        ChangeRingColor(sr2);
        ChangeRingColor(sr3);

        var startingHeight = new Vector3(0, Owner.ModelHeight + _startingFactor, 0);
        sr1.transform.localPosition = Vector3.zero;
        sr2.transform.localPosition = Vector3.zero;
        sr3.transform.localPosition = Vector3.zero;

        if(!string.IsNullOrEmpty(DisplayText))
        {
            BattleController.Instance.ShowSmallBattleMessage(DisplayText);
        }

        var sr1Tween = sr1.transform.MoveToLocal(startingHeight, TotalDuration * 0.5f, EasingEquations.EaseInExpo);
        var sr2Tween = sr2.transform.MoveToLocal(startingHeight, TotalDuration * 0.75f, EasingEquations.EaseInExpo);
        var sr3Tween = sr3.transform.MoveToLocal(startingHeight, TotalDuration, EasingEquations.EaseInExpo);

        while (sr1Tween.IsPlaying || sr2Tween.IsPlaying || sr3Tween.IsPlaying)
            yield return null;

        GameObject.Destroy(sr1);
        GameObject.Destroy(sr2);
        GameObject.Destroy(sr3);

        BattleController.Instance.HideSmallBattleMessage();

        Finish();

        GameObject.Destroy(this, 0.02f);
    }

    public void ChangeRingColor(GameObject r)
    {
        foreach (var line in r.GetComponentsInChildren<LineRenderer>())
        {
            line.startColor = IncreaseColor;
            line.endColor = IncreaseColor;
        }
    }
}
