﻿using UnityEngine;
using System.Collections;
using System.Linq;
using OrichalcumGames.Statistics;

//public class ExperienceGainAnimation : Animation
//{
//    public Unit KnockedOut;

//    public const float FillBarTiming = 1.5f;
//    public const float GrowthPanelTiming = 4f;

//    private bool WaitingOnAbilityAssigning = false;

//    public void Init(Unit knockedOut)
//    {
//        Init();
//        KnockedOut = knockedOut;
//    }

//    public override IEnumerator PlayAnimation()
//    {
//        var u = GetComponentInParent<Unit>();

//        var units = u.Player.ParticipatingUnits.Where(x => !x.GetComponent<KnockOutStatusEffect>());
//        var expShare = u.Player.Units.Where(x => x.GetComponentInChildren<SharesExperience>());

//        foreach (var unit in units.Union(expShare))
//        {
//            var expGain = ExperienceGainedFromUnit(unit, KnockedOut, BattleController.Instance.BattleType, units.Count(), expShare.Count());
//            BattleController.Instance.ShowSmallBattleMessage($"{unit.Player}'s {unit.name} gained {expGain} xp!");

//            var rank = unit.GetComponent<Rank>();

//            while (expGain > 0)
//            {
//                var expLeft = rank.ExperiencePointsToNextLevel - rank.ExperiencePoints;
//                float fillPercentage; int expInChunk;
//                if (expGain > expLeft)
//                {
//                    expInChunk = expLeft;
//                    fillPercentage = 1f;
//                }
//                else
//                {
//                    expInChunk = expGain;
//                    fillPercentage = 1.0f * (rank.ExperiencePoints + expGain) / rank.ExperiencePointsToNextLevel;
//                }

//                if (unit == unit.Player.Units.First())
//                {
//                    var fillBar = BattleController.Instance.StatPanelController.PlayerPanel.ExpBar;
//                    fillBar.SetPercent(fillPercentage, FillBarTiming);
//                    yield return new WaitForSeconds(FillBarTiming);
//                }

//                if (fillPercentage == 1)
//                {
//                    var oldStats = Rank.GetStats(unit);
//                    rank.EXP += expInChunk;
//                    BattleController.Instance.ShowSmallBattleMessage($"{unit.Player}'s {unit.name} reached Lv. {rank.LVL}!");
//                    var newStats = Rank.GetStats(unit);

//                    BattleController.Instance.StatGrowthPanel.Show(oldStats, newStats, GrowthPanelTiming);
//                    yield return new WaitForSeconds(GrowthPanelTiming);

//                    var learnableMoves = unit.UnitType.Abilities_Learned.Where(x => x.Item1 == rank.LVL);
//                    if (learnableMoves.Count() > 0)
//                    {
//                        foreach (var ability in learnableMoves)
//                        {
//                            var abCatalog = unit.GetComponentInChildren<AbilityCatalog>();

//                            if(abCatalog.AbilityCount() == AbilityCatalog.MaxNumberOfAbilities)
//                            {
//                                WaitingOnAbilityAssigning = true;

//                                BattleController.Instance.ShowSmallBattleMessage($"{unit.name} would like to learn {ability.Item2}!");
//                                yield return new WaitForSeconds(FillBarTiming);
//                                BattleController.Instance.ShowSmallBattleMessage($"Have {unit.name} learn {ability.Item2}?");
//                                yield return new WaitForSeconds(FillBarTiming);

//                                yield return StartCoroutine(BattleController.Instance.NonBattleMenuController.LearnAbility(this, unit, ability.Item2));
//                                while (WaitingOnAbilityAssigning)
//                                    yield return null;
//                            }
//                            else
//                            {
//                                var ec = new EnrichedCoroutine(this, AbilityFactory.Create(ability.Item2));
//                                yield return ec.coroutine;
//                                var ab = (Ability)ec.result;
//                                ab.transform.SetParent(abCatalog.transform);
//                                BattleController.Instance.ShowSmallBattleMessage($"{unit.name} learned {ability.Item2}!");
//                                yield return new WaitForSeconds(FillBarTiming);
//                            }
//                        }
//                    }

//                    rank.CheckEvolution();
//                }
//                else
//                {
//                    rank.EXP += expInChunk;
//                }

//                expGain -= expInChunk;
//            }

//            if (unit != unit.Player.Units.First())
//            {
//                yield return new WaitForSeconds(1.5f);
//            }

//        }

//        BattleController.Instance.HideSmallBattleMessage();

//        Finish();
//    }

//    public void ToggleWaiting()
//    {
//        WaitingOnAbilityAssigning = false;
//    }

//    public static int ExperienceGainedFromUnit(Unit unit, Unit knockedOutUnit, BattleType battleType,
//                                                int participatingUnits, int heldExp)
//    {
//        var holdsExpShare = unit.GetComponentInChildren<SharesExperience>() != null;
//        var expGrowthItem = unit.GetComponentInChildren<ExperienceGrowth>();

//        var a = battleType.HasFlag(BattleType.Trainer) ? 1.5f : 1f;
//        var b = knockedOutUnit.MonsterType.VictoryYield;
//        var L = knockedOutUnit.GetComponent<IStatistics>()[StatisticsMaster.Instance.LevelStat];
//        var Lp = unit.GetComponent<IStatistics>()[StatisticsMaster.Instance.LevelStat];
//        var s = heldExp == 0 ? participatingUnits : holdsExpShare ? 2 * heldExp : 2 * participatingUnits;
//        var e = expGrowthItem != null ? expGrowthItem.GrowthAmount : 1f;

//        float delta;
//        if (StatisticsMaster.Instance.UseScalingExperienceScale)
//        {
//            delta = (((a * b * L) / 5 * s) * (Mathf.Pow(2 * L + 10, 2.5f) / Mathf.Pow(L + Lp + 10, 2.5f)) + 1) * e; //Scaled Formula
//        }
//        else
//        {
//            delta = (a * b * e * L) / (7f * s); //Flat Formula
//        }

//        return Mathf.RoundToInt(delta);
//    }
//}
