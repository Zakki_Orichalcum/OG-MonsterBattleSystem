﻿using UnityEngine;
using System.Collections;

public class PoisonStatusAnimation : Animation
{
    public Color PoisonColor;
    public GameObject PoisonBubbles;

    public string PoisonMessage = "{0} {1} was poisoned.";

    public override IEnumerator PlayAnimation()
    {
        var cameraRig = BattleController.Instance.CameraRig;
        cameraRig.LookAtTargetUnit(Owner);

        var poisonSystem = Instantiate(PoisonBubbles, Owner.transform);
        poisonSystem.transform.position = Owner.transform.position;

        BattleController.Instance.ShowSmallBattleMessage(PoisonMessage.Format(Owner.Player.Name + "'s", Owner.name));

        yield return new WaitForSeconds(2.25f);

        BattleController.Instance.HideSmallBattleMessage();

        poisonSystem.Destroy();
        cameraRig.ResetToDefaultPosition();

        Finish();
    }
}
