﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class KnockedOutAnimation : Animation
{
    public override IEnumerator PlayAnimation()
    {
        Init();

        var cameraRig = BattleController.Instance.CameraRig;
        cameraRig.LookAtTargetUnit(Owner);

        Animator.Play("Fainting");
        yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
        Animator.Play("Fainted");

        var switchOut = BattleController.InstantiatePrefab<SwitchOutAnimation>("GenericAnimations/SwitchOutAnimation", Owner.transform);

        switchOut.Play();
        while (switchOut.IsPlaying)
            yield return null;

        cameraRig.ResetToDefaultPosition();

        if(!BattleController.Instance.IsCompetitiveBattle && Owner.Player is ComputerPlayer)
        {
            var unit = BattleController.Instance.PlayersUnits.First();
            //var exp = BattleController.InstantiatePrefab<ExperienceGainAnimation>("GenericAnimations/ExperienceGainAnimation", unit.transform);
            //exp.Init(Owner);

            //BattleController.Instance.AddToAnimationQueue(exp);
        }

        Finish();
    }
}
