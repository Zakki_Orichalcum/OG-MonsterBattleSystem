﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class MissedAnimation : Animation
{
    public const string MissedMessage = "But the ability missed {0}'s {1}!";

    public override IEnumerator PlayAnimation()
    {
        var cameraRig = BattleController.Instance.CameraRig;
        foreach (var t in Targets.Select(x=> x.Content))
        {
            var pos = t.transform.position;
            pos.y += t.ModelHeight * 0.6f;
            cameraRig.SetPostion(new Vector3(pos.x + (pos.x > 0 ? -2 : 2), pos.y, pos.z + (pos.z > 0 ? 2 : -2)));
            cameraRig.LookAt(pos);

            BattleController.Instance.ShowSmallBattleMessage(MissedMessage.Format(t.Player.Name, t.name));

            this.PostNotification(BaseAbilityEffect.MissedNotification, t);

            yield return new WaitForSeconds(1f);

            BattleController.Instance.HideSmallBattleMessage();
        }
        
        cameraRig.ResetToDefaultPosition();

        Finish();
    }
}
