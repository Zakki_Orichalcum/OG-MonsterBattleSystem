﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using System.Collections.Generic;

public abstract class Animation : MonoBehaviour
{
    public const int ANIMATION_FRAMES = 30;

    public enum AnimatingState
    {
        Stopped,
        Paused,
        Playing,
        Finished
    }

    public bool IsPlaying => AnimationState == AnimatingState.Playing;

    public AnimatingState AnimationState { get; set; }
    public AnimatingState PreviousAnimationState { get; set; }

    public UnityAction<Unit> ConnectionEvent;
    public event EventHandler UpdateEvent;
    public event EventHandler StateChangeEvent;
    public event EventHandler CompletedEvent;
    public event EventHandler LoopedEvent;

    protected Unit Owner;
    protected Model Model => Owner.GetComponentInChildren<Model>();
    protected Animator Animator { get { return Owner.GetComponent<Animator>(); } }
    protected List<BattlePlacement> Targets { get; set; }

    public void Start()
    {
        Init();
    }

    public virtual void Init()
    {
        Owner = GetComponentInParent<Unit>();
    }

    public abstract IEnumerator PlayAnimation();

    #region MonoBehaviour
    void OnEnable()
    {
        Resume();
    }

    void OnDisable()
    {
        Pause();
    }
    #endregion

    public void Play()
    {
        Targets = GetComponent<Ability>()?.Targets;
        if (!IsPlaying)
            SetAnimationState(AnimatingState.Playing);
    }

    public void Play(List<BattlePlacement> targets)
    {
        Targets = targets;

        if (!IsPlaying)
            SetAnimationState(AnimatingState.Playing);
    }

    public void Pause()
    {
        if (IsPlaying)
            SetAnimationState(AnimatingState.Paused);
    }

    public void Resume()
    {
        if (AnimationState == AnimatingState.Paused)
            SetAnimationState(PreviousAnimationState);
    }

    public void Stop()
    {
        SetAnimationState(AnimatingState.Stopped);
        PreviousAnimationState = AnimatingState.Stopped;
        //loops = 0;
        //if (endBehaviour == EndBehaviour.Reset)
        //    SeekToBeginning();
    }

    public void Finish()
    {
        OnComplete();
        AnimationState = AnimatingState.Finished;
    }

    #region Protected
    protected virtual void OnUpdate()
    {
        if (UpdateEvent != null)
            UpdateEvent(this, EventArgs.Empty);
    }

    protected virtual void OnLoop()
    {
        if (LoopedEvent != null)
            LoopedEvent(this, EventArgs.Empty);
    }

    protected virtual void OnComplete()
    {
        if (CompletedEvent != null)
            CompletedEvent(this, EventArgs.Empty);
        AnimationState = AnimatingState.Finished;
    }

    protected virtual void OnStateChange()
    {
        if (StateChangeEvent != null)
            StateChangeEvent(this, EventArgs.Empty);
    }
    #endregion

    void SetAnimationState(AnimatingState target)
    {
        if (isActiveAndEnabled)
        {
            if (AnimationState == target)
                return;

            PreviousAnimationState = AnimationState;
            AnimationState = target;
            OnStateChange();
            StopCoroutine("PlayAnimation");
            if (IsPlaying)
                StartCoroutine("PlayAnimation");
        }
        else
        {
            PreviousAnimationState = target;
            AnimationState = AnimatingState.Paused;
        }
    }
}

public struct PlayArgs
{
    public Unit[] Targets;
    public object[] Arguments;
}
