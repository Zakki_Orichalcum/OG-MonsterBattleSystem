﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Ability : MonoBehaviour, ITurnAction
{
    public int TurnActionPriority => 0;

    public const string CanPerformCheck = "Ability.CanPerformCheck";
    public const string FailedNotification = "Ability.FailedNotification";
    public const string DidPerformNotification = "Ability.DidPerformNotification";

    public List<BattlePlacement> Targets;

    public bool IsPlaying { get; set; }
    protected Animation Animation { get { return GetComponent<Animation>(); } }

    public bool CanPerform()
    {
        BaseException exc = new BaseException(true);
        this.PostNotification(CanPerformCheck, exc);

        var isKnockedOut = GetComponentInParent<Unit>()?.GetComponentInChildren<KnockOutStatusEffect>() != null;

        return exc.Toggle && !isKnockedOut;
    }

    public void Perform(List<BattlePlacement> targets)
    {
        Targets = targets;
        StartCoroutine("PerformAction");
    }

    public const string BattleMessage1 = "{0}'s {1} uses";
    public const string BattleMessage2 = "{0}!";

    private IEnumerator PerformAction()
    {
        IsPlaying = true;
        var unit = GetComponentInParent<Unit>();

        if (!CanPerform())
        {
            this.PostNotification(FailedNotification);
            yield return null;
        }
        else
        {
            BattleController.Instance.ShowBattleMessage(BattleMessage1.Format(unit.Player.Name, unit.name), BattleMessage2.Format(name));

            yield return new WaitForEndOfFrame();

            var hits = new List<BattlePlacement>();
            var misses = new List<BattlePlacement>();

            foreach (var t in Targets)
            {
                if (t.Content != null
                    && t.Content.GetComponentInChildren<KnockOutStatusEffect>() == null
                    && GetComponent<HitRate>().RollForHit(t.Content))
                {
                    hits.Add(t);
                }
                else
                {
                    misses.Add(t);
                }
            }

            if (misses.Any())
            {
                yield return new WaitForSeconds(BattleMessageController.BattleMessageDisplayTime);

                var instance = BattleController.InstantiatePrefab("GenericAnimations/MissedAnimation");
                var i = instance.GetComponent<MissedAnimation>();
                i.Play(misses);

                while (i.IsPlaying)
                    yield return null;

                instance.Destroy();
            }

            if (hits.Any())
            {
                if (Animation != null)
                {
                    Animation.ConnectionEvent += PerformAbility;
                    Animation.Play();
                    while (Animation.IsPlaying)
                        yield return null;
                    Animation.ConnectionEvent -= PerformAbility;
                }
                else
                {
                    for (int i = 0; i < hits.Count; ++i)
                        if (hits[i].Content != null)
                            PerformAbility(hits[i].Content);
                }
            }

            this.PostNotification(DidPerformNotification);

            if (hits.Any())
                yield return new WaitForSeconds(DamagedAnimation.TimeAmount * 1.5f);
        }

        IsPlaying = false;
    }

    public bool IsTarget(Unit tile)
    {
        Transform obj = transform;
        for (int i = 0; i < obj.childCount; ++i)
        {
            AbilityEffectTarget targeter = obj.GetChild(i).GetComponent<AbilityEffectTarget>();
            if (targeter.IsTarget(tile))
                return true;
        }
        return false;
    }

    void PerformAbility(Unit target)
    {
        for (int i = 0; i < transform.childCount; ++i)
        {
            Transform child = transform.GetChild(i);
            BaseAbilityEffect effect = child.GetComponent<BaseAbilityEffect>();
            AbilityEffectTarget targeter = child.GetComponent<AbilityEffectTarget>();
            PercentChance pc = child.GetComponent<PercentChance>(); 
            var t = (targeter is SelfAbilityEffectTarget) ? GetComponentInParent<Unit>() : target;

            if (pc == null || pc.RollForHit(t))
            {
                effect.Apply(target);
            }
        }
    }
}