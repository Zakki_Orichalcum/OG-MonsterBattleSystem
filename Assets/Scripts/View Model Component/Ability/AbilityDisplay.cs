﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// This is purely for demostration purposes
/// </summary>
public class AbilityDisplay : MonoBehaviour
{
    public Text Power;
    public Text Accuracy;
    public Image DamageType;
    public Text Description;
    public Text Tags;

    [SerializeField]
    private Dictionary<string, Sprite> Sprites;

    public void Start()
    {
        if(Sprites == null)
        {
            Sprites = new Dictionary<string, Sprite>();
        }

        if(!Sprites.ContainsKey("default") && Sprites.Count > 0)
        {
            Sprites.Add("default", Sprites.First().Value);
        }
    }

    public void SetAbility(Ability a)
    {
        var damageType = a.GetComponent<BaseAbilityPower>();
        var accuracy = a.GetComponent<HitRate>();
        var tags = a.GetComponent<Tags>();
        var description = a.GetComponent<Description>();

        Accuracy.text = accuracy is FullTypeHitRate ? "-" : ((ATypeHitRate)accuracy).HitRate.ToString();
        Power.text = damageType != null ? damageType.GetPower().ToString() : "-";
        if (Sprites.Count > 0)
            DamageType.sprite = damageType == null || !(damageType is StandardAbilityPower) ? Sprites["default"] : Sprites[((StandardAbilityPower)damageType).AttackingStat];
        Tags.text = tags.Values.Select(x => $"#{x}").Join(" ");
        Description.text = description.Value;
    }
}
