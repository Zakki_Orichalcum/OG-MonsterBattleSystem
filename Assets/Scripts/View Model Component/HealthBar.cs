﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class HealthBar : MonoBehaviour
{
    public GameObject Canvas;
    public Text NameText;
    public Image TopBar;
    public RectTransform Panel;
    public CanvasGroup Group;
    
    public float TimeAmount = 0.0625f;

    private EasingControl ec;
    private Vector3 _oldPlacement;
    
    public Color HealthBarHigh;
    public Color HealthBarMedium;
    public Color HealthBarLow;

    public void Awake()
    {
        ec = gameObject.AddComponent<EasingControl>();
        ec.duration = TimeAmount;
        ec.equation = EasingEquations.EaseInCubic;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;

        _oldPlacement = Panel.anchoredPosition;

        Canvas.SetActive(false);
    }

    public void Init(string name, float startingPercent)
    {
        NameText.text = name;
        SetPercent(startingPercent);
    }
    
    public void SetPercent(float percent)
    {
        TopBar.fillAmount = percent;
        TopBar.color = percent <= 0.25f ? HealthBarLow : percent <= 0.5f ? HealthBarMedium : HealthBarHigh;
    }

    void OnUpdateEvent(object sender, EventArgs e)
    {
        Group.alpha = ec.currentValue;

        var pos = new Vector2(_oldPlacement.x, _oldPlacement.y - 20 * (1.0f - ec.currentValue));
        Panel.anchoredPosition = pos;
    }

    public void Show()
    {
        Group.alpha = 0;
        Canvas.SetActive(true);
        ec.Play();
    }

    public void Hide()
    {
        ec.Reverse();
    }
}
