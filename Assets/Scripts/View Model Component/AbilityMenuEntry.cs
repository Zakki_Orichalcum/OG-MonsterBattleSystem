﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityMenuEntry : MonoBehaviour
{
    [SerializeField]
    private Image Bullet;
    [SerializeField]
    private Sprite NormalSprite;
    [SerializeField]
    private Sprite SelectedSprite;
    [SerializeField]
    private Sprite DisabledSprite;
    [SerializeField]
    private Text Label;
    [SerializeField]
    private Image Background;

    public string Title
    {
        get { return Label.text; }
        set { Label.text = value; }
    }

    [System.Flags]
    private enum States
    {
        None = 0,
        Selected = 1 << 0,
        Locked = 1 << 1
    }

    public bool IsLocked
    {
        get { return (State & States.Locked) != States.None; }
        set
        {
            if (value)
                State |= States.Locked;
            else
                State &= ~States.Locked;
        }
    }

    public bool IsSelected
    {
        get { return (State & States.Selected) != States.None; }
        set
        {
            if (value)
                State |= States.Selected;
            else
                State &= ~States.Selected;
        }
    }

    private States _state;
    private States State
    {
        get { return _state; }
        set
        {
            if (_state == value)
                return;

            _state = value;

            if (IsLocked)
            {
                Bullet.sprite = DisabledSprite;
                Label.color = UnuseableTextColor;
                Background.color = UnuseableColor;
            }
            else if (IsSelected)
            {
                Bullet.sprite = SelectedSprite;
                Label.color = SelectedTextColor;
                Background.color = SelectedColor;
            }
            else
            {
                Bullet.sprite = NormalSprite;
                Label.color = UnselectedTextColor;
                Background.color = UnselectedColor;
            }
        }
    }

    public Color SelectedColor;
    public Color SelectedTextColor;
    public Color UnuseableColor;
    public Color UnuseableTextColor;

    private Color UnselectedColor;
    private Color UnselectedTextColor;

    private void Awake()
    {
        UnselectedColor = Background.color;
        UnselectedTextColor = Label.color;
    }

    public void Reset()
    {
        State = States.None;
    }
}
