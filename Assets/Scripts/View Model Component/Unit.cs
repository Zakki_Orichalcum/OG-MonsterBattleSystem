﻿using OrichalcumGames.Statistics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public MonsterScriptable MonsterType;
    public float ModelHeight => 2f;
    public Player Player;
    public Transform DefaultAttackPoint => (GetComponentInChildren<AttackPoint>()).transform;

    public override string ToString()
    {
        var stats = GetComponent<IStatistics>();
        return $"{Player}'s {name} {stats.ToString()}";
    }
}
