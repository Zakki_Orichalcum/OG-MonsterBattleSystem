﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDescription : MonoBehaviour
{
    public Image Image;
    public Text ItemName;
    public Text Quantity;
    public Text Description;

    public void Set(Item i)
    {
        //TODO image from item
        ItemName.text = i.Name;
        Quantity.text = $"x{i.Quantity}";
        Description.text = i.Description;
    }
}
