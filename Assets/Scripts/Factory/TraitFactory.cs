﻿using UnityEngine;
using System.Collections;
using System;

public class TraitFactory
{
    public static IEnumerator Create(string traitName)
    {
        GameObject obj = InstantiatePrefab($"Traits/Trait/{traitName}");
        obj.name = traitName;

        yield return obj;
    }

    private static GameObject InstantiatePrefab(string name)
    {
        GameObject prefab = Resources.Load<GameObject>(name);
        if (prefab == null)
        {
            Debug.LogError("No Prefab for name: " + name);
            return new GameObject(name);
        }
        GameObject instance = GameObject.Instantiate(prefab);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance;
    }

    //private static void AddFromType(GameObject obj, string traitType, string trigger, JSONClass[] effects)
    //{
    //    switch (traitType)
    //    {
    //        case "prevent-status":
    //            AddPreventStatus(obj, trigger);
    //            break;
    //        case "prevent-stat-change":
    //            AddPreventStatChange(obj, trigger);
    //            break;
    //        case "prevent-damage":
    //            AddPreventDamage(obj, trigger);
    //            break;
    //        case "draw-in":
    //        case "increase-ability-power":
    //        case "enter-field":
    //        case "stat-increase":
    //        default:
    //            Debug.LogError($"Have not created type '{traitType}' for Traits!");
    //            break;
    //    }
    //}

    //private static void AddPreventStatus(GameObject obj, string status)
    //{
    //    var ps = obj.AddComponent<PreventStatusEffect>();
    //    ps.StatusEffect = status;
    //}

    //private static void AddPreventStatChange(GameObject obj, string stat)
    //{
    //    var ps = obj.AddComponent<PreventStatChangeEffect>();
    //    ps.Stat = (StatTypes)Enum.Parse(typeof(StatTypes), stat);
    //}

    //private static void AddPreventDamage(GameObject obj, string prevent)
    //{
    //    var ps = obj.AddComponent<PreventDamageEffect>();
    //    ps.DamagePreventedFrom = prevent;
    //}

    //private static void AddEffects(GameObject obj, JSONClass[] effects)
    //{
    //    foreach (JSONClass json in effects)
    //    {
    //        switch (json["type"].QuotelessValue.ToLower())
    //        {
    //            case "stat-change":
    //                AddStatChange(obj, json);
    //                break;
    //            case "apply-status":
    //                ApplyStatus(obj, json);
    //                break;
    //            case "recharge":
    //            case "charge":
    //            case "multi-hit":
    //            case "critical-rate":
    //            case "take-item":
    //            case "interrupt":
    //            case "repeat-move":
    //            case "protection":
    //            case "switch-out":
    //            case "heal":
    //            case "flinch":
    //            default:
    //                Debug.LogError($"Have not created type '{json["type"]}' for abilities!");
    //                break;
    //        }
    //    }
    //}

    //private static void AddStatChange(GameObject obj, JSONClass json)
    //{
    //    GameObject main = new GameObject("StatChange");
    //    main.transform.SetParent(obj.transform);
    //    var st = main.AddComponent<ChangeStageEffect>();
    //    var amount = json["amount"].AsInt;
    //    st.IsIncrease = amount > 0;
    //    st.NumberOfStagesChange = Math.Abs(amount);
    //    st.StatName = (StatTypes)Enum.Parse(typeof(StatTypes), json["stat"].QuotelessValue);

    //    var target = json["target"];
    //    if (target != null)
    //    {
    //        if (target.QuotelessValue == "self")
    //        {
    //            main.AddComponent<SelfAbilityEffectTarget>();
    //        }
    //        else
    //        {
    //            main.AddComponent<DefaultAbilityEffectTarget>();
    //        }
    //    }
    //    else
    //    {
    //        main.AddComponent<DefaultAbilityEffectTarget>();
    //    }

    //    var chance = json["chance"];
    //    if (chance != null)
    //    {
    //        var c = main.AddComponent<PercentChance>();
    //        c.Chance = chance.AsInt;
    //    }
    //}

    //private static void ApplyStatus(GameObject obj, JSONClass json)
    //{
    //    GameObject main = new GameObject("ApplyStatus");
    //    main.transform.SetParent(obj.transform);
    //    var st = main.AddComponent<InflictAbilityEffect>();

    //    st.StatusName = json["status"].QuotelessValue + "StatusEffect";
    //    var duration = json["duration"];
    //    if (duration != null)
    //    {
    //        st.Duration = duration.AsInt;
    //    }

    //    var target = json["target"];
    //    if (target != null)
    //    {
    //        if (target.QuotelessValue == "self")
    //        {
    //            main.AddComponent<SelfAbilityEffectTarget>();
    //        }
    //        else
    //        {
    //            main.AddComponent<DefaultAbilityEffectTarget>();
    //        }
    //    }
    //    else
    //    {
    //        main.AddComponent<DefaultAbilityEffectTarget>();
    //    }

    //    var chance = json["chance"];
    //    if (chance != null)
    //    {
    //        var c = main.AddComponent<PercentChance>();
    //        c.Chance = chance.AsInt;
    //    }
    //}
}
