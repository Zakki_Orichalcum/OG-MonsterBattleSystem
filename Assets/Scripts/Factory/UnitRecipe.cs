﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System;
//using System.Collections.Specialized;

//public class UnitRecipe //: OutsideResource
//{
//    public string RecipeName;
//    public string Model;

//    public int BaseHP;
//    public int BaseMP;
//    public int BaseATK;
//    public int BaseDEF;
//    public int BaseMAT;
//    public int BaseMDF;
//    public int BaseSPD;

//    public string[] Attributes;
//    public (string, float)[] Traits;

//    public (int, string)[] Abilities_Learned;
//    public string[] Abilities_Teachable;

//    public string[] FusionGroups;

//    public EVGains[] EVs;
//    public string EXPGain;
//    public int ExpPointYield;

//    public const string UnitRecipePath = "{0}/Settings/MonsterGameData_Units.json";
//    public const string UnitRecipeKey = "UnitRecipes";

//    public Dictionary<string, UnitRecipe> LoadAllResources()
//    {
//        if (InformationCache.ContainsKey(UnitRecipeKey))
//        {
//            var o = InformationCache.Get<Dictionary<string, UnitRecipe>>(UnitRecipeKey);

//            return o;
//        }

//        string readPath = string.Format(UnitRecipePath, Application.dataPath);
//        string readText = File.ReadAllText(readPath);
//        var json = JSON.Parse(readText);

//        Dictionary<string, UnitRecipe> resources = ParseUnitRecipe(json["UnitsRecipes"].AsArray);

//        InformationCache.Store<Dictionary<string, UnitRecipe>>(UnitRecipeKey, resources);

//        return resources;
//    }

//    public static IEnumerator LoadResource(string key)
//    {
//        var dict = new Dictionary<string, UnitRecipe>();
//        var continuing = true;
//        if (InformationCache.ContainsKey(UnitRecipeKey))
//        {
//            dict = InformationCache.Get<Dictionary<string, UnitRecipe>>(UnitRecipeKey);

//            if (dict.ContainsKey(key))
//            {
//                yield return dict[key];
//                continuing = false;
//            }
//        }

//        if(continuing)
//        {
//            var webRequest = new WebRequest();
//            var url = "http://localhost:13522/api/monster?name=" + key;
//            var headers = new NameValueCollection();

//            yield return BattleController.Instance.StartCoroutine(webRequest.Get(url, headers));

//            var request = webRequest.Result;
//            var json = JSON.Parse(request);
//            var result = ParseUnitRecipe(json.AsObject);

//            dict.Add(key, result);
//            InformationCache.Store<Dictionary<string, UnitRecipe>>(UnitRecipeKey, dict);

//            yield return result;
//        }
//    }

//    //public override OutsideResource LoadResource(string key)
//    //{
//    //    //TODO do this individually from the server and have it make a cache as needed.

//    //    var dict = LoadAllResources();

//    //    if(dict.ContainsKey(key))
//    //    {
//    //        return dict[key];
//    //    }
//    //    else
//    //    {
//    //        throw new NotImplementedException();
//    //    }
//    //}

//    static Dictionary<string, UnitRecipe> ParseUnitRecipe(JSONArray jArr)
//    {
//        var dict = new Dictionary<string, UnitRecipe>();
//        foreach (JSONClass ur in jArr)
//        {
//            dict.Add(ur["name"], ParseUnitRecipe(ur));
//        }

//        return dict;
//    }

//    static UnitRecipe ParseUnitRecipe(JSONClass unitR)
//    {
//        UnitRecipe obj = new UnitRecipe();
//        obj.RecipeName = unitR["name"];
//        obj.Model = unitR["Model"];

//        obj.BaseHP = unitR["baseHP"].AsInt;
//        obj.BaseMP = unitR["baseMP"].AsInt;
//        obj.BaseATK = unitR["baseATK"].AsInt;
//        obj.BaseDEF = unitR["baseDEF"].AsInt;
//        obj.BaseMAT = unitR["baseMAT"].AsInt;
//        obj.BaseMDF = unitR["baseMDF"].AsInt;
//        obj.BaseSPD = unitR["baseSPD"].AsInt;

//        obj.Attributes = unitR["attributes"].AsArray.Children.Select(x => x.QuotelessValue).ToArray();
//        obj.Traits = unitR["traits"].AsArray.Children.Select(x => (x["name"].QuotelessValue, x["probability"].AsFloat)).ToArray();

//        obj.Abilities_Learned = unitR["Abilities_Learned"].AsArray.Children.Select(x => (x["level"].AsInt, x["ability"].QuotelessValue)).ToArray();
//        //obj.Abilities_Teachable = unitR["Model"];

//        obj.FusionGroups = unitR["fusionGroups"].Children.Select(x => x.QuotelessValue).ToArray();
//        obj.EVs = unitR["EVs"].Children.Select(x => new EVGains {
//            Stat = (StatTypes)Enum.Parse(typeof(StatTypes), x["statName"].QuotelessValue),
//            Amount = x["amount"].AsInt
//        }).ToArray();
//        obj.EXPGain = unitR["expGain"].QuotelessValue;
//        obj.ExpPointYield = unitR["expPointYield"].AsInt;

//        return obj;
//    }
//}

//public struct EVGains
//{
//    public StatTypes Stat;
//    public int Amount;
//}
