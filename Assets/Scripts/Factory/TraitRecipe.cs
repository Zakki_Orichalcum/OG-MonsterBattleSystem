﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System;
//using System.Collections.Specialized;

//public class TraitRecipe //: OutsideResource
//{
//    public string RecipeName;
//    public string TraitType;
//    public string Trigger;
//    public JSONClass[] Effects;
//    public string Description;

//    public const string Path = "{0}/Settings/MonsterGameData_Traits.json";
//    public const string TraitRecipeKey = "TraitRecipes";

//    public Dictionary<string, TraitRecipe> LoadAllResources()
//    {
//        if (InformationCache.ContainsKey(TraitRecipeKey))
//        {
//            var o = InformationCache.Get<Dictionary<string, TraitRecipe>>(TraitRecipeKey);

//            return o;
//        }

//        string readPath = string.Format(Path, Application.dataPath);
//        string readText = File.ReadAllText(readPath);
//        var json = JSON.Parse(readText);

//        Dictionary<string, TraitRecipe> resources = ParseTraits(json["traits"].AsArray);

//        InformationCache.Store<Dictionary<string, TraitRecipe>>(TraitRecipeKey, resources);

//        return resources;
//    }

//    public static IEnumerator LoadResource(string key)
//    {
//        var dict = new Dictionary<string, TraitRecipe>();
//        var continuing = true;
//        if (InformationCache.ContainsKey(TraitRecipeKey))
//        {
//            dict = InformationCache.Get<Dictionary<string, TraitRecipe>>(TraitRecipeKey);

//            if (dict.ContainsKey(key))
//            {
//                yield return dict[key];
//                continuing = false;
//            }
//        }

//        if (continuing)
//        {
//            var webRequest = new WebRequest();
//            var url = "http://localhost:13522/api/trait?name=" + key;
//            var headers = new NameValueCollection();
//            var ec = new EnrichedCoroutine(BattleController.Instance, webRequest.Get(url, headers));
//            yield return ec.coroutine;

//            var request = (string)ec.result;
//            var json = JSON.Parse(request);
//            var result = ParseTrait(json.AsObject);

//            dict.Add(key, result);
//            InformationCache.Store<Dictionary<string, TraitRecipe>>(TraitRecipeKey, dict);

//            yield return result;
//        }
//    }

//    static Dictionary<string, TraitRecipe> ParseTraits(JSONArray jArr)
//    {
//        var dict = new Dictionary<string, TraitRecipe>();
//        foreach (JSONClass ur in jArr)
//        {
//            dict.Add(ur["name"], ParseTrait(ur));
//        }

//        return dict;
//    }

//    static TraitRecipe ParseTrait(JSONClass ab)
//    {
//        TraitRecipe obj = new TraitRecipe();
//        obj.RecipeName = ab["name"].QuotelessValue;
//        obj.TraitType = ab["type"].QuotelessValue.ToLower();
//        obj.Trigger = ab["trigger"].QuotelessValue;
//        obj.Effects = ab["effects"].AsArray.Children.Select(x => x.AsObject).ToArray();
//        obj.Description = ab["description"].QuotelessValue;

//        return obj;
//    }

//    public override string ToString()
//    {
//        return $"{RecipeName}, {TraitType}, {Trigger}, {Description}, {Effects.Count()}";
//    }
//}