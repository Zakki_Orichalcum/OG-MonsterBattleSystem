﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.Specialized;
using OrichalcumGames.Statistics;

public class UnitFactory
{
    public static IEnumerator Create(UnitInformation ui)
    {
        //var cd = new EnrichedCoroutine(BattleController.Instance, UnitRecipe.LoadResource(ui.UnitType));
        //yield return cd.coroutine;

        //var ec = new EnrichedCoroutine(BattleController.Instance, Create((UnitRecipe)cd.result, ui));
        //yield return ec.coroutine;

        //yield return (GameObject)ec.result;
        yield return null;
    }

    ////public static GameObject Create2(UnitInformation ui)
    ////{
    ////    UnitRecipe recipe = OutsideResource.Load<UnitRecipe>(ui.UnitType);
    ////    if (recipe == null)
    ////    {
    ////        Debug.LogError("No Unit Recipe for name: " + ui.UnitType);
    ////        return null;
    ////    }
    ////    return Create(recipe, ui);
    ////}

    //public static IEnumerator Create(UnitRecipe recipe, UnitInformation ui)
    //{
    //    GameObject obj = InstantiatePrefab("Units/Unit");
    //    obj.name = string.IsNullOrEmpty(ui.Nickname) ? ui.UnitType : ui.Nickname;

    //    ////obj.name = recipe.RecipeName;
    //    //var unit = obj.AddComponent<Unit>();
    //    //unit.UnitType = recipe;
    //    //yield return BattleController.Instance.StartCoroutine(AddModel(obj, ui));
    //    //AddStats(obj, ui);

    //    //obj.AddComponent<Status>();
    //    //obj.AddComponent<Equipment>();

    //    //var h = obj.AddComponent<Health>();
    //    //h.Reset();
    //    //var m = obj.AddComponent<Mana>();
    //    //m.Reset();
    //    //yield return BattleController.Instance.StartCoroutine(AddAbilityCatalog(obj, ui.Abilities));
    //    //yield return BattleController.Instance.StartCoroutine(AddTrait(obj, ui.Trait));
    //    ////AddHappiness(obj, ui.Happiness);
    //    //yield return BattleController.Instance.StartCoroutine(AddAttributes(obj, recipe.Attributes));
    //    //AddNature(obj, ui.Nature);

    //    //var attr = obj.GetComponent<UnitAttributes>();
    //    //var renderer = obj.GetComponentInChildren<Model>().GetComponentInChildren<MeshRenderer>();
    //    //renderer.material.SetColor("_Color", Colors.ColorFromWord(obj.name, attr.Attributes.Length > 1 ? 
    //    //    attr.Attributes[0].Color.Combine(attr.Attributes[1].Color) : attr.Attributes[0].Color));

    //    yield return obj;
    //}

    //private static GameObject InstantiatePrefab(string name)
    //{
    //    GameObject prefab = Resources.Load<GameObject>(name);
    //    if (prefab == null)
    //    {
    //        Debug.LogError("No Prefab for name: " + name);
    //        return new GameObject(name);
    //    }
    //    GameObject instance = GameObject.Instantiate(prefab);
    //    instance.name = instance.name.Replace("(Clone)", "");
    //    return instance;
    //}

    //private static IEnumerator AddModel(GameObject obj, UnitInformation info)
    //{
    //    var unit = obj.GetComponent<Unit>();
    //    var recipe = unit.UnitType;
    //    GameObject model = InstantiatePrefab("UnitModels/" + (string.IsNullOrEmpty(recipe.Model) ? "Creature" : recipe.Model));
    //    //GameObject model = new GameObject("Monster Model");
    //    model.transform.SetParent(obj.transform);
    //    //var c = model.GetComponent<CreatureController>();

    //    var ec = new EnrichedCoroutine(BattleController.Instance, ModelRecipe.LoadResource(info.UnitType));
    //    yield return ec.coroutine;
    //    ModelRecipe readObj = (ModelRecipe)ec.result;

    //    //c.LoadFromJson(readObj.ModelData);

    //    obj.GetComponentInChildren<Model>().SetModel(model.transform);
    //}

    //private static void AddStats(GameObject obj, UnitInformation info)
    //{
    //    //IStatistics s = obj.AddComponent<IStatistics>();
    //    //Unit u = obj.GetComponent<Unit>();
    //    //UnitRecipe ut = u.UnitType;

    //    //s.SetValue(StatTypes.BASE_HP, ut.BaseHP, false);
    //    //s.SetValue(StatTypes.BASE_MP, ut.BaseMP, false);
    //    //s.SetValue(StatTypes.BASE_ATK, ut.BaseATK, false);
    //    //s.SetValue(StatTypes.BASE_DEF, ut.BaseDEF, false);
    //    //s.SetValue(StatTypes.BASE_MAT, ut.BaseMAT, false);
    //    //s.SetValue(StatTypes.BASE_MDF, ut.BaseMDF, false);
    //    //s.SetValue(StatTypes.BASE_SPD, ut.BaseSPD, false);

    //    //s.SetValue(StatTypes.IV_HP, info.IV_HP, false);
    //    //s.SetValue(StatTypes.IV_MP, info.IV_MP, false);
    //    //s.SetValue(StatTypes.IV_ATK, info.IV_ATK, false);
    //    //s.SetValue(StatTypes.IV_DEF, info.IV_DEF, false);
    //    //s.SetValue(StatTypes.IV_MAT, info.IV_MAT, false);
    //    //s.SetValue(StatTypes.IV_MDF, info.IV_MDF, false);
    //    //s.SetValue(StatTypes.IV_SPD, info.IV_SPD, false);

    //    //AddGrowthStats(obj, info);
    //    //AddRank(obj, info);
    //}

    //private static void AddGrowthStats(GameObject obj, UnitInformation info)
    //{
    //    //GrowthStats gs = obj.AddComponent<GrowthStats>();

    //    //gs.HP = info.EV_HP;
    //    //gs.MP = info.EV_MP;
    //    //gs.ATK = info.EV_ATK;
    //    //gs.DEF = info.EV_DEF;
    //    //gs.MAT = info.EV_MAT;
    //    //gs.MDF = info.EV_MDF;
    //    //gs.SPD = info.EV_SPD;
    //}

    //private static void AddRank(GameObject obj, UnitInformation info)
    //{
    //    //Rank rank = obj.AddComponent<Rank>();
    //    //rank.Init(info.Level, info.Current_EXP);
    //}

    //private static IEnumerator AddAbilityCatalog(GameObject obj, string[] abilities)
    //{
    //    GameObject main = new GameObject("Ability Catalog");
    //    main.transform.SetParent(obj.transform);
    //    main.AddComponent<AbilityCatalog>();

    //    for (int i = 0; i < abilities.Length; ++i)
    //    {
    //        var ec = new EnrichedCoroutine(BattleController.Instance, AbilityFactory.Create(abilities[i]));
    //        yield return ec.coroutine;
    //        GameObject ability = (GameObject)ec.result;
    //        ability.transform.SetParent(main.transform);
    //    }
    //}

    //private static IEnumerator AddAttributes(GameObject obj, string[] ua)
    //{
    //    var attr = obj.AddComponent<UnitAttributes>();

    //    var aList = new List<Attribute>();

    //    foreach (string a in ua)
    //    {
    //        var ec = new EnrichedCoroutine(BattleController.Instance, Attribute.LoadResource(a));
    //        yield return ec.coroutine;
    //        Attribute r = (Attribute)ec.result;
    //        if (r == null)
    //        {
    //            Debug.LogError("No Attribute for name: " + a);
    //        }
    //        else
    //        {
    //            aList.Add(r);
    //        }
    //    }
    //    attr.Attributes = aList.ToArray();
    //}

    //private static void AddNature(GameObject obj, string nature)
    //{
    //    var n = obj.AddComponent<Nature>();
    //    NatureType nt = OutsideResource.Load<NatureType>(nature);
    //    if (nt == null)
    //    {
    //        Debug.LogError("No Nature for name: " + nature);
    //        return;
    //    }

    //    n.NatureType = nt;
    //}

    ////private static void AddHappiness(GameObject obj, int happiness)
    ////{
    ////    var h = obj.AddComponent<Happiness>();
    ////    h.Amount = happiness;
    ////}

    //private static IEnumerator AddTrait(GameObject obj, string traitName)
    //{
    //    var ec = new EnrichedCoroutine(BattleController.Instance, TraitFactory.Create(traitName));
    //    yield return ec.coroutine;
    //    var trait = (GameObject)ec.result;
    //    trait.transform.SetParent(obj.transform);
    //}
}

public struct UnitInformation
{
    public string Nickname;
    public string UnitType;
    public string Trait;
    public string[] Abilities;
    public string Nature;
    public int Current_EXP;
    public int Level;
    public int EV_HP;
    public int EV_MP;
    public int EV_ATK;
    public int EV_DEF;
    public int EV_MAT;
    public int EV_MDF;
    public int EV_SPD;
    public int IV_HP;
    public int IV_MP;
    public int IV_ATK;
    public int IV_DEF;
    public int IV_MAT;
    public int IV_MDF;
    public int IV_SPD;
    public int Happiness;
}
