﻿using UnityEngine;
using System.Collections;

public class AbilityCatalogRecipe : ScriptableObject
{
    [System.Serializable]
    public class Category
    {
        public string Name;
        public string[] Entries;
    }
    public Category[] categories;
}