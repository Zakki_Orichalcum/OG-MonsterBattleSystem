﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class NatureType : OutsideResource
{
    public string Name;
    public StatTypes Benefited;
    public StatTypes Detrimented;

    public const string Path = "{0}/Settings/MonsterGameData_Natures.json";
    public const string NaturesCacheKey = "Natures";

    public override Dictionary<string, OutsideResource> LoadAllResources()
    {
        if (InformationCache.ContainsKey(NaturesCacheKey))
        {
            var o = InformationCache.Get<Dictionary<string, OutsideResource>>(NaturesCacheKey);

            return o;
        }

        string readPath = string.Format(Path, Application.dataPath);
        string readText = File.ReadAllText(readPath);
        var json = JSON.Parse(readText);

        Dictionary<string, OutsideResource> resources = ParseUnitRecipe(json["natures"].AsArray);

        InformationCache.Store<Dictionary<string, OutsideResource>>(NaturesCacheKey, resources);

        return resources;
    }

    public override OutsideResource LoadResource(string key)
    {
        var dict = LoadAllResources();

        return dict[key];
    }

    static Dictionary<string, OutsideResource> ParseUnitRecipe(JSONArray jArr)
    {
        var dict = new Dictionary<string, OutsideResource>();
        foreach (JSONClass ur in jArr)
        {
            dict.Add(ur["name"], ParseNature(ur));
        }

        return dict;
    }

    static NatureType ParseNature(JSONClass unitR)
    {
        NatureType obj = new NatureType();
        obj.Name = unitR["name"];
        obj.Benefited = !string.IsNullOrEmpty(unitR["b"]) ? (StatTypes)Enum.Parse(typeof(StatTypes), unitR["b"]) : StatTypes.Count;
        obj.Detrimented = !string.IsNullOrEmpty(unitR["d"]) ? (StatTypes)Enum.Parse(typeof(StatTypes), unitR["d"]) : StatTypes.Count;

        return obj;
    }

    public override string ToString()
    {
        return Name;
    }
}
