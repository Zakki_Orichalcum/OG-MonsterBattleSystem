﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System;
//using System.Linq;
//using System.Collections.Specialized;

//public class AbilityRecipe //: OutsideResource
//{
//    public string RecipeName;
//    public string Attribute;
//    public string Category;
//    public int Power;
//    public int Accuracy;
//    public int MP;
//    public JSONClass[] Additions;
//    public string Description;
//    public string[] Tags;
//    public string Animation;

//    public const string Path = "{0}/Settings/MonsterGameData_Abilities.json";
//    public const string AbilityRecipeKey = "AbilityRecipes";

//    public Dictionary<string, AbilityRecipe> LoadAllResources()
//    {
//        if (InformationCache.ContainsKey(AbilityRecipeKey))
//        {
//            var o = InformationCache.Get<Dictionary<string, AbilityRecipe>>(AbilityRecipeKey);

//            return o;
//        }

//        string readPath = string.Format(Path, Application.dataPath);
//        string readText = File.ReadAllText(readPath);
//        var json = JSON.Parse(readText);

//        Dictionary<string, AbilityRecipe> resources = ParseAbilities(json["abilities"].AsArray);

//        InformationCache.Store<Dictionary<string, AbilityRecipe>>(AbilityRecipeKey, resources);

//        return resources;
//    }

//    public static IEnumerator LoadResource(string key)
//    {
//        var dict = new Dictionary<string, AbilityRecipe>();
//        var continuing = true;
//        if (InformationCache.ContainsKey(AbilityRecipeKey))
//        {
//            dict = InformationCache.Get<Dictionary<string, AbilityRecipe>>(AbilityRecipeKey);

//            if (dict.ContainsKey(key))
//            {
//                yield return dict[key];
//                continuing = false;
//            }
//        }

//        if (continuing)
//        {
//            var webRequest = new WebRequest();
//            var url = "http://localhost:13522/api/ability?name=" + key;
//            var headers = new NameValueCollection();

//            var ec = new EnrichedCoroutine(BattleController.Instance, webRequest.Get(url, headers));
//            yield return ec.coroutine;

//            var request = (string)ec.result;
//            var json = JSON.Parse(request);
//            AbilityRecipe ar = ParseAbility(json.AsObject);
//            dict.Add(key, ar);
//            InformationCache.Store<Dictionary<string, AbilityRecipe>>(AbilityRecipeKey, dict);

//            yield return ar;
//        }
//    }

//    static Dictionary<string, AbilityRecipe> ParseAbilities(JSONArray jArr)
//    {
//        var dict = new Dictionary<string, AbilityRecipe>();
//        foreach (JSONClass ur in jArr)
//        {
//            dict.Add(ur["name"], ParseAbility(ur));
//        }

//        return dict;
//    }

//    static AbilityRecipe ParseAbility(JSONClass ab)
//    {
//        AbilityRecipe obj = new AbilityRecipe();
//        obj.RecipeName = ab["name"].QuotelessValue;
//        obj.Attribute = ab["attribute"].QuotelessValue.ToUpper();
//        obj.Category = ab["category"].QuotelessValue.ToUpper();
//        obj.Power = ab["power"] != null ? ab["power"].AsInt : -1;
//        obj.Accuracy = ab["accuracy"] != null ? ab["accuracy"].AsInt : -1;
//        obj.MP = ab["mp"].AsInt;
//        obj.Additions = ab["additions"].AsArray.Children.Select(x => x.AsObject).ToArray();
//        obj.Description = ab["description"].QuotelessValue;
//        obj.Tags = ab["tags"].AsArray.Children.Select(x => x.QuotelessValue).ToArray();

//        return obj;
//    }

//    public override string ToString()
//    {
//        return $"{RecipeName}, {Attribute}, {Category}, {Power}, {Accuracy}, {MP}, {Tags.ToExpandedString()}, {Additions.Count()}";
//    }
//}
