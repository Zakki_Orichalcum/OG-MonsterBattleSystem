﻿using UnityEngine;
using System.Collections;
using System;

public class AbilityFactory
{
    public static IEnumerator Create(string abilityName)
    {
        GameObject obj = InstantiatePrefab($"Abilities/Ability/{abilityName}");
        obj.name = abilityName;
        //var ec = new EnrichedCoroutine(BattleController.Instance, AbilityRecipe.LoadResource(abilityName));
        //yield return ec.coroutine;
        //var recipe = (AbilityRecipe)ec.result;
        //if (recipe == null)
        //{
        //    Debug.LogError("No Unit Recipe for name: " + abilityName);
        //    yield return null;
        //}
        //var cd = new EnrichedCoroutine(BattleController.Instance, Create(recipe));
        //yield return cd.coroutine;

        yield return obj;
    }

    //public static IEnumerator Create(AbilityRecipe recipe)
    //{
    //    GameObject obj = InstantiatePrefab("Abilities/Ability");
    //    obj.name = recipe.RecipeName;

    //    var ability = obj.AddComponent<Ability>();

    //    var mp = obj.AddComponent<AbilityMagicCost>();
    //    mp.Amount = recipe.MP;
    //    AddDamage(obj, recipe.Category, recipe.Power);
    //    AddHitRate(obj, recipe.Accuracy);

    //    var des = obj.AddComponent<Description>();
    //    des.Value = recipe.Description;
    //    var tags = obj.AddComponent<Tags>();
    //    tags.Values = recipe.Tags;

    //    yield return BattleController.Instance.StartCoroutine(AddAttribute(obj, recipe.Attribute));

    //    AddAdditions(obj, recipe.Additions);

    //    yield return obj;
    //}

    private static GameObject InstantiatePrefab(string name)
    {
        GameObject prefab = Resources.Load<GameObject>(name);
        if (prefab == null)
        {
            Debug.LogError("No Prefab for name: " + name);
            return new GameObject(name);
        }
        GameObject instance = GameObject.Instantiate(prefab);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance;
    }

    //private static void AddDamage(GameObject obj, string c, int pow)
    //{
    //    if (c.ToUpper() == "STATUS")
    //        return;
    //    else if (c.ToUpper() == "PHYSICAL")
    //    {
    //        var p = obj.AddComponent<PhysicalAbilityPower>();
    //        p.Level = pow;

    //        var a = obj.AddComponent<MeleeAnimation>();
    //    }
    //    else
    //    {
    //        var p = obj.AddComponent<MagicalAbilityPower>();
    //        p.Level = pow;

    //        var a = obj.AddComponent<MagicAnimation>();
    //        a.Bullet = BattleController.InstantiatePrefab<Transform>("GenericAnimations/Bullet");
    //        a.Explosion = BattleController.InstantiatePrefab("GenericAnimations/Explosion");
    //    }

    //    GameObject main = new GameObject("Damage");
    //    main.transform.SetParent(obj.transform);
    //    main.AddComponent<DamageAbilityEffect>();

    //    //TODO Add multi target attacks
    //    main.AddComponent<DefaultAbilityEffectTarget>();
    //}

    //private static void AddHitRate(GameObject obj, int accuracy)
    //{
    //    if (accuracy < 0)
    //    {
    //        obj.AddComponent<FullTypeHitRate>();
    //    }
    //    else
    //    {
    //        var hr = obj.AddComponent<ATypeHitRate>();
    //        hr.HitRate = accuracy;
    //    }
    //}


    //private static IEnumerator AddAttribute(GameObject obj, string a)
    //{
    //    var attr = obj.AddComponent<AbilityAttribute>();
    //    var ec = new EnrichedCoroutine(BattleController.Instance, Attribute.LoadResource(a));
    //    yield return ec.coroutine;
    //    Attribute r = (Attribute)ec.result;
    //    if (r == null)
    //    {
    //        Debug.LogError("No Attribute for name: " + a);
    //    }
    //    else
    //    {
    //        attr.Attribute = r;
    //    }
    //}

    //private static void AddAdditions(GameObject obj, JSONClass[] additions)
    //{
    //    foreach (JSONClass json in additions)
    //    {
    //        switch (json["type"].QuotelessValue.ToLower())
    //        {
    //            case "stat-change":
    //                AddStatChange(obj, json);
    //                break;
    //            case "apply-status":
    //                ApplyStatus(obj, json);
    //                break;
    //            case "recharge":
    //            case "charge":
    //            case "multi-hit":
    //            case "critical-rate":
    //            case "take-item":
    //            case "interrupt":
    //            case "repeat-move":
    //            case "protection":
    //            case "switch-out":
    //            case "heal":
    //            case "flinch":
    //            default:
    //                Debug.LogError($"Have not created type '{json["type"]}' for abilities!");
    //                break;
    //        }
    //    }
    //}

    //private static void AddStatChange(GameObject obj, JSONClass json)
    //{
    //    GameObject main = new GameObject("StatChange");
    //    main.transform.SetParent(obj.transform);
    //    var st = main.AddComponent<ChangeStageEffect>();
    //    var amount = json["amount"].AsInt;
    //    st.IsIncrease = amount > 0;
    //    st.NumberOfStagesChange = Math.Abs(amount);
    //    st.StatName = (StatTypes)Enum.Parse(typeof(StatTypes), json["stat"].QuotelessValue);

    //    var target = json["target"];
    //    if (target != null)
    //    {
    //        if (target.QuotelessValue == "self")
    //        {
    //            main.AddComponent<SelfAbilityEffectTarget>();
    //        }
    //        else
    //        {
    //            main.AddComponent<DefaultAbilityEffectTarget>();
    //        }
    //    }
    //    else
    //    {
    //        main.AddComponent<DefaultAbilityEffectTarget>();
    //    }

    //    var chance = json["chance"];
    //    if (chance != null)
    //    {
    //        var c = main.AddComponent<PercentChance>();
    //        c.Chance = chance.AsInt;
    //    }
    //}

    //private static void ApplyStatus(GameObject obj, JSONClass json)
    //{
    //    GameObject main = new GameObject("ApplyStatus");
    //    main.transform.SetParent(obj.transform);
    //    var st = main.AddComponent<InflictAbilityEffect>();

    //    st.StatusName = json["status"].QuotelessValue + "StatusEffect";
    //    var duration = json["duration"];
    //    if (duration != null)
    //    {
    //        st.Duration = duration.AsInt;
    //    }

    //    var target = json["target"];
    //    if (target != null)
    //    {
    //        if (target.QuotelessValue == "self")
    //        {
    //            main.AddComponent<SelfAbilityEffectTarget>();
    //        }
    //        else
    //        {
    //            main.AddComponent<DefaultAbilityEffectTarget>();
    //        }
    //    }
    //    else
    //    {
    //        main.AddComponent<DefaultAbilityEffectTarget>();
    //    }

    //    var chance = json["chance"];
    //    if (chance != null)
    //    {
    //        var c = main.AddComponent<PercentChance>();
    //        c.Chance = chance.AsInt;
    //    }
    //}
}
