﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Collections.Specialized;

//public class ModelRecipe 
//{
//    public string RecipeName;
//    public string ModelData;

//    public const string ModelRecipeKey = "ModelRecipes";

//    public Dictionary<string, ModelRecipe> LoadAllResources()
//    {
//        if (InformationCache.ContainsKey(ModelRecipeKey))
//        {
//            var o = InformationCache.Get<Dictionary<string, ModelRecipe>>(ModelRecipeKey);

//            return o;
//        }

//        //string readPath = string.Format(Path, Application.dataPath);
//        //string readText = File.ReadAllText(readPath);
//        var json = JSON.Parse("");

//        Dictionary<string, ModelRecipe> resources = null;// ParseTraits(json["traits"].AsArray);

//        InformationCache.Store<Dictionary<string, ModelRecipe>>(ModelRecipeKey, resources);

//        return resources;
//    }

//    public static IEnumerator LoadResource(string key)
//    {
//        var dict = new Dictionary<string, ModelRecipe>();
//        var continuing = true;
//        if (InformationCache.ContainsKey(ModelRecipeKey))
//        {
//            dict = InformationCache.Get<Dictionary<string, ModelRecipe>>(ModelRecipeKey);

//            if (dict.ContainsKey(key))
//            {
//                yield return dict[key];
//                continuing = false;
//            }
//        }

//        if (continuing)
//        {
//            var webRequest = new WebRequest();
//            var url = "http://localhost:13522/api/monster/model?name=" + key;
//            var headers = new NameValueCollection();
//            var ec = new EnrichedCoroutine(BattleController.Instance, webRequest.Get(url, headers));
//            yield return ec.coroutine;

//            var request = (string)ec.result;
//            //var json = JSON.Parse(request);
//            var result = ParseModel(key, request);

//            dict.Add(key, result);
//            InformationCache.Store<Dictionary<string, ModelRecipe>>(ModelRecipeKey, dict);

//            yield return result;
//        }
//    }

//    //static Dictionary<string, ModelRecipe> ParseTraits(JSONArray jArr)
//    //{
//    //    var dict = new Dictionary<string, ModelRecipe>();
//    //    foreach (JSONClass ur in jArr)
//    //    {
//    //        dict.Add(ur["name"], ParseTrait(ur));
//    //    }

//    //    return dict;
//    //}

//    static ModelRecipe ParseModel(string key, string data)
//    {
//        ModelRecipe obj = new ModelRecipe();
//        obj.RecipeName = key;
//        obj.ModelData = data;

//        return obj;
//    }

//    public override string ToString()
//    {
//        return $"{RecipeName} - {ModelData}";
//    }
//}
