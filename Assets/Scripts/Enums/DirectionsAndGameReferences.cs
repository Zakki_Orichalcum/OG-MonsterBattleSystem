﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Directions {
    North,
    NorthEast,
    SouthEast,
    South,
    SouthWest,
    NorthWest
}

public enum Facings
{
    Front,
    Side,
    Back
}

public enum Locomotions
{
    Walk,
    Fly,
    Teleport
}

public enum Alliances
{
    None = 0,
    Neutral = 1 << 0,
    Hero = 1 << 1,
    Enemy = 1 << 2
}

public enum Targets
{
    None,
    Self,
    Ally,
    Foe,
    Tile
}

public enum Drivers
{
    None,
    Human,
    Computer
}

public enum BattleType
{
    SingleBattle = 0,
    DoubleBattle = 1 << 0,
    Trainer = 2 << 0,
    Competitive = 3 << 0,
    Wild = 4 << 0
}
