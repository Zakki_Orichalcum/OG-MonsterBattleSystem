﻿using UnityEngine;
using System.Collections;

public enum StatTypes
{
    LVL, // Level
    EXP, // Experience
    HP,  // Hit Points
    MHP, // Max Hit Points
    MP,  // Magic Points
    MMP, // Max Magic Points
    ATK, // Physical Attack
    DEF, // Physical Defense
    MAT, // Magic Attack
    MDF, // Magic Defense
    EVD, // Evade
    SPD, // Speed,
    BASE_HP,  // Base Hit Points
    BASE_MP,  // Base Magic Points
    BASE_ATK, // Base Physical Attack
    BASE_DEF, // Base Physical Defense
    BASE_MAT, // Base Magic Attack
    BASE_MDF, // Base Magic Defense
    BASE_SPD, // Base Speed,
    EV_HP,  // Effort Hit Points
    EV_MP,  // Effort Magic Points
    EV_ATK, // Effort Physical Attack
    EV_DEF, // Effort Physical Defense
    EV_MAT, // Effort Magic Attack
    EV_MDF, // Effort Magic Defense
    EV_SPD, // Effort Speed,
    IV_HP,  // Effort Hit Points
    IV_MP,  // Individual Magic Points
    IV_ATK, // Individual Physical Attack
    IV_DEF, // Individual Physical Defense
    IV_MAT, // Individual Magic Attack
    IV_MDF, // Individual Magic Defense
    IV_SPD, // Individual Speed,
    HAPPINESS,
    ACC,
    Count
}

[System.Flags]
public enum EquipSlots
{
    None = 0,
    Primary = 1 << 0,   // usually a weapon (sword etc)
    Secondary = 1 << 1,  // usually a shield, but could be another sword (dual-wield) or occupied by two-handed weapon
    Head = 1 << 2,    // helmet, hat, etc
    Body = 1 << 3,    // body armor, robe, etc
    Accessory = 1 << 4  // ring, belt, etc
}