﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DirectionExtensions {

    public const float MIN_CUTOFF = 0.45f;
    public const float TURN_RADIUS = 60f;
    public const float TURN_OFFSET = 60f;

    public static Directions GetDirection(this Vector2 p)
    {
        if (p.y > MIN_CUTOFF && (Mathf.Abs(p.x) < MIN_CUTOFF))
            return Directions.North;

        if (p.y < -MIN_CUTOFF && (Mathf.Abs(p.x) < MIN_CUTOFF))
            return Directions.South;

        if (p.x > MIN_CUTOFF && p.y > 0)
            return Directions.NorthEast;

        if (p.x > MIN_CUTOFF && p.y < 0)
            return Directions.SouthEast;
        
        if (p.x < -MIN_CUTOFF && p.y < 0)
            return Directions.SouthWest;

        return Directions.NorthWest;
    }

    public static Directions GetDirection(this HexPoint p)
    {
        if (p.z > 0 && p.y < 0)
            return Directions.North;

        if (p.z < 0 && p.x > 0)
            return Directions.NorthEast;

        if (p.y < 0 && p.x > 0)
            return Directions.SouthEast;

        if (p.y > 0 && p.z < 0)
            return Directions.South;

        if (p.x < 0 && p.z > 0)
            return Directions.SouthWest;

        return Directions.NorthWest;
    }

	//public static Directions GetDirection (this Tile t1, Tile t2)
 //   {
 //       if (t1.Pos.x < t2.Pos.x && t1.Pos.y > t2.Pos.y)
 //           return Directions.North;

 //       if (t1.Pos.z > t2.Pos.z && t1.Pos.x < t2.Pos.x)
 //           return Directions.NorthEast;

 //       if (t1.Pos.y < t2.Pos.y && t1.Pos.z > t2.Pos.z)
 //           return Directions.SouthEast;

 //       if (t1.Pos.y < t2.Pos.y && t1.Pos.x > t2.Pos.x)
 //           return Directions.South;

 //       if (t1.Pos.x > t2.Pos.x && t1.Pos.z < t2.Pos.z)
 //           return Directions.SouthWest;

 //       return Directions.NorthWest;
 //   }

    public static Vector3 ToEuler(this Directions d)
    {
        return new Vector3(0, ((int)d * TURN_RADIUS) + TURN_OFFSET, 0);
    }

    public static HexPoint GetNormal(this Directions dir)
    {
        switch (dir)
        {
            case Directions.North:
                return Hex.Neighbors[0];
            case Directions.NorthEast:
                return Hex.Neighbors[1];
            case Directions.SouthEast:
                return Hex.Neighbors[2];
            case Directions.South:
                return Hex.Neighbors[3];
            case Directions.SouthWest:
                return Hex.Neighbors[4];
            default: //NorthWest
                return Hex.Neighbors[5];
        }
    }
}

public static class PointExtensions
{
    public static Point ToPoint(this Vector2 vect)
    {
        return new Point((int)vect.x, (int)vect.y);
    }
}
