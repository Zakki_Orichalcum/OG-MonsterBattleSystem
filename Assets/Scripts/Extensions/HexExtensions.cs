﻿using UnityEngine;
using System.Collections;

public static class HexExtensions
{

    public static Point ToAxialPoint(this HexPoint p)
    {
        return new Point(p.x, p.z);
    }

    public static HexPoint ToCubePoint(this Point p)
    {
        var x = p.x;
        var z = p.y;

        return new HexPoint(x, -x-z, z);
    }

    public static Point ToAxialPoint(this Vector2 p, float size = 1)
    {
        return p.ToCubePoint(size).ToAxialPoint();
    }

    public static HexPoint ToCubePoint(this Vector2 p, float size = 1)
    {
        return HexPoint.FromPixelPoints(p.x, p.y, size);
    }

    public static Vector2 ToVector2(this Point p, float size = 1f)
    {
        var x = size * ((3.0f / 2.0f) * p.x);
        var y = size * ((Mathf.Sqrt(3.0f) / 2.0f) * p.x) + size * (Mathf.Sqrt(3.0f) * p.y);

        return new Vector2(x, y);
    }
}
