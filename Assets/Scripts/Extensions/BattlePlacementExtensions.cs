﻿using System.Collections.Generic;
using System.Linq;

public static class BattlePlacementExtensions
{
    public static List<Unit> GetContents(this IEnumerable<BattlePlacement> placements)
    {
        return placements.Select(x => x.Content).ToList();
    }
}