﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityMenuPanelController : MonoBehaviour
{
    private const string ShowKey = "Show";
    private const string HideKey = "Hide";
    private const string EntryPoolKey = "AbilityMenuPanel.Entry";
    private const int MenuCount = 4;

    [SerializeField]
    private GameObject EntryPrefab;
    [SerializeField]
    private Text TitleLabel;
    [SerializeField]
    private Panel Panel;
    [SerializeField]
    private GameObject Canvas;
    private List<AbilityMenuEntry> MenuEntries = new List<AbilityMenuEntry>(MenuCount);
    public int Selection { get; private set; }

    private void Awake()
    {
        GameObjectPoolController.AddEntry(EntryPoolKey, EntryPrefab, MenuCount, int.MaxValue);
    }

    private void Start()
    {
        Panel.SetPosition(HideKey, false);
        Canvas.SetActive(false);
    }

    private AbilityMenuEntry Dequeue()
    {
        Poolable p = GameObjectPoolController.Dequeue(EntryPoolKey);
        AbilityMenuEntry entry = p.GetComponent<AbilityMenuEntry>();
        entry.transform.SetParent(Panel.transform, false);
        entry.transform.localScale = Vector3.one;
        entry.gameObject.SetActive(true);
        entry.Reset();

        return entry;
    }

    private void Enqueue(AbilityMenuEntry entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        GameObjectPoolController.Enqueue(p);
    }

    private void Clear()
    {
        for(int i = MenuEntries.Count -1; i >= 0; --i)
        {
            Enqueue(MenuEntries[i]);
        }
        MenuEntries.Clear();
    }

    private Tweener TogglePos(string pos)
    {
        Tweener t = Panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;

        return t;
    }

    private bool SetSelection(int value)
    {
        if (value >= MenuEntries.Count || MenuEntries[value].IsLocked)
            return false;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = false;

        Selection = value;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = true;

        return true;
    }

    public void Next()
    {
        for(int i = Selection + 1; i < Selection + MenuEntries.Count; ++i)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                break;
        }
    }

    public void Previous()
    {
        //for (int i = Selection - 1 + MenuEntries.Count; i < Selection ; i--)
        //{
        //    Debug.Log(Selection + ", " + i);
        //    int index = i % MenuEntries.Count;
        //    if (SetSelection(index))
        //        break;
        //}

        int i = Selection - 1 + MenuEntries.Count; var breakout = false;
        while(i > Selection && !breakout)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                breakout = true;
            i--;
        }
    }

    public void Show(string title, List<string> options)
    {
        Canvas.SetActive(true);
        Clear();
        TitleLabel.text = title;

        for(int i = 0; i < options.Count; ++i)
        {
            AbilityMenuEntry entry = Dequeue();
            entry.Title = options[i];
            MenuEntries.Add(entry);
        }

        SetSelection(0);
        TogglePos(ShowKey);
    }

    public void Hide()
    {
        Tweener t = TogglePos(HideKey);
        t.completedEvent += delegate (object sender, System.EventArgs e)
        {
            if (Panel.CurrentPosition == Panel[HideKey])
            {
                Clear();
                Canvas.SetActive(false);
            }
        };
    }

    public void SetLocked(int index, bool value)
    {
        if(index < 0 || index >= MenuEntries.Count)
        {
            return;
        }

        MenuEntries[index].IsLocked = value;
        if (value && Selection == index)
            Next();
    }
}
