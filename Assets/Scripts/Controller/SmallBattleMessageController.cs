﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class SmallBattleMessageController : MonoBehaviour
{
    [SerializeField] private Text Label;
    [SerializeField] private GameObject Canvas;
    [SerializeField] private CanvasGroup Group;

    private EasingControl ec;

    private float HidingTiming = -1;

    void Awake()
    {
        ec = gameObject.AddComponent<EasingControl>();
        ec.duration = 0.5f;
        ec.equation = EasingEquations.EaseInOutQuad;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;
    }

    public void Display(string message)
    {
        Group.alpha = 0;
        Label.text = message;
        StartCoroutine(Sequence());
    }

    public void Display(string message, float time)
    {
        HidingTiming = time;
        Display(message);
    }

    public void Hide()
    {
        StartCoroutine(HideSequence());
    }

    void OnUpdateEvent(object sender, EventArgs e)
    {
        Group.alpha = ec.currentValue;
    }

    IEnumerator Sequence()
    {
        Canvas.SetActive(true);
        ec.Play();

        while (ec.IsPlaying)
            yield return null;

        if(HidingTiming > 0)
        {
            yield return new WaitForSeconds(HidingTiming);

            yield return StartCoroutine(HideSequence());
        }
    }

    IEnumerator HideSequence()
    {
        ec.Reverse();

        while (ec.IsPlaying)
            yield return null;

        Canvas.SetActive(false);
    }
}