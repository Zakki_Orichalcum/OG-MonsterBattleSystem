﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ItemMenuController : MonoBehaviour
{
    private const string ShowKey = "Show";
    private const string HideKey = "Hide";
    
    public GameObject Canvas;
    public Panel Panel;

    public TopMenuController TopMenu;
    public ScrollableMenuController ItemMenu;
    public ItemDescription ItemDescription;

    public List<Item> Items => BattleController.Instance.Player.Items;

    public void Awake()
    {
        ItemDescription.gameObject.SetActive(false);
    }

    private void Start()
    {
        Panel.SetPosition(HideKey, false);
        Canvas.SetActive(false);
    }

    private Tweener TogglePos(string pos)
    {
        Tweener t = Panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;

        return t;
    }

    public void NextItemCategory()
    {
        TopMenu.Next();
        SwitchToNewItemCategory();
    }

    public void PreviousItemCategory()
    {
        TopMenu.Previous();
        SwitchToNewItemCategory();
    }

    public void SwitchToNewItemCategory()
    {
        var cat = TopMenu.MenuEntries[TopMenu.Selection].Name.text;

        var ls = Items.Where(x => x.Category == cat).Select(y => (IMenuItem)y).ToList();

        ItemMenu.Hide();
        ItemMenu.Show(ls);
    }

    public void Show()
    {
        Canvas.SetActive(true);

        var categories = new List<string>();
        foreach(var i in Items)
        {
            if (!categories.Contains(i.Category))
                categories.Add(i.Category);
        }

        TopMenu.Show(categories);

        TogglePos(ShowKey);

        SwitchToNewItemCategory();
    }

    public void Hide()
    {
        Canvas.SetActive(false);
    }
}
