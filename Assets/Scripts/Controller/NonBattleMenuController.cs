﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NonBattleMenuController : StateMachine
{
    public Camera Camera;
    
    public bool AllowCancel;

    public IMenuedBattleState owner;
    public List<Unit> Units => BattleController.Instance.PlayersUnits;
    public Transform UnitDisplay;
    public TeamMenuController TeamMenuController;
    public AbilityMenuPanelController UnitMenuController;
    public StatScreenController StatScreenController;
    public AbilitiesMenuController AbilitiesMenuController;
    public ItemMenuController ItemMenuController;

    public int Selection => TeamMenuController.Selection;

    private void Start()
    {
        Camera.gameObject.SetActive(false);
    }

    public void Show(IMenuedBattleState o, bool showItems, bool allowCancel = true)
    {
        owner = o;
        AllowCancel = allowCancel;
        Camera.gameObject.SetActive(true);

        if (showItems)
            ChangeState<ShowItemMenuState>();
        else
            ChangeState<ShowUnitModelMenuState>();
    }

    public void Hide()
    {
        ChangeState<ClosedMenuState>();
        //Tweener t = TogglePos(HideKey);
        //t.completedEvent += delegate (object sender, System.EventArgs e)
        //{
        //    if (Panel.CurrentPosition == Panel[HideKey])
        //    {
        //        Clear();
        //        Canvas.SetActive(false);
        //    }
        //};
        TeamMenuController.Clear();
        Camera.gameObject.SetActive(false);
    }

    public void CancelToBattleState()
    {
        Hide();
        owner.CloseUnitMenu();
    }


    public void SetToSwitch()
    {
        var swi = new SwitchTurnAction();
        swi.Player = BattleController.Instance.Player;
        swi.SwitchFrom = 0;
        swi.SwitchTo = Selection;

        Hide();

        owner.UsingAction(swi);
    }

    public void SetToUseItem()
    {
        var swi = new UseItemAction();
        

        Hide();

        owner.UsingAction(swi);
    }

    public IEnumerator SelectModel()
    {
        UnitDisplay.gameObject.SetActive(true);

        var ui = Units[Selection];
        if (UnitDisplay.childCount > 0)
            UnitDisplay.DestroyChildren();

        GameObject obj = BattleController.InstantiatePrefab("Units/Unit", UnitDisplay);
        var modelName = ui.GetComponentInChildren<Model>().transform.GetChild(0).name;
        GameObject model = BattleController.InstantiatePrefab("UnitModels/" + (string.IsNullOrEmpty(modelName) ? "Default" : modelName));

        yield return null;

        //c.LoadFromJson(readObj.ModelData);
        obj.GetComponentInChildren<Model>().SetModel(model.transform);

        var attr = ui.GetComponent<UnitAttributes>();
        var renderer = model.GetComponentInChildren<MeshRenderer>();
        renderer.material.SetColor("_Color", ui.name.ColorFromWord(attr.Attributes.Length > 1 ?
            attr.Attributes[0].Color.Combine(attr.Attributes[1].Color) : attr.Attributes[0].Color));

        var m = obj.GetComponentInChildren<Model>();

        m.SetModel(model.transform);
        obj.transform.rotation = Quaternion.Euler(0, 220, 0);
    }

    public Animation ExpGain_ForAbilitySelection;
    public Unit Unit_ForAbilitySelection;
    public GameObject Ability_forAbilitySelection;

    public IEnumerator LearnAbility(Animation x, Unit unit, string abilityName)
    {
        Camera.gameObject.SetActive(true);
        ExpGain_ForAbilitySelection = x;
        Unit_ForAbilitySelection = unit;

        var ec = new EnrichedCoroutine(this, AbilityFactory.Create(abilityName));
        yield return ec.coroutine;
        Ability_forAbilitySelection = (GameObject)ec.result;

        var abCatalog = Unit_ForAbilitySelection.GetComponentInChildren<AbilityCatalog>();
        Ability_forAbilitySelection.transform.SetParent(abCatalog.transform);

        ChangeState<ConfirmToLearnAbilityMenuState>();
    }

    public void ForgetAbility(int abilityForgot)
    {
        if(abilityForgot == 5)
        {
            BattleController.Instance.ShowSmallBattleMessage($"Gave up on {Ability_forAbilitySelection.name}.", 2f);
            Ability_forAbilitySelection.Destroy();
        }
        else
        {
            var abCatalog = Unit_ForAbilitySelection.GetComponentInChildren<AbilityCatalog>();
            var ab = abCatalog.GetAbility(abilityForgot);

            BattleController.Instance.ShowSmallBattleMessage($"{Unit_ForAbilitySelection.name} forgot {ab.name} and " +
                $"learned {Ability_forAbilitySelection.name}!", 2f);

            ab.gameObject.Destroy();
        }

        Hide();
        
        //ExpGain_ForAbilitySelection.ToggleWaiting();
    }
}