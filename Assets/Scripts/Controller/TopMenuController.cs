﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TopMenuController : MonoBehaviour
{
    private const string EntryPoolKey = "TopMenu.Entry";
    private const int MenuCount = 3;

    public GameObject Panel;

    public int Selection;

    [SerializeField]
    private TopMenuEntry prefab;
    public List<TopMenuEntry> MenuEntries = new List<TopMenuEntry>();

    private void Awake()
    {
        GameObjectPoolController.AddEntry(EntryPoolKey, prefab.gameObject, MenuCount, int.MaxValue);
    }

    private void Start()
    {
        //Panel.SetPosition(HideKey, false);
        Panel.gameObject.SetActive(false);
    }

    private TopMenuEntry Dequeue()
    {
        Poolable p = GameObjectPoolController.Dequeue(EntryPoolKey);
        TopMenuEntry entry = p.GetComponent<TopMenuEntry>();
        entry.transform.SetParent(Panel.transform, false);
        entry.transform.localScale = Vector3.one;
        entry.gameObject.SetActive(true);
        entry.Reset();

        return entry;
    }

    private void Enqueue(TopMenuEntry entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        GameObjectPoolController.Enqueue(p);
    }

    public void Clear()
    {
        for (int i = MenuEntries.Count - 1; i >= 0; --i)
        {
            Enqueue(MenuEntries[i]);
        }
        MenuEntries.Clear();
    }

    private Tweener TogglePos(string pos)
    {
        Tweener t = null;// Panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;

        return t;
    }

    private bool SetSelection(int value)
    {
        if (value >= MenuEntries.Count || MenuEntries[value].IsLocked)
            return false;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = false;

        Selection = value;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = true;

        return true;
    }

    public void Next()
    {
        for (int i = Selection + 1; i < Selection + MenuEntries.Count; ++i)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                break;
        }
    }

    public void Previous()
    {
        //for (int i = Selection - 1 + MenuEntries.Count; i < Selection ; i--)
        //{
        //    Debug.Log(Selection + ", " + i);
        //    int index = i % MenuEntries.Count;
        //    if (SetSelection(index))
        //        break;
        //}

        int i = Selection - 1 + MenuEntries.Count; var breakout = false;
        while (i > Selection && !breakout)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                breakout = true;
            i--;
        }
    }
    
    public void SetLocked(int index, bool value)
    {
        if (index < 0 || index >= MenuEntries.Count)
        {
            return;
        }

        MenuEntries[index].IsLocked = value;
        if (value && Selection == index)
            Next();
    }

    public void Show(List<string> options)
    {
        Panel.gameObject.SetActive(true);
        if(MenuEntries != null && MenuEntries.Any())
            Clear();

        for (int i = 0; i < options.Count; ++i)
        {
            TopMenuEntry entry = Dequeue();
            entry.Name.text = options[i];
            MenuEntries.Add(entry);
        }

        SetSelection(0);
        //TogglePos(ShowKey);

        SetSelection(0);
    }

    public void Hide()
    {
        //Tweener t = TogglePos(HideKey);
        //t.completedEvent += delegate (object sender, System.EventArgs e)
        //{
        //    if (Panel.CurrentPosition == Panel[HideKey])
        //    {
        //        Clear();
        //        Canvas.SetActive(false);
        //    }
        //};
        //Clear();
        Panel.gameObject.SetActive(false);
    }
}
