﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using OrichalcumGames.Statistics;

/// <summary>
/// This is all things based out of a hypothetical system
/// that would be used for display purposes only
/// </summary>
public class StatScreenController : MonoBehaviour
{
    [SerializeField]
    private GameObject Canvas;

    public StatBundle[] Stats;
    public Text Nature;
    public Text Name;
    public Text Level;
    public Text ExpNumbers;
    public FillBar HealthBar;
    public FillBar ManaBar;
    public FillBar ExpBar;

    private void Start()
    {
        Canvas.SetActive(false);
    }

    public void Show(Unit u)
    {
        Canvas.SetActive(true);
        var nature = u.GetComponent<Nature>();
        var stats = u.GetComponent<IStatistics>();
        var health = u.GetComponent<Health>();

        foreach(var s in Stats)
        {
            s.UpdateFromStats(stats, nature);
        }

        HealthBar.SetPercent((1.0f * stats["HP"]) / stats["MaximumHP"]);
        ManaBar.SetPercent((1.0f * stats["MP"]) / stats["MaximumMP"]);

        Name.text = u.name;
        Nature.text = nature.Name;
        Level.text = stats["LVL"].ToString();
        var exp = stats["EXP"];
        var nexp = stats["NxEXP"];
        var per = (1.0f * exp) / nexp;
        ExpBar.SetPercent(per);
        ExpNumbers.text = $"{exp}/{nexp}";
    }

    public void Hide()
    {
        Canvas.SetActive(false);
    }
}
