﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TeamMenuController : MonoBehaviour
{
    private const string EntryPoolKey = "UnitItemController.Entry";
    private const int MenuCount = 6;

    public const string OnSetSelectionNotification = "TeamMenuController.OnSetSelection";

    public GameObject Canvas;
    public GameObject Panel;

    [SerializeField]
    private Vector3[] UnitMenuSections;

    
    public int Selection;

    [SerializeField]
    private UnitItemController prefab;
    private List<UnitItemController> MenuEntries = new List<UnitItemController>();

    public List<Unit> Units => BattleController.Instance.PlayersUnits;

    private void Awake()
    {
        GameObjectPoolController.AddEntry(EntryPoolKey, prefab.gameObject, MenuCount, int.MaxValue);
    }

    private void Start()
    {
        //Panel.SetPosition(HideKey, false);
        Canvas.SetActive(false);
    }

    private UnitItemController Dequeue()
    {
        Poolable p = GameObjectPoolController.Dequeue(EntryPoolKey);
        UnitItemController entry = p.GetComponent<UnitItemController>();
        entry.transform.SetParent(Panel.transform, false);
        entry.transform.localScale = Vector3.one;
        entry.gameObject.SetActive(true);
        entry.Reset();

        return entry;
    }

    private void Enqueue(UnitItemController entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        GameObjectPoolController.Enqueue(p);
    }

    public void Clear()
    {
        for (int i = MenuEntries.Count - 1; i >= 0; --i)
        {
            Enqueue(MenuEntries[i]);
        }
        MenuEntries.Clear();
    }

    private Tweener TogglePos(string pos)
    {
        Tweener t = null;// Panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;

        return t;
    }

    private bool SetSelection(int value)
    {
        if (value >= MenuEntries.Count || MenuEntries[value].IsLocked)
            return false;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = false;

        Selection = value;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = true;

        this.PostNotification(OnSetSelectionNotification, MenuEntries[Selection]);

        return true;
    }

    public void Next()
    {
        for (int i = Selection + 1; i < Selection + MenuEntries.Count; ++i)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                break;
        }
    }

    public void Previous()
    {
        int i = Selection - 1 + MenuEntries.Count; var breakout = false;
        while (i > Selection && !breakout)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                breakout = true;
            i--;
        }
    }
    
    public void SetLocked(int index, bool value)
    {
        if (index < 0 || index >= MenuEntries.Count)
        {
            return;
        }

        MenuEntries[index].IsLocked = value;
        if (value && Selection == index)
            Next();
    }

    public void Show()
    {
        Canvas.SetActive(true);
        if(MenuEntries != null && MenuEntries.Any())
            Clear();

        var locked = new List<int>();

        for (int i = 0; i < Units.Count; ++i)
        {
            var unit = Units[i];
            UnitItemController entry = Dequeue();
            entry.Rect.anchoredPosition = UnitMenuSections[i];
            entry.SetUnit(unit);
            MenuEntries.Add(entry);

            if (unit.GetComponentInChildren<KnockOutStatusEffect>() != null)
            {
                locked.Add(i);
            }
        }

        SetSelection(0);

        foreach (var i in locked)
            SetLocked(i, true);

        //TogglePos(ShowKey);
    }

    public void Hide()
    {
        //Tweener t = TogglePos(HideKey);
        //t.completedEvent += delegate (object sender, System.EventArgs e)
        //{
        //    if (Panel.CurrentPosition == Panel[HideKey])
        //    {
        //        Clear();
        //        Canvas.SetActive(false);
        //    }
        //};
        //Clear();
        Canvas.SetActive(false);
    }
}
