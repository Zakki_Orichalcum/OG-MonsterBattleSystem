﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : StateMachine {

    public bool Debugging;

    public CameraRig CameraRig;
    public GameObject HeroPrefab;
    public GameObject ItemPrefab;
    public AbilityMenuPanelController AbilityMenuPanelController;
    public StatPanelController StatPanelController;
    public List<Unit> Units = new List<Unit>();
    public IEnumerator Round;
    public Player Player;
    public ComputerPlayer Opponent;
    public Transform ObjectPool;
    public BattleMessageController BattleMessageController;
    public SmallBattleMessageController SmallBattleMessageController;
    public AutoStatusController AutoStatusController;
    public StatGrowthPanelController StatGrowthPanel;

    public GameObject NonBattleMenu;
    public NonBattleMenuController NonBattleMenuController;

    public List<Turn> Turns = new List<Turn>();
    public Turn CurrentTurn;

    public List<Animation> AnimationQueue { get; set; }

    public BattleType BattleType { get; private set; }

    public Unit CurrentUnit;
    public List<Unit> PlayersUnits => Player.Units;
    public List<Unit> OpponentsUnits => Opponent.Units;

    public BattlePlacement[] BattlePlacements;
    public string TurnOrderComparisonStat;

    private static BattleController battleController;

    public static BattleController Instance
    {
        get
        {
            if (!battleController)
            {
                battleController = FindObjectOfType(typeof(BattleController)) as BattleController;

                if (!battleController)
                {
                    Debug.LogError("There needs to be one active BattleController script on a GameObject in your scene.");
                }
            }

            return battleController;
        }
    }

    public void Start()
    {
        if (Debugging)
            return;
        AnimationQueue = new List<Animation>();

    }

    public void Init(EncounterInformation info)
    {
        BattleType = info.BattleType;

        if (IsWildBattle)
            Opponent = gameObject.AddComponent<WildComputerPlayer>();

        Player = GameManager.Instance.Player;

        ChangeState<InitBattleState>();
    }

    public bool IsSingleBattle => BattleType.HasFlag(BattleType.SingleBattle);
    public bool IsCompetitiveBattle => BattleType.HasFlag(BattleType.Competitive);
    public bool IsWildBattle => BattleType.HasFlag(BattleType.Wild);

    public Coroutine LaunchCoroutine(IEnumerator coroutine)
    {
        return StartCoroutine(coroutine);
    }

    public void AddToAnimationQueue(Animation a)
    {
        AnimationQueue.Add(a);
    }

    public Player GetOpponent(Player p)
    {
        if (p == Player)
            return Opponent;
        else
            return Player;
    }

    public void RetrieveUnit(Unit u, Vector3 p, Quaternion q)
    {
        var model = u.GetComponentInChildren<Model>();
        model.ModelObject.SetActive(true);
        model.Mesh.enabled = false;
        u.transform.SetPositionAndRotation(p, q);
    }

    public void RemoveUnitFromView(Unit u)
    {
        var model = u.GetComponentInChildren<Model>();
        model.Mesh.enabled = false;
        model.ModelObject.SetActive(false);
        u.transform.SetPositionAndRotation(ObjectPool.transform.position, ObjectPool.transform.rotation);
    }

    public void ShowBattleMessage(string str, string str2 = "")
    {
        BattleMessageController.Display(str, str2);
    }

    public void ShowSmallBattleMessage(string str)
    {
        SmallBattleMessageController.Display(str);
    }

    public void ShowSmallBattleMessage(string str, float time)
    {
        SmallBattleMessageController.Display(str, time);
    }

    public void HideSmallBattleMessage()
    {
        SmallBattleMessageController.Hide();
    }

    public static GameObject InstantiatePrefab(string name, Transform t = null)
    {
        GameObject prefab = Resources.Load<GameObject>(name);
        if (prefab == null)
        {
            Debug.LogError("No Prefab for name: " + name);
            return new GameObject(name);
        }
        GameObject instance = GameObject.Instantiate(prefab, t == null ? BattleController.Instance.transform : t);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance;
    }

    public static T InstantiatePrefab<T>(string name, Transform t = null)
    {
        GameObject prefab = Resources.Load<GameObject>(name);
        if (prefab == null)
        {
            throw new KeyNotFoundException(name);
        }
        GameObject instance = GameObject.Instantiate(prefab, t == null ? BattleController.Instance.transform : t);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance.GetComponent<T>();
    }
}
