﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using OrichalcumGames.Statistics;

public class AbilitiesMenuController : MonoBehaviour
{
    private const string ShowKey = "Show";
    private const string HideKey = "Hide";
    private const string EntryPoolKey = "AbilitiesMenu.Entry";
    private const int MenuCount = 4;

    [SerializeField]
    private Vector3[] AbilityMenuSections;

    public Text Name;
    public Text Level;
    public Text ExpNumbers;
    public FillBar ExpBar;
    public Text TraitName;
    public Text TraitDescription;

    [SerializeField]
    private GameObject EntryPrefab;
    [SerializeField]
    private Panel Panel;
    [SerializeField]
    private GameObject Canvas;
    private List<AbilitiesMenuEntry> MenuEntries = new List<AbilitiesMenuEntry>(MenuCount);
    public int Selection { get; private set; }
    public AbilityDisplay AbilityDisplay;
    public bool IsUsingACostSystem;

    private void Awake()
    {
        GameObjectPoolController.AddEntry(EntryPoolKey, EntryPrefab, MenuCount, int.MaxValue);
    }

    private void Start()
    {
        Panel.SetPosition(HideKey, false);
        Canvas.SetActive(false);
    }

    private AbilitiesMenuEntry Dequeue()
    {
        Poolable p = GameObjectPoolController.Dequeue(EntryPoolKey);
        AbilitiesMenuEntry entry = p.GetComponent<AbilitiesMenuEntry>();
        entry.transform.SetParent(Panel.transform, false);
        entry.transform.localScale = Vector3.one;
        entry.gameObject.SetActive(true);
        entry.Reset();

        return entry;
    }

    private void Enqueue(AbilitiesMenuEntry entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        GameObjectPoolController.Enqueue(p);
    }

    private void Clear()
    {
        for (int i = MenuEntries.Count - 1; i >= 0; --i)
        {
            Enqueue(MenuEntries[i]);
        }
        MenuEntries.Clear();
    }

    private Tweener TogglePos(string pos)
    {
        Tweener t = Panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;

        return t;
    }

    private bool SetSelection(int value)
    {
        if (value >= MenuEntries.Count || MenuEntries[value].IsLocked)
            return false;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = false;

        Selection = value;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = true;

        AbilityDisplay.SetAbility(MenuEntries[Selection].Ability);

        return true;
    }

    public void Next()
    {
        for (int i = Selection + 1; i < Selection + MenuEntries.Count; ++i)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                break;
        }
    }

    public void Previous()
    {
        //for (int i = Selection - 1 + MenuEntries.Count; i < Selection ; i--)
        //{
        //    Debug.Log(Selection + ", " + i);
        //    int index = i % MenuEntries.Count;
        //    if (SetSelection(index))
        //        break;
        //}

        int i = Selection - 1 + MenuEntries.Count; var breakout = false;
        while (i > Selection && !breakout)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                breakout = true;
            i--;
        }
    }

    public void Show(Unit u)
    {
        Canvas.SetActive(true);
        Clear();
        var abilityCatalog = u.GetComponentInChildren<AbilityCatalog>();
        var stats = u.GetComponent<IStatistics>();
        var trait = u.GetComponentInChildren<Trait>();

        TraitName.text = trait.Name;
        TraitDescription.text = trait.Description;
        Name.text = u.name;
        Level.text = stats["LVL"].ToString();
        var exp = stats["EXP"];
        var nexp = stats["NxEXP"];
        var per = exp / nexp;
        ExpBar.SetPercent(per);
        ExpNumbers.text = $"{exp}/{nexp}";

        var locked = new List<int>();

        for (int i = 0; i < abilityCatalog.AbilityCount(); ++i)
        {
            AbilitiesMenuEntry entry = Dequeue();
            entry.Rect.anchoredPosition = AbilityMenuSections[i];
            var ab = abilityCatalog.GetAbility(i);
            entry.SetAbility(ab);
            MenuEntries.Add(entry);
            if(IsUsingACostSystem)
            {
                var manaCost = ab.GetComponent<AbilityMagicCost>();
                if (stats[manaCost.Stat] < manaCost.Amount)
                {
                    locked.Add(i);
                }
            }
            else
            {
                var pp = ab.GetComponent<AbilityPowerPoints>();

                if(!pp.CanStillUse())
                {
                    locked.Add(i);
                }
            }
        }

        SetSelection(0);

        foreach (var i in locked)
            SetLocked(i, true);

        TogglePos(ShowKey);
    }

    public void Hide()
    {
        Tweener t = TogglePos(HideKey);
        t.completedEvent += delegate (object sender, System.EventArgs e)
        {
            if (Panel.CurrentPosition == Panel[HideKey])
            {
                Clear();
                Canvas.SetActive(false);
            }
        };
    }

    public void SetLocked(int index, bool value)
    {
        if (index < 0 || index >= MenuEntries.Count)
        {
            return;
        }

        MenuEntries[index].IsLocked = value;
        if (value && Selection == index)
            Next();
    }
}
