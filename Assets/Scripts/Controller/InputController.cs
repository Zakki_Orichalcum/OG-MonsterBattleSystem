﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static event EventHandler<MultiAxisInputArgs> LeftStickEvent;
    public static event EventHandler<MultiAxisInputArgs> SmoothLeftStickEvent;
    public static event EventHandler<InputArgs> LeftStickButtonEvent;
    public static event EventHandler<MultiAxisInputArgs> RightStickEvent;
    public static event EventHandler<MultiAxisInputArgs> SmoothRightStickEvent;
    public static event EventHandler<InputArgs> RightStickButtonEvent;

    public static event EventHandler<InputArgs> LeftStickButtonPressedEvent;
    public static event EventHandler<InputArgs> RightStickButtonPressedEvent;

    public static event EventHandler<InputArgs> LeftShoulder1Event;
    public static event EventHandler<AxisInputArgs> LeftShoulder2Event;
    public static event EventHandler<InputArgs> RightShoulder1Event;
    public static event EventHandler<AxisInputArgs> RightShoulder2Event;

    public static event EventHandler<MultiAxisInputArgs> DpadEvent;

    public static event EventHandler<InputArgs> LeftShoulder1PressedEvent;
    public static event EventHandler<AxisInputArgs> LeftShoulder2PressedEvent;
    public static event EventHandler<InputArgs> RightShoulder1PressedEvent;
    public static event EventHandler<AxisInputArgs> RightShoulder2PressedEvent;

    public static event EventHandler<MultiAxisInputArgs> DpadPressedEvent;

    public static event EventHandler<InputArgs> InputButtonTopEvent;   //i.e. Triangle
    public static event EventHandler<InputArgs> InputButtonDownEvent;  //i.e. X
    public static event EventHandler<InputArgs> InputButtonRightEvent; //i.e. Circle
    public static event EventHandler<InputArgs> InputButtonLeftEvent;  //i.e. Square
    public static event EventHandler<InputArgs> InputButtonTopPressedEvent;   //i.e. Triangle
    public static event EventHandler<InputArgs> InputButtonDownPressedEvent;  //i.e. X
    public static event EventHandler<InputArgs> InputButtonRightPressedEvent; //i.e. Circle
    public static event EventHandler<InputArgs> InputButtonLeftPressedEvent;  //i.e. Square

    public static event EventHandler<InputArgs> CenterButtonLeftEvent;  //i.e. Select
    public static event EventHandler<InputArgs> CenterButtonRightEvent;  //i.e. Start
    public static event EventHandler<InputArgs> CenterButtonLeftPressedEvent;  //i.e. Select
    public static event EventHandler<InputArgs> CenterButtonRightPressedEvent;  //i.e. Start

    private MultiAxisWatcher LeftStickInputWatcher;
    private InputWatcher LeftStickButtonInputWatcher;
    private MultiAxisWatcher RightStickInputWatcher;
    private InputWatcher RightStickButtonInputWatcher;

    private InputWatcher LeftShoulder1InputWatcher;
    private AxisWatcher LeftShoulder2InputWatcher;
    private InputWatcher RightShoulder1InputWatcher;
    private AxisWatcher RightShoulder2InputWatcher;

    private MultiAxisWatcher DpadInputWatcher;

    private InputWatcher InputButtonTopInputWatcher;// i.e. Triangle
    private InputWatcher InputButtonDownInputWatcher;// i.e. X
    private InputWatcher InputButtonRightInputWatcher;// i.e. Circle
    private InputWatcher InputButtonLeftInputWatcher;// i.e. Square

    private InputWatcher CenterButtonLeftInputWatcher;// i.e. Select
    private InputWatcher CenterButtonRightInputWatcher;// i.e. Start

    public float LeftStickDeadZone { get; private set; }
    public float RightStickDeadZone { get; private set; }
    public float RightShoulder2DeadZone { get; private set; }
    public float LeftShoulder2DeadZone { get; private set; }
    public float DPadDeadZone { get; private set; }

    void Start()
    {
        LeftStickDeadZone = 0.3f;
        RightStickDeadZone = 0f;
        LeftShoulder2DeadZone = 0f;
        RightShoulder2DeadZone = 0f;
        DPadDeadZone = 0f;

        LeftStickInputWatcher = new MultiAxisWatcher("HorizontalL", "VerticalL", LeftStickDeadZone);
        LeftStickButtonInputWatcher = new InputWatcher("AxisLButton");
        RightStickInputWatcher = new MultiAxisWatcher("HorizontalR", "VerticalR", RightStickDeadZone);
        RightStickButtonInputWatcher = new InputWatcher("AxisRButton");

        LeftShoulder1InputWatcher = new InputWatcher("ShoulderLButton");
        LeftShoulder2InputWatcher = new AxisWatcher("ShoulderLAxis", LeftShoulder2DeadZone);
        RightShoulder1InputWatcher = new InputWatcher("ShoulderRButton");
        RightShoulder2InputWatcher = new AxisWatcher("ShoulderRAxis", RightShoulder2DeadZone); ;

        DpadInputWatcher = new MultiAxisWatcher("HorizontalDPAD", "VerticalDPAD", DPadDeadZone);

        InputButtonTopInputWatcher = new InputWatcher("InputTopButton");// i.e. Triangle
        InputButtonDownInputWatcher = new InputWatcher("InputBottomButton");// i.e. X
        InputButtonRightInputWatcher = new InputWatcher("InputRightButton");// i.e. Circle
        InputButtonLeftInputWatcher = new InputWatcher("InputLeftButton");// i.e. Square

        CenterButtonLeftInputWatcher = new InputWatcher("CenterRButton");// i.e. Select
        CenterButtonRightInputWatcher = new InputWatcher("CenterLButton");// i.e. Start
    }

    void Update()
    {
        var lsiw = LeftStickInputWatcher.Update(); if (lsiw.Item2) { LeftStickEvent?.Invoke(this, lsiw.Item1); }
        if (lsiw.Item3) { SmoothLeftStickEvent?.Invoke(this, lsiw.Item1); }
        var lsbiw = LeftStickButtonInputWatcher.Update();
        if (lsbiw.Item2)
        {
            if(lsbiw.Item1.InputState == InputState.Pressed)
            {
                LeftStickButtonPressedEvent?.Invoke(this, lsbiw.Item1);
            }
            
            LeftStickButtonEvent?.Invoke(this, lsbiw.Item1);
        }
        var rsiw = RightStickInputWatcher.Update(); if (rsiw.Item2) { RightStickEvent?.Invoke(this, rsiw.Item1); }
        if (rsiw.Item3) { SmoothRightStickEvent?.Invoke(this, rsiw.Item1); }
        var rsbiw = RightStickButtonInputWatcher.Update();
        if (rsbiw.Item2) {
            if (rsbiw.Item1.InputState == InputState.Pressed)
            {
                RightStickButtonPressedEvent?.Invoke(this, rsbiw.Item1);
            }

            RightStickButtonEvent?.Invoke(this, rsbiw.Item1);
        }

        var ls1iw = LeftShoulder1InputWatcher.Update();
        if (ls1iw.Item2) {
            if (ls1iw.Item1.InputState == InputState.Pressed)
            {
                LeftShoulder1PressedEvent?.Invoke(this, ls1iw.Item1);
            }
            LeftShoulder1Event?.Invoke(this, ls1iw.Item1);
        }
        var ls2iw = LeftShoulder2InputWatcher.Update();
        if (ls2iw.Item2) {
            if (ls2iw.Item1.InputState == InputState.Pressed)
            {
                LeftShoulder2PressedEvent?.Invoke(this, ls2iw.Item1);
            }
            LeftShoulder2Event?.Invoke(this, ls2iw.Item1);
        }
        var rs1iw = RightShoulder1InputWatcher.Update();
        if (rs1iw.Item2) {
            if (rs1iw.Item1.InputState == InputState.Pressed)
            {
                RightShoulder1PressedEvent?.Invoke(this, rs1iw.Item1);
            }
            RightShoulder1Event?.Invoke(this, rs1iw.Item1);
        }
        var rs2iw = RightShoulder2InputWatcher.Update();
        if (rs2iw.Item2) {
            if (rs2iw.Item1.InputState == InputState.Pressed)
            {
                RightShoulder2PressedEvent?.Invoke(this, rs2iw.Item1);
            }
            RightShoulder2Event?.Invoke(this, rs2iw.Item1);
        }

        var diw = DpadInputWatcher.Update();
        if (diw.Item2) {
            if (diw.Item1.InputState == InputState.Pressed)
            {
                DpadPressedEvent?.Invoke(this, diw.Item1);
            }
            DpadEvent?.Invoke(this, diw.Item1);
        }

        var ibtiw = InputButtonTopInputWatcher.Update();
        if (ibtiw.Item2) {
            if (ibtiw.Item1.InputState == InputState.Pressed)
            {
                InputButtonTopPressedEvent?.Invoke(this, ibtiw.Item1);
            }
            InputButtonTopEvent?.Invoke(this, ibtiw.Item1);
        }
        var ibdiw = InputButtonDownInputWatcher.Update();
        if (ibdiw.Item2)
        {
            if (ibdiw.Item1.InputState == InputState.Pressed)
            {
                InputButtonDownPressedEvent?.Invoke(this, ibdiw.Item1);
            }
            InputButtonDownEvent?.Invoke(this, ibdiw.Item1);
        }
        var ibriw = InputButtonRightInputWatcher.Update();
        if (ibriw.Item2)
        {
            if (ibriw.Item1.InputState == InputState.Pressed)
            {
                InputButtonRightPressedEvent?.Invoke(this, ibriw.Item1);
            }
            InputButtonRightEvent?.Invoke(this, ibriw.Item1);
        }
        var ibliw = InputButtonLeftInputWatcher.Update();
        if (ibliw.Item2)
        {
            if (ibliw.Item1.InputState == InputState.Pressed)
            {
                InputButtonLeftPressedEvent?.Invoke(this, ibliw.Item1);
            }
            InputButtonLeftEvent?.Invoke(this, ibliw.Item1);
        }

        var cbliw = CenterButtonLeftInputWatcher.Update();
        if (cbliw.Item2)
        {
            if (cbliw.Item1.InputState == InputState.Pressed)
            {
                CenterButtonLeftPressedEvent?.Invoke(this, cbliw.Item1);
            }
            CenterButtonLeftEvent?.Invoke(this, cbliw.Item1);
        }
        var cbriw = CenterButtonRightInputWatcher.Update();
        if (cbriw.Item2)
        {
            if (cbriw.Item1.InputState == InputState.Pressed)
            {
                CenterButtonRightPressedEvent?.Invoke(this, cbriw.Item1);
            }
            CenterButtonRightEvent?.Invoke(this, cbriw.Item1);
        }
    }

    class MultiAxisWatcher
    {
        AxisWatcher _axis1;
        AxisWatcher _axis2;

        public MultiAxisWatcher(string axisName, string secondAxisName, float minimumThres)
        {
            _axis1 = new AxisWatcher(axisName, minimumThres);
            _axis2 = new AxisWatcher(secondAxisName, minimumThres);
        }

        public (MultiAxisInputArgs, bool, bool) Update()
        {
            (var args1, var trigger1, var smooth1) = _axis1.Update();
            (var args2, var trigger2, var smooth2) = _axis2.Update();

            InputState mixedState = (int)args2.InputState > (int)args1.InputState ? args2.InputState : args1.InputState;

            return (new MultiAxisInputArgs(
                new Point(args1.Current, args2.Current),
                new Vector2(args1.Raw, args2.Raw),
                mixedState), trigger1 || trigger2, smooth1 || smooth2);
        }
    }

    class AxisWatcher
    {
        float minimumMovementThreshold = 0.15f;
        const float threshold = 0.5f;
        const float rate = 0.25f;
        float _next;
        InputState _state;
        string _axis;

        public AxisWatcher(string axisName, float minimumThres)
        {
            _axis = axisName;
            minimumMovementThreshold = minimumThres;
        }

        public (AxisInputArgs, bool, bool) Update()
        {
            int retValue = 0;
            float raw = Input.GetAxisRaw(_axis);
            bool smooth = Mathf.Abs(raw) > minimumMovementThreshold;
            int value = Mathf.RoundToInt(raw);
            bool trigger = false;

            if (value != 0)
            {
                if (Time.time > _next)
                {
                    retValue = value;
                    _next = Time.time + (_state == InputState.Held ? rate : threshold);
                    _state = (_state == InputState.Pressed || _state == InputState.Held) ? InputState.Held : InputState.Pressed;
                    trigger = true;
                }
            }
            else
            {
                _state = (_state == InputState.Pressed || _state == InputState.Held) ? InputState.Released : InputState.None;
                _next = 0;
                trigger = _state == InputState.Released;
            }

            return (new AxisInputArgs(retValue, raw, _state), trigger, smooth);
        }
    }

    class InputWatcher
    {
        const float threshold = 0.5f;
        const float rate = 0.25f;
        float _next;
        InputState _state;
        string _button;
        bool _previous = false;

        public InputWatcher(string buttonName)
        {
            _button = buttonName;
        }

        public (InputArgs, bool) Update()
        {
            bool retValue = false;
            bool value = Input.GetButton(_button);
            bool trigger = false;

            if (value)
            {
                if (Time.time > _next)
                {
                    retValue = value;
                    _next = Time.time + (_state == InputState.Held ? rate : threshold);
                    _state = (_state == InputState.Pressed || _state == InputState.Held) ? InputState.Held : InputState.Pressed;
                    trigger = true;
                }
            }
            else
            {
                _state = (_state == InputState.Pressed || _state == InputState.Held) ? InputState.Released : InputState.None;
                _next = 0;
                trigger = _state == InputState.Released;
            }

            return (new InputArgs(retValue, _state), trigger);
        }
    }
}


public enum InputState
{
    None = 0,
    Pressed = 1,
    Released = 2,
    Held = 3
}

public class InputArgs
{
    public bool Activated { get; set; }
    public InputState InputState { get; set; }
    public InputArgs(bool active, InputState state)
    {
        Activated = active; InputState = state;
    }
}

public class AxisInputArgs : InputArgs
{
    public int Current { get; set; }
    public float Raw { get; set; }
    public AxisInputArgs(int value, float raw, InputState state) : base(value != 0, state)
    {
        Current = value;
        Raw = raw;
    }
}

public class MultiAxisInputArgs : InputArgs
{
    public Point Current { get; set; }
    public Vector2 Raw { get; set; }

    public MultiAxisInputArgs(Point current, Vector2 raw, InputState state) : base(current.x != 0f || current.y != 0f, state)
    {
        Current = current;
        Raw = raw;
    }
}