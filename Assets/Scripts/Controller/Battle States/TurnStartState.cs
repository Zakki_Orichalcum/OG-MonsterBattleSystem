﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TurnStartState : BattleState
{

    public override void Enter()
    {
        base.Enter();

        Turns.Clear();
        CurrentTurn = new Turn();

        StartCoroutine(Run());
    }

    public IEnumerator Run()
    {
        yield return null;

        //if (owner.HeroPlayer.Units.Count > 0 && (owner.PlayersUnits == null || owner.PlayersUnits.Length == 0))
        //    owner.PlayersUnits = new Unit[] { owner.HeroPlayer.Units.First() };

        //if (owner.CPU.Units.Length > 0 && (owner.OpponentsUnits == null || owner.OpponentsUnits.Length == 0))
        //    owner.OpponentsUnits = new Unit[] { owner.CPU.Units.First() };

        owner.CurrentUnit = PlayersUnit;

        RefreshStatPanels();

        owner.ChangeState<CommandSelectionState>();
    }
}
