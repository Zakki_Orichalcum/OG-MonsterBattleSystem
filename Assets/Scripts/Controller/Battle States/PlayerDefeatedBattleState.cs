﻿using UnityEngine;
using System.Collections;

public class PlayerDefeatedBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();


        StartCoroutine(PlayerDefeated());
    }

    IEnumerator PlayerDefeated()
    {
        owner.ShowSmallBattleMessage($"You were defeated by {owner.Opponent.Name}! ", 2.5f);
        yield return new WaitForSeconds(2.5f);
        owner.ShowSmallBattleMessage($"Returning to Surface!"); 
    }
}
