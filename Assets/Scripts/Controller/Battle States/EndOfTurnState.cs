﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;
using OrichalcumGames.Statistics;

public class EndOfTurnState : BattleState, IMenuedBattleState
{
    public const string CanPerformCheck = "EndOfTurnState.CollectEndOfTurnTriggers";

    private List<EndOfTurnTrigger> EndOfTurnTriggers = new List<EndOfTurnTrigger>();
    private List<SwitchTurnAction> SwitchIns;

    public override void Enter()
    {
        base.Enter();
        SwitchIns = new List<SwitchTurnAction>();

        EndOfTurnTriggers.Clear();

        StartCoroutine(EndOfTurnCleanUp());
    }

    IEnumerator EndOfTurnCleanUp()
    {
        yield return new WaitForSeconds(0.15f);

        this.PostNotification(CanPerformCheck, this);

        EndOfTurnTriggers.Sort();

        foreach(var eot in EndOfTurnTriggers)
        {
            yield return StartCoroutine(eot.Action.Apply());
        }

        if(PlayersUnit.GetComponentInChildren<KnockOutStatusEffect>() != null)
        {
            LaunchTeamMenu();

            while (SwitchIns.Count == 0)
                yield return null;
        }

        if(OpponentsUnit.GetComponentInChildren<KnockOutStatusEffect>() != null)
        {
            SwitchIns.Add(owner.Opponent.SwitchInNext());
        }

        foreach(var swi in SwitchIns)
        {
            yield return StartCoroutine(swi.SwitchIn());
        }

        owner.ChangeState<TurnStartState>();
    }

    public void AddTrigger(EndOfTurnTrigger eot)
    {
        EndOfTurnTriggers.Add(eot);
    }

    public void CloseUnitMenu()
    {
        owner.CameraRig.gameObject.SetActive(true);
    }

    public void UsingAction(ITurnAction swi)
    {
        SwitchIns.Add(swi as SwitchTurnAction);

        owner.NonBattleMenu.SetActive(false);
        owner.CameraRig.gameObject.SetActive(true);
    }

    private void LaunchTeamMenu()
    {
        owner.CameraRig.gameObject.SetActive(false);

        owner.NonBattleMenu.SetActive(true);
        owner.NonBattleMenuController.Show(this, false);
    }
}

public interface IEndOfTurnTriggable
{
    int Priority { get; }
    IEnumerator Apply();
}


public class EndOfTurnTrigger : IComparable<EndOfTurnTrigger>
{
    public Unit Unit;
    public IEndOfTurnTriggable Action;
    public Player Player;

    public int CompareTo(EndOfTurnTrigger other)
    {
        var difference = Action.Priority - other.Action.Priority;

        if(difference != 0)
        {
            return -difference;
        }

        var speed1 = Unit.GetComponent<IStatistics>()[BattleController.Instance.TurnOrderComparisonStat];
        var speed2 = other.Unit.GetComponent<IStatistics>()[BattleController.Instance.TurnOrderComparisonStat];

        var spdDiff = speed1 - speed2;
        if (spdDiff != 0)
            return -spdDiff;

        //speed tie
        System.Random r = new System.Random();
        var rd = new int[] { -1, 1 };
        return rd[r.Next(2)];
    }
}