﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ExecuteTurnState : BattleState
{
    private bool MonsterFainted { get; set; }

    public override void Enter()
    {
        base.Enter();

        StartCoroutine(RunTurn());
    }

    IEnumerator RunTurn()
    {
        yield return new WaitForSeconds(0.25f);

        Turns.Sort();

        foreach(var turn in Turns)
        {
            yield return new WaitForSeconds(0.25f);
            // TODO play animations, etc
            turn.Action.Perform(turn.Targets);

            while (turn.Action.IsPlaying)
            {
                //Debug.Log(turn.Action.IsPlaying); //Debug
                yield return null;
            }

            if (AnimationQueue.Any())
            {
                AnimationQueue.Sort();

                while(AnimationQueue.Any())
                {
                    var i = AnimationQueue.First();

                    yield return new WaitForSeconds(0.25f);
                    i.Play();

                    while (i.IsPlaying)
                        yield return null;

                    AnimationQueue.RemoveAt(0);
                }

                AnimationQueue.Clear();
            }
            
            //if (IsBattleOver())
            //    owner.ChangeState<CutSceneState>();
            //else if (!UnitHasControl())
            //    owner.ChangeState<SelectUnitState>();
            //if (Turn.HasUnitMoved)
            //    owner.ChangeState<EndFacingState>();
            //else
            //    owner.ChangeState<CommandSelectionState>();
        }

        owner.ChangeState<EndOfTurnState>();
    }


}