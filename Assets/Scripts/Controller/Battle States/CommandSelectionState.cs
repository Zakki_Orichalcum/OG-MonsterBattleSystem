﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandSelectionState : BaseAbilityMenuState
{
    public override void Enter()
    {
        base.Enter();

        //StatPanelController.ShowPrimary(Turn.Actor.gameObject);
        //if (Driver.Current == Drivers.Computer)
            //StartCoroutine(ComputerTurn());
    }

    protected override void Cancel()
    {

    }

    protected override void Confirm()
    {
        switch (AbilityMenuPanelController.Selection)
        {
            case 0: //Fight
                owner.ChangeState<ActionSelectionState>();
                break;
            case 1: //Team
                owner.ChangeState<ViewingNonBattleState>();
                break;
            case 2: //Items
                owner.ChangeState<ViewingItemBattleState>();
                break;
            case 3: //Run
                //owner.ChangeState<EndFacingState>();
                break;
        }
    }

    protected override void LoadMenu()
    {
        if(MenuOptions == null)
        {
            MenuTitle = "Commands";
            MenuOptions = new List<string>();
            MenuOptions.Add("Fight");
            MenuOptions.Add("Team");
            MenuOptions.Add("Item");
            MenuOptions.Add("Run");
        }

        AbilityMenuPanelController.Show(MenuTitle, MenuOptions);
        AbilityMenuPanelController.SetLocked(3, true);
    }
}
