﻿using System.Collections;
using UnityEngine;

public class SelectUnitState : BattleState
{
    private int Index = -1;

    public override void Enter()
    {
        base.Enter();
        StartCoroutine("ChangeCurrentUnit");
    }

    IEnumerator ChangeCurrentUnit()
    {
        owner.Round.MoveNext();
        //SelectTile(Turn.Actor.Tile.Pos);
        //RefreshPrimaryStatPanel(Pos);
        yield return null;
        owner.ChangeState<CommandSelectionState>();
    }
}