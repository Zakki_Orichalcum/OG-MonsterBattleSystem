﻿using UnityEngine;
using System.Collections.Generic;

public class ActionSelectionState : BaseAbilityMenuState
{
    public static AbilityCatalog Catalog;

    protected override void Cancel()
    {
        owner.ChangeState<CommandSelectionState>();
    }

    protected override void Confirm()
    {
        CurrentTurn.Action = Catalog.GetAbility(AbilityMenuPanelController.Selection);

        owner.ChangeState<AbilityTargetState>();
    }

    protected override void LoadMenu()
    {
        Catalog = CurrentUnit.GetComponentInChildren<AbilityCatalog>();
        int count = Catalog.AbilityCount();

        if (MenuOptions == null)
            MenuOptions = new List<string>();
        else
            MenuOptions.Clear();

        bool[] locks = new bool[count];
        for (int i = 0; i < count; ++i)
        {
            Ability ability = Catalog.GetAbility(i);
            AbilityMagicCost cost = ability.GetComponent<AbilityMagicCost>();
            if (cost)
                MenuOptions.Add(string.Format("{0}: {1}", ability.name, cost.Amount));
            else
                MenuOptions.Add(ability.name);
            locks[i] = !ability.CanPerform();
        }

        AbilityMenuPanelController.Show(MenuTitle, MenuOptions);
        for (int i = 0; i < count; ++i)
            AbilityMenuPanelController.SetLocked(i, locks[i]);
    }


    private void SetOptions(string[] options)
    {
        MenuOptions.Clear();
        MenuOptions.AddRange(options);
    }
}