﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BattleState : State {

    protected BattleController owner;
    protected Driver Driver;
    public CameraRig CameraRig { get { return owner.CameraRig; } }
    public AbilityMenuPanelController AbilityMenuPanelController { get { return owner.AbilityMenuPanelController; } }
    public StatPanelController StatPanelController { get { return owner.StatPanelController; } }
    //public HitSuccessIndicator HitSuccessIndicator { get { return owner.HitSuccessIndicator; } }
    public Turn CurrentTurn { get { return owner.CurrentTurn; } set { owner.CurrentTurn = value; } }
    public List<Turn> Turns { get { return owner.Turns; } }
    public List<Unit> Units { get { return owner.Units; } }

    public Unit CurrentUnit { get { return owner.CurrentUnit; } set { owner.CurrentUnit = value; } }
    public Unit OpponentsUnit { get { return owner.OpponentsUnits[0]; } }
    public Unit PlayersUnit { get { return owner.PlayersUnits[0]; } }
    public List<Animation> AnimationQueue { get { return owner.AnimationQueue; } }

    protected virtual void Awake()
    {
        owner = GetComponent<BattleController>();
    }

    protected override void AddListeners()
    {
        if(Driver == null || Driver.Current == Drivers.Human)
        {
            InputController.LeftStickEvent += OnMove_Raw;
            InputController.DpadEvent += OnMove_Raw;
            InputController.InputButtonDownPressedEvent += OnConfirm;
            InputController.InputButtonRightPressedEvent += OnCancel;
        }
    }

    protected override void RemoveListeners()
    {
        InputController.LeftStickEvent -= OnMove_Raw;
        InputController.DpadEvent -= OnMove_Raw;
        InputController.InputButtonDownPressedEvent -= OnConfirm;
        InputController.InputButtonRightPressedEvent -= OnCancel;
    }

    protected virtual void OnConfirm(object sender, InputArgs e)
    {
        
    }

    protected virtual void OnCancel(object sender, InputArgs e)
    {

    }

    protected virtual void OnMove_Raw(object sender, MultiAxisInputArgs e)
    {
        if(e.InputState == InputState.Pressed || e.InputState == InputState.Held)
        {
            OnMove(sender, e);
        }
    }

    protected virtual void OnMove(object sender, MultiAxisInputArgs e)
    {
        
    }

    protected virtual Unit GetUnit(HexPoint p)
    {
        //Tile t = Board.GetTile(p);
        //GameObject content = t != null ? t.Content : null;
        return null; //content != null ? content.GetComponent<Unit>() : null;
    }

    protected virtual void RefreshStatPanels()
    {
        RefreshPrimaryStatPanel(OpponentsUnit);
        RefreshSecondaryStatPanel(PlayersUnit);
    }

    protected virtual void HideStatPanels()
    {
        RefreshPrimaryStatPanel(null);
        RefreshSecondaryStatPanel(null);
    }

    protected virtual void RefreshPrimaryStatPanel(Unit p)
    {
        if (p != null)
            StatPanelController.ShowPrimary(p.gameObject);
        else
            StatPanelController.HidePrimary();
    }

    protected virtual void RefreshSecondaryStatPanel(Unit p)
    {
        if (p != null)
            StatPanelController.ShowSecondary(p.gameObject);
        else
            StatPanelController.HideSecondary();
    }

    protected virtual bool DidPlayerWin()
    {
        return owner.GetComponent<BaseVictoryCondition>().Victor == Alliances.Hero;
    }

    protected virtual bool IsBattleOver()
    {
        return owner.GetComponent<BaseVictoryCondition>().Victor != Alliances.None;
    }
}
