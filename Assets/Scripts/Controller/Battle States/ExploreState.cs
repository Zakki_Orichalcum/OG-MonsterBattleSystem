﻿using UnityEngine;
using System.Collections;

public class ExploreState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        //RefreshPrimaryStatPanel(Pos);
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        //SelectTile(e.Current.ToCubePoint() + Pos);
        //RefreshPrimaryStatPanel(Pos);
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        owner.ChangeState<CommandSelectionState>();
    }
}
