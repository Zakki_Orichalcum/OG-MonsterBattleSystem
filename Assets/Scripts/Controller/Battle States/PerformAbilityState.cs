﻿using UnityEngine;
using System.Collections;

public class PerformAbilityState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        //Turn.HasUnitActed = true;
        //if (Turn.HasUnitMoved)
        //    Turn.LockMove = true;
        StartCoroutine("Animate");
    }

    IEnumerator Animate()
    {
        yield return new WaitForSeconds(0.25f);
        // TODO play animations, etc
        //Turn.Ability.Perform(Turn.Targets);
        
        //while(Turn.Ability.IsPlaying)
        //    yield return null;
        
        //yield return new WaitForSeconds(0.25f);
        //if (IsBattleOver())
        //    owner.ChangeState<CutSceneState>();
        //else if (!UnitHasControl())
        //    owner.ChangeState<SelectUnitState>();
        //if (Turn.HasUnitMoved)
        //    owner.ChangeState<EndFacingState>();
        //else
        //    owner.ChangeState<CommandSelectionState>();
    }

    bool UnitHasControl()
    {
        return CurrentTurn.Actor.GetComponentInChildren<KnockOutStatusEffect>() == null;
    }
}