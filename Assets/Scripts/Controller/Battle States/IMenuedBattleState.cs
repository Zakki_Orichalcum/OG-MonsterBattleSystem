﻿using UnityEngine;
using System.Collections;

public interface IMenuedBattleState
{
    void CloseUnitMenu();
    void UsingAction(ITurnAction action);
}
