﻿using UnityEngine;
using System.Collections;

public class PlayerVictoryBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();

        owner.ShowSmallBattleMessage($"You defeated {owner.Opponent.Name}!");
    }
}
