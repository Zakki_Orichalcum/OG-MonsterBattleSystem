﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InitBattleState : BattleState {
    
    public override void Enter()
    {
        base.Enter();
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        var i = Instantiate(owner.ItemPrefab, owner.Player.transform);
        var item = i.GetComponent<Item>();
        owner.Player.Items = new List<Item> { item };

        yield return StartCoroutine(SpawnTestUnits());
        //AddVictoryCondition();

        owner.ChangeState<InitialSwitchInBattleState>();
    }

    IEnumerator SpawnTestUnits()
    {
        GameObject unitContainer = new GameObject("Units");
        unitContainer.transform.SetParent(owner.transform);

        var uis = new UnitInformation[] {
            CreateUnitInfo("Lohowl"),
            CreateUnitInfo("Icicirobo"),
            CreateUnitInfo("Pupapalloy")
        };

        foreach(var ui in uis)
        {
            yield return StartCoroutine(Create(unitContainer, ui, owner.Player));
        }

        foreach (var ui in uis)
        {
            yield return StartCoroutine(Create(unitContainer, ui, owner.Opponent));
        }
    }

    private void AddVictoryCondition()
    {
        //DefeatTargetVictoryCondition vc = owner.gameObject.AddComponent<DefeatTargetVictoryCondition>();
        //Unit enemy = Units[Units.Count - 1];
        //vc.target = enemy;
        //Health health = enemy.GetComponent<Health>();
        //health.MinHP = 10;
    }

    public IEnumerator Create(GameObject unitContainer, UnitInformation ui, Player p)
    {
        var ec = new EnrichedCoroutine(this, UnitFactory.Create(ui));

        yield return ec.coroutine;

        GameObject instance = (GameObject)ec.result;
        instance.transform.SetParent(unitContainer.transform);
        var unit = instance.GetComponent<Unit>();
        owner.RemoveUnitFromView(unit);

        unit.Player = p;
        p.Units.Add(unit);
    }

    public UnitInformation CreateUnitInfo(string unitType)
    {
        var ui = new UnitInformation();
        ui.UnitType = unitType;
        ui.Level = UnityEngine.Random.Range(45, 55);
        ui.EV_HP = UnityEngine.Random.Range(0, 100);
        ui.EV_MP = UnityEngine.Random.Range(0, 100);
        ui.EV_ATK = UnityEngine.Random.Range(0, 100);
        ui.EV_DEF = UnityEngine.Random.Range(0, 100);
        ui.EV_MAT = UnityEngine.Random.Range(0, 100);
        ui.EV_MDF = UnityEngine.Random.Range(0, 100);
        ui.EV_SPD = UnityEngine.Random.Range(0, 100);
        ui.IV_HP = UnityEngine.Random.Range(0, 31);
        ui.IV_MP = UnityEngine.Random.Range(0, 31);
        ui.IV_ATK = UnityEngine.Random.Range(0, 31);
        ui.IV_DEF = UnityEngine.Random.Range(0, 31);
        ui.IV_MAT = UnityEngine.Random.Range(0, 31);
        ui.IV_MDF = UnityEngine.Random.Range(0, 31);
        ui.IV_SPD = UnityEngine.Random.Range(0, 31);
        ui.Trait = "Shade Dodge";

        var natures = OutsideResource.LoadAll<NatureType>();
        ui.Nature = natures.Keys.ToArray()[Random.Range(0, natures.Keys.Count)];

        ui.Abilities = new string[] { "Form Rush", "ToxiBeam", "Gloss Chop", "Ingot Charge" };

        return ui;
    }
}
