﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseAbilityMenuState : BattleState
{
    protected string MenuTitle;
    protected List<string> MenuOptions;

    public override void Enter()
    {
        base.Enter();
        //SelectTile(Turn.Actor.Tile.Pos);

        //if(Driver.Current == Drivers.Human)
            LoadMenu();
    }

    public override void Exit()
    {
        base.Exit();
        AbilityMenuPanelController.Hide();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        Confirm();
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        Cancel();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if(e.Current.x > 0 || e.Current.y < 0)
        {
            AbilityMenuPanelController.Next();
        }
        else
        {
            AbilityMenuPanelController.Previous();
        }
    }

    protected abstract void LoadMenu();
    protected abstract void Confirm();
    protected abstract void Cancel();
}
