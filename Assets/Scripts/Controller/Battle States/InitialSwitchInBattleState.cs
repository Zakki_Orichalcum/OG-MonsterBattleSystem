﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class InitialSwitchInBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        StartCoroutine("SwitchIn");
    }

    IEnumerator SwitchIn()
    {
        var bp1 = owner.BattlePlacements[0];
        owner.RetrieveUnit(PlayersUnit, bp1.transform.position, bp1.transform.rotation);
        var switchIn = BattleController.InstantiatePrefab<SwitchInAnimation>("GenericAnimations/SwitchInAnimation", PlayersUnit.transform);
        switchIn.Position = bp1.transform.position;
        switchIn.Rotation = bp1.transform.rotation;

        switchIn.Play();
        while (switchIn.IsPlaying)
            yield return null;

        bp1.Content = PlayersUnit;

        var bp2 = owner.BattlePlacements[1];

        owner.RetrieveUnit(OpponentsUnit, bp2.transform.position, bp2.transform.rotation);
        var switchIn2 = BattleController.InstantiatePrefab<SwitchInAnimation>("GenericAnimations/SwitchInAnimation", OpponentsUnit.transform);
        switchIn2.Position = bp2.transform.position;
        switchIn2.Rotation = bp2.transform.rotation;

        switchIn2.Play();
        while (switchIn2.IsPlaying)
            yield return null;

        bp2.Content = OpponentsUnit;
        var stats2 = OpponentsUnit.GetComponent<IStatistics>();

        owner.Player.ParticipatingUnits.Clear();
        owner.Opponent.ParticipatingUnits.Clear();
        owner.Player.ParticipatingUnits.Add(PlayersUnit);
        owner.Opponent.ParticipatingUnits.Add(OpponentsUnit);

        switchIn.gameObject.Destroy();
        switchIn2.gameObject.Destroy();

        owner.ChangeState<TurnStartState>();
    }
}
