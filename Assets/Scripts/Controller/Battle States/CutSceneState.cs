﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneState : BattleState
{
    private ConversationController ConversationController;
    private ConversationData Data;

    protected override void Awake()
    {
        base.Awake();
        ConversationController = owner.GetComponentInChildren<ConversationController>();
        Data = Resources.Load<ConversationData>("Conversations/IntroScene");
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (Data)
        {
            Resources.UnloadAsset(Data);
        }
    }

    public override void Enter()
    {
        base.Enter();
        if (IsBattleOver())
        {
            if (DidPlayerWin())
                Data = Resources.Load<ConversationData>("Conversations/OutroSceneWin");
            else
                Data = Resources.Load<ConversationData>("Conversations/OutroSceneLose");
        }
        else
        {
            Data = Resources.Load<ConversationData>("Conversations/IntroScene");
        }
        ConversationController.Show(Data);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        ConversationController.CompleteEvent += OnCompleteConversation;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        ConversationController.CompleteEvent -= OnCompleteConversation;
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        base.OnConfirm(sender, e);
        ConversationController.Next();
    }

    void OnCompleteConversation(object sender, System.EventArgs e)
    {
        if (IsBattleOver())
            owner.ChangeState<EndBattleState>();
        else
            owner.ChangeState<SelectUnitState>();
    }
}
