﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerOpponentTurnState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        HideStatPanels();
        AbilityMenuPanelController.Hide();

        StartCoroutine(ComputerThoughts());
    }

    IEnumerator ComputerThoughts()
    {
        yield return null;

        owner.Turns.Add(owner.Opponent.Evaluate(owner.OpponentsUnits[0]));

        owner.ChangeState<ExecuteTurnState>();
    }
}