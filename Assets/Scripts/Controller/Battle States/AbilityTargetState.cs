﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityTargetState : BattleState
{
    List<Unit> Targets;
    AbilityRange AR;

    public override void Enter()
    {
        base.Enter();
        if(owner.IsSingleBattle)
        {
            StartCoroutine(MoveToOpponentTurn());
            return;
        }
    }

    public override void Exit()
    {
        base.Exit();
        HideStatPanels();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        //TODO
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        HideStatPanels();
        owner.ChangeState<ComputerOpponentTurnState>();
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.ChangeState<ActionSelectionState>();
    }

    IEnumerator MoveToOpponentTurn()
    {
        yield return null;

        CurrentTurn.Actor = CurrentUnit;
        CurrentTurn.SetTarget(owner.BattlePlacements[1]);
        CurrentTurn.Player = owner.Player;

        Turns.Add(CurrentTurn);

        owner.ChangeState<ComputerOpponentTurnState>();
    }
}