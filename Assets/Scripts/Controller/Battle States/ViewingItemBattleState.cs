﻿using UnityEngine;
using System.Collections;

public class ViewingItemBattleState : BattleState, IMenuedBattleState
{
    public override void Enter()
    {
        base.Enter();
        owner.StatPanelController.Hide();
        owner.AbilityMenuPanelController.gameObject.SetActive(false);
        owner.CameraRig.gameObject.SetActive(false);

        owner.NonBattleMenu.SetActive(true);
        owner.NonBattleMenuController.Show(this, true);
    }

    public override void Exit()
    {
        base.Exit();
        owner.StatPanelController.Show();
        owner.AbilityMenuPanelController.gameObject.SetActive(true);
        owner.CameraRig.gameObject.SetActive(true);
    }

    public void CloseUnitMenu()
    {
        owner.NonBattleMenu.SetActive(false);
        owner.ChangeState<CommandSelectionState>();
    }

    public void UsingAction(ITurnAction action)
    {
        owner.NonBattleMenu.SetActive(false);

        var turn = new Turn();
        turn.Action = action;
        turn.Actor = CurrentUnit;
        turn.Player = owner.Player;

        Turns.Add(turn);

        owner.ChangeState<ComputerOpponentTurnState>();
    }
}
