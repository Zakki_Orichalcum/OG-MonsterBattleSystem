﻿using UnityEngine;
using System.Collections;
using System;
using OrichalcumGames.Statistics;

public class AutoStatusController : MonoBehaviour
{
    public StatusChangeNotifierController Notifier;

    private Camera MainCamera => GetComponentInParent<BattleController>().CameraRig.Camera;

    //void OnEnable()
    //{
    //    this.AddObserver(OnHit, BaseAbilityEffect.HitNotification);
    //    this.AddObserver(OnMiss, BaseAbilityEffect.MissedNotification);
    //    this.AddObserver(OnHeal, HealAbilityEffect.HealNotification);
    //    this.AddObserver(OnStatusChangeNotification, Status.AddedNotification);
    //}

    //void OnDisable()
    //{
    //    this.RemoveObserver(OnHit, BaseAbilityEffect.HitNotification);
    //    this.RemoveObserver(OnMiss, BaseAbilityEffect.MissedNotification);
    //    this.RemoveObserver(OnHeal, HealAbilityEffect.HealNotification);
    //    this.RemoveObserver(OnStatusChangeNotification, Status.RemovedNotification);
    //}

    
}

public class HealthChangedNotification
{
    public Unit Unit;
    //public Part PartStruck;
    public Vector3 PositionOfChange;
    public int HealthChange;
    public IStatistics Stats;
}