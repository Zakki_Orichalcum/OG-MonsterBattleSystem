﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class StatGrowthPanelController : MonoBehaviour
{
    [SerializeField]
    private Panel Panel;
    [SerializeField]
    private GameObject Canvas;

    public Text Name;
    public Text Level;
    public Text AtkStat;
    public Text AtkGrowthStat;
    public Text DefStat;
    public Text DefGrowthStat;
    public Text MatStat;
    public Text MatGrowthStat;
    public Text MdfStat;
    public Text MdfGrowthStat;
    public Text SpdStat;
    public Text SpdGrowthStat;

    private EasingControl ec;
    private (int, int, int, int, int, int, int) _oldStats;
    private (int, int, int, int, int, int, int) _newStats;

    public void Awake()
    {
        ec = gameObject.AddComponent<EasingControl>();
        ec.equation = EasingEquations.EaseInCubic;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;
    }
    public void Start()
    {
        Panel.SetPosition("Hide", false);
        Canvas.SetActive(false);
    }

    public void Show((int, int, int, int, int, int, int) oldStats, (int, int, int, int, int, int, int) newStats, float timing)
    {
        Canvas.SetActive(true);
        Panel.SetPosition("Show", true);
        ec.duration = timing;
        ec.SeekToBeginning();
        _oldStats = oldStats;
        _newStats = newStats;

        ec.Play();
    }

    void OnUpdateEvent(object sender, EventArgs e)
    {
        if(ec.currentValue == 1)
        {
            Panel.SetPosition("Hide", true);
            Canvas.SetActive(false);
        }
        else if(ec.currentValue > 0.8)
        {
            Level.text = _newStats.Item1.ToString();
            AtkStat.text = _newStats.Item3.ToString();
            DefStat.text = _newStats.Item4.ToString();
            MatStat.text = _newStats.Item5.ToString();
            MdfStat.text = _newStats.Item6.ToString();
            SpdStat.text = _newStats.Item7.ToString();

            AtkGrowthStat.text = "";
            DefGrowthStat.text = "";
            MatGrowthStat.text = "";
            MdfGrowthStat.text = "";
            SpdGrowthStat.text = "";
        }
        else if(ec.currentValue > 0.6)
        {
            var deltaAtk = _newStats.Item3 - _oldStats.Item3;
            var deltaDef = _newStats.Item4 - _oldStats.Item4;
            var deltaMat = _newStats.Item5 - _oldStats.Item5;
            var deltaMdf = _newStats.Item6 - _oldStats.Item6;
            var deltaSpd = _newStats.Item7 - _oldStats.Item7;

            var percent = (ec.currentValue - 0.6f) / 0.2f;

            AtkStat.text = (_oldStats.Item3 + (deltaAtk * percent)).ToString();
            DefStat.text = (_oldStats.Item4 + (deltaDef * percent)).ToString();
            MatStat.text = (_oldStats.Item5 + (deltaMat * percent)).ToString();
            MdfStat.text = (_oldStats.Item6 + (deltaMdf * percent)).ToString();
            SpdStat.text = (_oldStats.Item7 + (deltaSpd * percent)).ToString();

            AtkGrowthStat.text = $"+ {deltaAtk - (deltaAtk * percent)}";
            DefGrowthStat.text = $"+ {deltaDef - (deltaDef * percent)}";
            MatGrowthStat.text = $"+ {deltaMat - (deltaMat * percent)}";
            MdfGrowthStat.text = $"+ {deltaMdf - (deltaMdf * percent)}";
            SpdGrowthStat.text = $"+ {deltaSpd - (deltaSpd * percent)}";
        }
        else
        {
            Level.text = $"{_oldStats.Item1} + {_newStats.Item1 - _oldStats.Item1}";

            AtkStat.text = _oldStats.Item3.ToString();
            DefStat.text = _oldStats.Item4.ToString();
            MatStat.text = _oldStats.Item5.ToString();
            MdfStat.text = _oldStats.Item6.ToString();
            SpdStat.text = _oldStats.Item7.ToString();

            AtkGrowthStat.text = $"+ {_newStats.Item3 - _oldStats.Item3}";
            DefGrowthStat.text = $"+ {_newStats.Item4 - _oldStats.Item4}";
            MatGrowthStat.text = $"+ {_newStats.Item5 - _oldStats.Item5}";
            MdfGrowthStat.text = $"+ {_newStats.Item6 - _oldStats.Item6}";
            SpdGrowthStat.text = $"+ {_newStats.Item7 - _oldStats.Item7}";
        }
    }
}
