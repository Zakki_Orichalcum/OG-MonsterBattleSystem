﻿using UnityEngine;
using System.Collections;

public class DefeatTargetVictoryCondition : BaseVictoryCondition
{
    public Unit target;

    protected override void CheckForVictory()
    {
        base.CheckForVictory();
        if (Victor == Alliances.None && IsDefeated(target))
            Victor = Alliances.Hero;
    }
}