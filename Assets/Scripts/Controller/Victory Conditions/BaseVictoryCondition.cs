﻿using UnityEngine;
using System.Collections;

public abstract class BaseVictoryCondition : MonoBehaviour
{
    public Alliances Victor
    {
        get { return victor; }
        protected set { victor = value; }
    }
    Alliances victor = Alliances.None;

    protected BattleController BC;

    protected virtual void Awake()
    {
        BC = GetComponent<BattleController>();
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnStatusAdded, Status.AddedNotification);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnStatusAdded, Status.AddedNotification);
    }

    protected virtual void OnStatusAdded(object sender, object args)
    {
        if (args is KnockOutStatusEffect)
        {
            CheckForVictory();
        }
    }

    protected virtual bool IsDefeated(Unit unit)
    {
        return unit.GetComponentInChildren<KnockOutStatusEffect>();
    }

    protected virtual bool PartyDefeated(Player p)
    {
        for (int i = 0; i < p.Units.Count; ++i)
        {
            Unit a = p.Units[i];
            if (a == null)
                continue;
            if (!IsDefeated(a))
                return false;
        }
        return true;
    }

    protected virtual void CheckForVictory()
    {
        if (PartyDefeated(BattleController.Instance.Player))
            BattleController.Instance.ChangeState<PlayerDefeatedBattleState>();
    }
}