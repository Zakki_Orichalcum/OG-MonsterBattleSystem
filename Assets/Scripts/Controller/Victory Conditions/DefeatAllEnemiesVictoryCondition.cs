﻿using UnityEngine;
using System.Collections;

public class DefeatAllEnemiesVictoryCondition : BaseVictoryCondition
{
    protected override void CheckForVictory()
    {
        base.CheckForVictory();
        if (PartyDefeated(BattleController.Instance.Opponent))
            BattleController.Instance.ChangeState<PlayerVictoryBattleState>();
    }
}