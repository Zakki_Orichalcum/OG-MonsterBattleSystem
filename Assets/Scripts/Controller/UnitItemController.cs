﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using OrichalcumGames.Statistics;

/// <summary>
/// This is all things based out of a hypothetical system
/// that would be used for display purposes only
/// </summary>
public class UnitItemController : MonoBehaviour
{
    public Image BackgroundImage;
    public Text Name;
    public Text Level;
    public Text Health;
    public Text Mana;
    public Image Status;
    public Image HealthBar;
    public Image ManaBar;

    public RectTransform Rect => GetComponent<RectTransform>();
    public Unit Unit;

    public Color SelectedColor;
    public Color SelectedTextColor;
    public Color KnockedOutColor;
    public Color KnockedOutTextColor;
    private Color UnselectedColor;
    private Color UnselectedTextColor;

    public Color HealthBarHigh;
    public Color HealthBarMedium;
    public Color HealthBarLow;
    public Color ManaBarHigh;
    public Color ManaBarMedium;
    public Color ManaBarLow;

    public Vector3 StatusEffectPosition;

    [System.Flags]
    private enum States
    {
        None = 0,
        Selected = 1 << 0,
        Locked = 1 << 1
    }

    private void Awake()
    {
        UnselectedColor = BackgroundImage.color;
        UnselectedTextColor = Level.color;
    }

    public void SetUnit(Unit u)
    {
        Unit = u;
        var stats = Unit.GetComponent<IStatistics>();

        Name.text = Unit.name;
        Level.text = stats["LVL"].ToString();
        Health.text = $"{stats["HP"]}/{stats["MaximumHP"]}";
        Mana.text = $"{stats["MP"]}/{stats["MaximumMP"]}";

        var hBarPercent = 1.0f * stats["HP"]/stats["MaximumHP"];
        var mBarPercent = 1.0f * stats["MP"]/stats["MaximumMP"];

        HealthBar.fillAmount = hBarPercent;
        HealthBar.color = (hBarPercent > 0.5f) ? HealthBarHigh : (hBarPercent > 0.5f) ? HealthBarMedium : HealthBarLow;
        ManaBar.color = (mBarPercent > 0.5f) ? ManaBarHigh : (hBarPercent > 0.5f) ? ManaBarMedium : ManaBarLow;

        var status = u.GetComponentInChildren<StatusEffect>();
        if(status != null)
        {
            var obj = BattleController.InstantiatePrefab($"StatusIndicators/{status.StatusName}Status");
            obj.GetComponent<RectTransform>().anchoredPosition = StatusEffectPosition;
        }
    }

    public bool IsLocked
    {
        get { return (State & States.Locked) != States.None; }
        set
        {
            if (value)
                State |= States.Locked;
            else
                State &= ~States.Locked;
        }
    }

    public bool IsSelected
    {
        get { return (State & States.Selected) != States.None; }
        set
        {
            if (value)
                State |= States.Selected;
            else
                State &= ~States.Selected;
        }
    }

    private States _state;
    private States State
    {
        get { return _state; }
        set
        {
            if (_state == value)
                return;

            _state = value;

            if (IsLocked)
            {
                BackgroundImage.color = KnockedOutColor;
                Name.color = KnockedOutTextColor;
                Level.color = KnockedOutTextColor;
                Health.color = KnockedOutTextColor;
                Mana.color = KnockedOutTextColor;
                //Outline.effectColor = new Color32(20, 36, 44, 255);
            }
            else if (IsSelected)
            {
                BackgroundImage.color = SelectedColor;
                Name.color = SelectedTextColor;
                Level.color = SelectedTextColor;
                Health.color = SelectedTextColor;
                Mana.color = SelectedTextColor;
                //Outline.effectColor = new Color32(255, 160, 72, 255);
            }
            else
            {
                BackgroundImage.color = UnselectedColor;
                Name.color = UnselectedTextColor;
                Level.color = UnselectedTextColor;
                Health.color = UnselectedTextColor;
                Mana.color = UnselectedTextColor;
                //Outline.effectColor = new Color32(20, 36, 44, 255);
            }
        }
    }

    public void Reset()
    {
        State = States.None;
    }
}
