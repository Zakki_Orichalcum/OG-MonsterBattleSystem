﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player Player => GetComponentInChildren<Player>();

    public EncounterInformation EncounterInformation;
    public Vector3 PlayerLastFieldPosition { get; set; }
    public Quaternion PlayerLastFieldRotation { get; set; }

    private static GameManager inst;
    public static GameManager Instance
    {
        get
        {
            if (!inst)
            {
                inst = FindObjectOfType(typeof(GameManager)) as GameManager;
            }

            return inst;
        }
    }

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public Coroutine LaunchCoroutine(IEnumerator coroutine)
    {
        return StartCoroutine(coroutine);
    }

    public void InitiateEncounter(EncounterInformation info)
    {
        EncounterInformation = info;

        //PlayerLastFieldPosition = FieldController.Instance.PlayerPawn.transform.position;
        //PlayerLastFieldRotation = FieldController.Instance.PlayerPawn.transform.rotation;

        StartCoroutine(InitiateBattle());
    }

    IEnumerator InitiateBattle()
    {
        SceneManager.LoadScene("BattleScene");

        yield return new WaitForSeconds(1f);

        BattleController.Instance.Init(EncounterInformation);
    }

    public void ReturnToField()
    {
        SceneManager.LoadScene("FieldScene");

        //FieldController.Instance.LoadPlayer(PlayerLastFieldPosition, PlayerLastFieldRotation);
    }
}
