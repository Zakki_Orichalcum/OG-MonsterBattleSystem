﻿using UnityEngine;
using System.Collections;

public interface IStatPanel
{
    Panel Panel { get; }
    void Display(GameObject obj);
}

public class StatPanelController : MonoBehaviour
{
    #region Const

    const string ShowKey = "Show";
    const string HideKey = "Hide";

    #endregion

    #region Fields

    [SerializeField] OpponentStatPanel _opponentPanel;
    [SerializeField] StatPanel _playerPanel;
    public StatPanel PlayerPanel => _playerPanel;

    Tweener _primaryTransition;
    Tweener _secondaryTransition;

    #endregion

    #region MonoBehaviour

    void Start()
    {
        if (_opponentPanel.Panel.CurrentPosition == null)
            _opponentPanel.Panel.SetPosition(HideKey, false);
        if (_playerPanel.Panel.CurrentPosition == null)
            _playerPanel.Panel.SetPosition(HideKey, false);
    }

    #endregion

    #region Public

    public void Show()
    {
        MovePanel(_opponentPanel, ShowKey, ref _primaryTransition);
        MovePanel(_playerPanel, ShowKey, ref _secondaryTransition);
    }

    public void Hide()
    {
        MovePanel(_opponentPanel, HideKey, ref _primaryTransition);
        MovePanel(_playerPanel, HideKey, ref _secondaryTransition);
    }

    public void ShowPrimary(GameObject obj)
    {
        _opponentPanel.Display(obj);
        MovePanel(_opponentPanel, ShowKey, ref _primaryTransition);
    }

    public void HidePrimary()
    {
        MovePanel(_opponentPanel, HideKey, ref _primaryTransition);
    }

    public void ShowSecondary(GameObject obj)
    {
        _playerPanel.Display(obj);
        MovePanel(_playerPanel, ShowKey, ref _secondaryTransition);
    }

    public void HideSecondary()
    {
        MovePanel(_playerPanel, HideKey, ref _secondaryTransition);
    }

    #endregion

    #region Private

    void MovePanel(IStatPanel obj, string pos, ref Tweener t)
    {
        Panel.Position target = obj.Panel[pos];
        if (obj.Panel.CurrentPosition != target)
        {
            if (t != null && t != null)
                t.Stop();
            t = obj.Panel.SetPosition(pos, true);
            t.duration = 0.5f;
            t.equation = EasingEquations.EaseOutQuad;
        }
    }
    #endregion
}