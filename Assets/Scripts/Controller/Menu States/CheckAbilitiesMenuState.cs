﻿using UnityEngine;
using System.Collections;

public class CheckAbilitiesMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();
        owner.AbilitiesMenuController.Show(owner.Units[owner.Selection]);
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        owner.ChangeState<ConfirmSwitchMenuState>();
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.AbilitiesMenuController.Hide();
        owner.ChangeState<ShowUnitModelMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.AbilitiesMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.AbilitiesMenuController.Next();
        }
        else if(e.Current.x != 0)
        {
            owner.AbilitiesMenuController.Hide();
            owner.ChangeState<SummaryMenuState>();
        }
    }

    protected override void OnLeftShoulderPressed(object sender, InputArgs e)
    {
        owner.TeamMenuController.Previous();
        owner.AbilitiesMenuController.Show(owner.Units[owner.Selection]);
    }

    protected override void OnRightShoulderPressed(object sender, InputArgs e)
    {
        owner.TeamMenuController.Next();
        owner.AbilitiesMenuController.Show(owner.Units[owner.Selection]);
    }
}
