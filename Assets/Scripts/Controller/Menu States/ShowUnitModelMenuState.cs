﻿using UnityEngine;
using System.Collections;

public class ShowUnitModelMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();
        owner.TeamMenuController.Show();
        owner.SelectModel();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnSelect, TeamMenuController.OnSetSelectionNotification, owner.TeamMenuController);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnSelect, TeamMenuController.OnSetSelectionNotification, owner.TeamMenuController);
    }

    private void OnSelect(object sender, object args)
    {
        owner.SelectModel();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        owner.ChangeState<UnitMenuState>();
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.CancelToBattleState();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.TeamMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.TeamMenuController.Next();
        }
    }
}
