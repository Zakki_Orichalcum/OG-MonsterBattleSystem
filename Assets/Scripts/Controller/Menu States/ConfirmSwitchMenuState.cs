﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConfirmSwitchMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();

        var options = new List<string>();
        options.Add("Switch");
        options.Add("Cancel");

        owner.UnitMenuController.Show("Switch Monsters?", options);

        if (owner.Selection == 0)
            owner.UnitMenuController.SetLocked(0, true);
    }

    public override void Exit()
    {
        base.Exit();
        owner.UnitMenuController.Hide();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        switch (owner.UnitMenuController.Selection)
        {
            case 0: //Switch
                owner.SetToSwitch();
                break;
            case 1:
                OnCancel(null, null);
                break;
        }
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.AbilitiesMenuController.Hide();
        owner.StatScreenController.Hide();
        owner.ChangeState<ShowUnitModelMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.UnitMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.UnitMenuController.Next();
        }
    }
}
