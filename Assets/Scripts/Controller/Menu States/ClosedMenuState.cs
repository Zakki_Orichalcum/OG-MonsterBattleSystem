﻿using UnityEngine;
using System.Collections;

public class ClosedMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();
        owner.AbilitiesMenuController.Hide();
        owner.StatScreenController.Hide();
    }
}
