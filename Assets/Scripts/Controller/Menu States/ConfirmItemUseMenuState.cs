﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConfirmItemUseMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();

        var options = new List<string>();
        options.Add("Use");
        options.Add("Cancel");

        owner.UnitMenuController.Show("Use Item?", options);
        owner.ItemMenuController.ItemDescription.gameObject.SetActive(true);
        var item = owner.ItemMenuController.ItemMenu.Entries[owner.ItemMenuController.ItemMenu.Selection] as Item;
        owner.ItemMenuController.ItemDescription.Set(item);
    }

    public override void Exit()
    {
        base.Exit();
        owner.UnitMenuController.Hide();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        switch (owner.UnitMenuController.Selection)
        {
            case 0: //Use
                owner.ChangeState<ChooseItemTargetMenuState>();
                break;
            case 1:
                owner.ChangeState<ShowItemMenuState>();
                break;
        }
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.ItemMenuController.ItemDescription.gameObject.SetActive(false);
        owner.ChangeState<ShowItemMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.UnitMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.UnitMenuController.Next();
        }
    }
}
