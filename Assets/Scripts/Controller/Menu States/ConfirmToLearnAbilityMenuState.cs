﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConfirmToLearnAbilityMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();

        var options = new List<string>();
        options.Add("Learn");
        options.Add("Cancel");

        owner.UnitMenuController.Show($"Learn {owner.Ability_forAbilitySelection.name}?", options);
    }

    public override void Exit()
    {
        base.Exit();
        owner.UnitMenuController.Hide();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        switch (owner.UnitMenuController.Selection)
        {
            case 0: //Switch
                owner.ChangeState<SelectAbilityToLoseMenuState>();
                break;
            case 1:
                OnCancel(null, null);
                break;
        }
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.ChangeState<ConfirmCancelAbilityMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.UnitMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.UnitMenuController.Next();
        }
    }
}
