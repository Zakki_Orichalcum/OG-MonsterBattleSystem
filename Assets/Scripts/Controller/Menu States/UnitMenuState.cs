﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();

        var options = new List<string>();
        options.Add("Switch");
        options.Add("Summary");
        options.Add("Check Abilities");
        options.Add("Cancel");

        owner.UnitMenuController.Show("Battle", options);

        if(owner.Selection == 0)
            owner.UnitMenuController.SetLocked(0, true);
    }

    public override void Exit()
    {
        base.Exit();
        owner.UnitMenuController.Hide();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        switch (owner.UnitMenuController.Selection)
        {
            case 0: //Switch
                owner.SetToSwitch();
                break;
            case 1:
                owner.TeamMenuController.Hide();
                owner.ChangeState<SummaryMenuState>();
                break;
            case 2:
                owner.TeamMenuController.Hide();
                owner.ChangeState<CheckAbilitiesMenuState>();
                break;
            case 3:
                owner.ChangeState<ShowUnitModelMenuState>();
                break;
        }
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.ChangeState<ShowUnitModelMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.UnitMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.UnitMenuController.Next();
        }
    }
    
}
