﻿using UnityEngine;
using System.Collections;

public class MenuState : State
{
    public NonBattleMenuController owner;

    protected virtual void Awake()
    {
        owner = GetComponent<NonBattleMenuController>();
    }

    public override void Enter()
    {
        base.Enter();
    }

    protected override void AddListeners()
    {
        InputController.LeftStickEvent += OnMove_Raw;
        InputController.DpadEvent += OnMove_Raw;
        InputController.InputButtonDownPressedEvent += OnConfirm;
        InputController.InputButtonRightPressedEvent += OnCancel_Start;
        InputController.LeftShoulder1PressedEvent += OnLeftShoulderPressed;
        InputController.RightShoulder1PressedEvent += OnRightShoulderPressed;
    }

    protected override void RemoveListeners()
    {
        InputController.LeftStickEvent -= OnMove_Raw;
        InputController.DpadEvent -= OnMove_Raw;
        InputController.InputButtonDownPressedEvent -= OnConfirm;
        InputController.InputButtonRightPressedEvent -= OnCancel_Start;
        InputController.LeftShoulder1PressedEvent -= OnLeftShoulderPressed;
        InputController.RightShoulder1PressedEvent -= OnRightShoulderPressed;
    }

    protected virtual void OnConfirm(object sender, InputArgs e)
    {

    }

    protected void OnCancel_Start(object sender, InputArgs e)
    {
        if (owner.AllowCancel)
            OnCancel(sender, e);
    }

    protected virtual void OnCancel(object sender, InputArgs e)
    {

    }

    protected virtual void OnMove_Raw(object sender, MultiAxisInputArgs e)
    {
        if (e.InputState == InputState.Pressed || e.InputState == InputState.Held)
        {
            OnMove(sender, e);
        }
    }

    protected virtual void OnMove(object sender, MultiAxisInputArgs e)
    {

    }

    protected virtual void OnLeftShoulderPressed(object sender, InputArgs e)
    {

    }

    protected virtual void OnRightShoulderPressed(object sender, InputArgs e)
    {

    }
}
