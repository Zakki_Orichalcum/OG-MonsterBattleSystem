﻿using UnityEngine;
using System.Collections;

public class ChooseItemTargetMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();
        owner.TeamMenuController.Show();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        owner.SetToUseItem();
        owner.ItemMenuController.Hide();
        owner.TeamMenuController.Hide();
        owner.ItemMenuController.ItemDescription.gameObject.SetActive(false);
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.ItemMenuController.ItemDescription.gameObject.SetActive(false);
        owner.ChangeState<ShowItemMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.TeamMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.TeamMenuController.Next();
        }
    }
}
