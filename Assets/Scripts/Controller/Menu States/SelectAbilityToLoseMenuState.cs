﻿using UnityEngine;
using System.Collections;

public class SelectAbilityToLoseMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();

        owner.AbilitiesMenuController.Show(owner.Unit_ForAbilitySelection);
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        if(owner.AbilitiesMenuController.Selection == 5)
        {
            owner.ChangeState<ConfirmCancelAbilityMenuState>();
        }
        else
        {
            owner.ChangeState<ConfirmAbilityToLoseMenuState>();
        }
        
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.AbilitiesMenuController.Hide();
        owner.ChangeState<ConfirmCancelAbilityMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.y > 0)
        {
            owner.AbilitiesMenuController.Previous();
        }
        else if (e.Current.y < 0)
        {
            owner.AbilitiesMenuController.Next();
        }
    }
}
