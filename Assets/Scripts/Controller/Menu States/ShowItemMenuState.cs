﻿using UnityEngine;
using System.Collections;

public class ShowItemMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();
        owner.ItemMenuController.Show();
        owner.TeamMenuController.Show();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if(e.Current.x < 0)
        {
            owner.ItemMenuController.PreviousItemCategory();
        }
        else if (e.Current.x > 0)
        {
            owner.ItemMenuController.NextItemCategory();
        }
        else if(e.Current.y > 0)
        {
            owner.ItemMenuController.ItemMenu.Previous();
        }
        else if(e.Current.y < 0)
        {
            owner.ItemMenuController.ItemMenu.Next();
        }
    }

    protected override void OnLeftShoulderPressed(object sender, InputArgs e)
    {
        owner.ItemMenuController.PreviousItemCategory();
    }

    protected override void OnRightShoulderPressed(object sender, InputArgs e)
    {
        owner.ItemMenuController.NextItemCategory();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        owner.ChangeState<ConfirmItemUseMenuState>();
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.ItemMenuController.Hide();
        owner.TeamMenuController.Hide();
        owner.Hide();
    }
}
