﻿using UnityEngine;
using System.Collections;

public class SummaryMenuState : MenuState
{
    public override void Enter()
    {
        base.Enter();
        owner.UnitDisplay.gameObject.SetActive(true);
        owner.StatScreenController.Show(owner.Units[owner.Selection]);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnSelect, TeamMenuController.OnSetSelectionNotification, owner.TeamMenuController);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnSelect, TeamMenuController.OnSetSelectionNotification, owner.TeamMenuController);
    }

    private void OnSelect(object sender, object args)
    {
        owner.SelectModel();
    }

    protected override void OnConfirm(object sender, InputArgs e)
    {
        owner.ChangeState<ConfirmSwitchMenuState>();
    }

    protected override void OnCancel(object sender, InputArgs e)
    {
        owner.StatScreenController.Hide();
        owner.ChangeState<ShowUnitModelMenuState>();
    }

    protected override void OnMove(object sender, MultiAxisInputArgs e)
    {
        if (e.Current.x != 0)
        {
            owner.StatScreenController.Hide();
            owner.UnitDisplay.gameObject.SetActive(false);
            owner.ChangeState<CheckAbilitiesMenuState>();
        }
    }

    protected override void OnLeftShoulderPressed(object sender, InputArgs e)
    {
        owner.TeamMenuController.Previous();
        owner.StatScreenController.Show(owner.Units[owner.Selection]);
    }

    protected override void OnRightShoulderPressed(object sender, InputArgs e)
    {
        owner.TeamMenuController.Next();
        owner.StatScreenController.Show(owner.Units[owner.Selection]);
    }
}
