﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusChangeNotifierController : MonoBehaviour
{
    [SerializeField]
    private Canvas Canvas;
    [SerializeField]
    private Text Text;

    private string Green = "00D68F";
    private string Red = "C00C00";

    private string Default = "F3F3F3";

    public void PlayChangeNumber(int i)
    {
        PlayChange(Math.Abs(i).ToString(), i > -1 ? Green : Red);
    }

    public void PlayChange(string str, string col = null)
    {
        Text.text = str;
        Text.color = (col ?? Default).HexToColor();

        StartCoroutine("Play");
    }

    private IEnumerator Play()
    {
        var maximumHeight = 1f;

        var howLong = 0.8f;
        var StartPosition = transform.localPosition;

        var t = transform.MoveToLocal(transform.localPosition + new Vector3(0, maximumHeight, 0),
            howLong, EasingEquations.EaseInOutQuad);

        while (t != null)
            yield return null;
        
        gameObject.Destroy();
    }

    internal void SetCamera(Camera mainCamera)
    {
        var c = Canvas.GetComponent<AlwaysFaceTheCamera>();
        c.m_Camera = mainCamera;
        c.Active = true;
    }
}
