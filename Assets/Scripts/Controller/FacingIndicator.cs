﻿using UnityEngine;
using System.Collections;

public class FacingIndicator : MonoBehaviour
{
    [SerializeField] Renderer[] Directions;
    [SerializeField] Material Normal;
    [SerializeField] Material Selected;

    public void SetDirection(Directions dir)
    {
        int index = (int)dir;
        for (int i = 0; i < 6; ++i)
            Directions[i].material = (i == index) ? Selected : Normal;
    }
}