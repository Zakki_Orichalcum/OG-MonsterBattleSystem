﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawnController : MonoBehaviour
{
    public const string CollisionEnterNotification = "PlayerPawnController.CollisionEnterNotification";
    public const string CollisionExitNotification = "PlayerPawnController.CollisionExitNotification";

    private void OnTriggerEnter(Collider other)
    {
        var i = other.gameObject.GetComponent<Interactable>();
        if (i)
        {
            this.PostNotification(CollisionEnterNotification, i);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var i = other.gameObject.GetComponent<Interactable>();
        if (i)
        {
            this.PostNotification(CollisionExitNotification, i);
        }
    }
}
