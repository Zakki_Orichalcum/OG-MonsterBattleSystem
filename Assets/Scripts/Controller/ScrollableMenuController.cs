﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public interface IMenuItem
{
    bool IsLocked { get; }
}

public class ScrollableMenuController : MonoBehaviour
{
    private const string ShowKey = "Show";
    private const string HideKey = "Hide";
    private const string EntryPoolKey = ".Entry";

    public int MenuCount = 4;

    public Image TopScollbar;
    public Image BottomScollbar;

    [SerializeField]
    private MenuItem EntryPrefab;
    [SerializeField]
    private Panel Panel;
    [SerializeField]
    private Transform MenuPanel;
    private List<MenuItem> MenuEntries;
    public int Selection { get; private set; }

    public List<IMenuItem> Entries;

    private int CurrentTopOfMenu;
    private int CurrentBottomOfMenu;

    private void Awake()
    {
        GameObjectPoolController.AddEntry(name + EntryPoolKey, EntryPrefab.gameObject, MenuCount, int.MaxValue);
        MenuEntries = new List<MenuItem>(MenuCount);
    }

    private void Start()
    {
        Panel.gameObject.SetActive(false);
        Panel.SetPosition(HideKey, false);
    }

    private MenuItem Dequeue()
    {
        Poolable p = GameObjectPoolController.Dequeue(name + EntryPoolKey);
        MenuItem entry = p.GetComponent<MenuItem>();
        entry.transform.SetParent(MenuPanel, false);
        entry.transform.localScale = Vector3.one;
        entry.gameObject.SetActive(true);
        entry.Reset();

        return entry;
    }

    private void Enqueue(MenuItem entry)
    {
        Poolable p = entry.GetComponent<Poolable>();
        GameObjectPoolController.Enqueue(p);
    }

    private void Clear()
    {
        for (int i = MenuEntries.Count - 1; i >= 0; --i)
        {
            Enqueue(MenuEntries[i]);
        }
        MenuEntries.Clear();
    }

    private Tweener TogglePos(string pos)
    {
        Tweener t = Panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;

        return t;
    }

    private bool SetSelection(int value)
    {
        if (value >= MenuEntries.Count || MenuEntries[value].IsLocked)
            return false;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = false;

        if(value < CurrentTopOfMenu)
        {
            CurrentTopOfMenu -= value;
            CurrentBottomOfMenu -= value;
            SetupMenu();
        }
        else if(value > CurrentBottomOfMenu)
        {
            CurrentTopOfMenu += value;
            CurrentBottomOfMenu += value;
            SetupMenu();
        }

        var scrollBarPercentage = 1f * (CurrentBottomOfMenu - CurrentTopOfMenu) / Entries.Count;
        var topPercent = (1f * CurrentTopOfMenu / Entries.Count) - (scrollBarPercentage / 2f);
        var bottomPercent = (1f * CurrentBottomOfMenu / Entries.Count) + (scrollBarPercentage / 2f);
        TopScollbar.fillAmount = topPercent;
        BottomScollbar.fillAmount = bottomPercent;

        Selection = value;

        if (Selection >= 0 && Selection < MenuEntries.Count)
            MenuEntries[Selection].IsSelected = true;

        return true;
    }

    public void Next()
    {
        for (int i = Selection + 1; i < Selection + MenuEntries.Count; ++i)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                break;
        }
    }

    public void Previous()
    {
        int i = Selection - 1 + MenuEntries.Count; var breakout = false;
        while (i > Selection && !breakout)
        {
            int index = i % MenuEntries.Count;
            if (SetSelection(index))
                breakout = true;
            i--;
        }
    }

    public void Show(List<IMenuItem> entries)
    {
        Entries = entries;

        Panel.gameObject.SetActive(true);

        CurrentTopOfMenu = 0;
        CurrentBottomOfMenu = Math.Min(Entries.Count, MenuCount);

        SetupMenu();

        SetSelection(0);

        TogglePos(ShowKey);
    }

    public void SetupMenu()
    {
        Clear();
        var locked = new List<int>();

        for (int i = CurrentTopOfMenu; i < CurrentBottomOfMenu; ++i)
        {
            MenuItem entry = Dequeue();
            entry.Set(Entries[i]);
            MenuEntries.Add(entry);

            if (Entries[i].IsLocked)
                locked.Add(i);
        }


        foreach (var i in locked)
            SetLocked(i, true);
    }

    public void Hide()
    {
        Tweener t = TogglePos(HideKey);
        t.completedEvent += delegate (object sender, System.EventArgs e)
        {
            if (Panel.CurrentPosition == Panel[HideKey])
            {
                Clear();
                Panel.gameObject.SetActive(false);
            }
        };
    }

    public void SetLocked(int index, bool value)
    {
        if (index < 0 || index >= MenuEntries.Count)
        {
            return;
        }

        MenuEntries[index].IsLocked = value;
        if (value && Selection == index)
            Next();
    }
}
