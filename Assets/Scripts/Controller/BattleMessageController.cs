﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class BattleMessageController : MonoBehaviour
{
    [SerializeField] private Text Label;
    [SerializeField] private Text Label2;
    [SerializeField] private GameObject Canvas;
    [SerializeField] private CanvasGroup Group;

    private EasingControl ec;
    private EasingControl ec_l1;
    private EasingControl ec_l2;

    private Vector2 _oldPlacementLabel1;
    private Vector2 _oldPlacementLabel2;

    public const float BattleMessageDisplayTime = 3.0f;

    void Awake()
    {
        ec = gameObject.AddComponent<EasingControl>();
        ec.duration = BattleMessageDisplayTime * 0.125f;
        ec.equation = EasingEquations.EaseInOutQuad;
        ec.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec.updateEvent += OnUpdateEvent;

        ec_l1 = gameObject.AddComponent<EasingControl>();
        ec_l1.duration = BattleMessageDisplayTime * 0.125f;
        ec_l1.equation = EasingEquations.EaseInOutQuad;
        ec_l1.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec_l1.updateEvent += OnUpdateEvent_Label1;

        ec_l2 = gameObject.AddComponent<EasingControl>();
        ec_l2.duration = BattleMessageDisplayTime * 0.25f;
        ec_l2.equation = EasingEquations.EaseInOutQuad;
        ec_l2.endBehaviour = EasingControl.EndBehaviour.Constant;
        ec_l2.updateEvent += OnUpdateEvent_Label2;

        _oldPlacementLabel1 = Label.rectTransform.anchoredPosition;
        _oldPlacementLabel2 = Label2.rectTransform.anchoredPosition;
        Canvas.SetActive(false);
    }

    public void Display(string message, string message2 = "")
    {
        Canvas.SetActive(true);
        Group.alpha = 0;
        Label.text = message;
        Label2.text = message2;
        StartCoroutine(Sequence());
    }

    void OnUpdateEvent(object sender, EventArgs e)
    {
        Group.alpha = ec.currentValue;
    }

    void OnUpdateEvent_Label1(object sender, EventArgs e)
    {
        var c = Label.color;
        c.a = ec_l1.currentValue;
        Label.color = c;

        var pos = new Vector2(_oldPlacementLabel1.x - 20 * (1.0f - ec_l1.currentValue), _oldPlacementLabel1.y);
        Label.rectTransform.anchoredPosition = pos;
    }

    void OnUpdateEvent_Label2(object sender, EventArgs e)
    {
        var c = Label2.color;
        c.a = ec_l2.currentValue;
        Label2.color = c;

        var pos = new Vector2(_oldPlacementLabel2.x + 20 * (1.0f - ec_l2.currentValue), _oldPlacementLabel2.y);
        Label2.rectTransform.anchoredPosition = pos;
    }

    IEnumerator Sequence()
    {
        ec.Play();

        while (ec.IsPlaying)
            yield return null;

        ec_l1.Play();
        ec_l2.Play();

        while (ec_l1.IsPlaying || ec_l2.IsPlaying)
            yield return null;

        yield return new WaitForSeconds(BattleMessageDisplayTime * 0.5f);

        ec.Reverse();
        ec_l1.Reverse();
        ec_l2.Reverse();

        while (ec.IsPlaying || ec_l1.IsPlaying || ec_l2.IsPlaying)
            yield return null;

        Canvas.SetActive(false);
    }
}