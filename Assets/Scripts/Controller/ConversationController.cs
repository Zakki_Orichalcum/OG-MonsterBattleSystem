﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationController : MonoBehaviour {

    [SerializeField] private ConversationPanel LeftPanel;
    [SerializeField] private ConversationPanel RightPanel;
    private Canvas Canvas;
    private IEnumerator Conversation;
    private Tweener Transition;

    private const string ShowTop = "Show Top";
    private const string ShowBottom = "Show Bottom";
    private const string HideTop = "Hide Top";
    private const string HideBottom = "Hide Bottom";

    public static event EventHandler CompleteEvent;
    

	// Use this for initialization
	void Start () {
        Canvas = GetComponentInChildren<Canvas>();
        if(LeftPanel.Panel.CurrentPosition == null)
        {
            LeftPanel.Panel.SetPosition(HideBottom, false);
        }
        if(RightPanel.Panel.CurrentPosition == null)
        {
            RightPanel.Panel.SetPosition(HideBottom, false);
        }
        Canvas.gameObject.SetActive(false);
	}
	
	public void Show(ConversationData data)
    {
        Canvas.gameObject.SetActive(true);
        Conversation = Sequence(data);
        Conversation.MoveNext();
    }

    public void Next()
    {
        if (Conversation == null || Transition != null)
            return;

        Conversation.MoveNext();
    }

    private IEnumerator Sequence(ConversationData data)
    {
        for(int i = 0; i < data.List.Count; ++i)
        {
            SpeakerData sd = data.List[i];

            ConversationPanel currentPanel = (sd.anchor == TextAnchor.UpperLeft ||
                                                sd.anchor == TextAnchor.MiddleLeft ||
                                                sd.anchor == TextAnchor.LowerLeft) ?
                                                LeftPanel : RightPanel;

            IEnumerator presenter = currentPanel.Display(sd);
            presenter.MoveNext();

            string show, hide;
            if(sd.anchor == TextAnchor.UpperLeft || sd.anchor == TextAnchor.UpperCenter || sd.anchor == TextAnchor.UpperRight)
            {
                show = ShowTop;
                hide = HideTop;
            }
            else
            {
                show = ShowBottom;
                hide = HideBottom;
            }

            currentPanel.Panel.SetPosition(hide, false);
            MovePanel(currentPanel, show);

            yield return null;
            while (presenter.MoveNext())
                yield return null;

            MovePanel(currentPanel, hide);
            Transition.completedEvent += delegate (object sender, EventArgs e)
            {
                Conversation.MoveNext();
            };

            yield return null;
        }

        Canvas.gameObject.SetActive(false);
        if(CompleteEvent != null)
        {
            CompleteEvent(this, EventArgs.Empty);
        }
    }

    private void MovePanel(ConversationPanel obj, string pos)
    {
        Transition = obj.Panel.SetPosition(pos, true);
        Transition.duration = 0.5f;
        Transition.equation = EasingEquations.EaseOutQuad;
    }
}
