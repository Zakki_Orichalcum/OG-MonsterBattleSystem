﻿using UnityEngine;
using System.Collections;

public class CrossFadeAudioDemo : MonoBehaviour
{
    [SerializeField] AudioSource fadeInSource;
    [SerializeField] AudioSource fadeOutSource;

    void Start()
    {
        fadeInSource.volume = 0;
        fadeOutSource.volume = 1;

        fadeInSource.Play();
        fadeOutSource.Play();

        fadeInSource.VolumeTo(1);
        fadeOutSource.VolumeTo(0);
    }
}