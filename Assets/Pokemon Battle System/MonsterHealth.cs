﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class MonsterHealth : Health
{
    protected override void OnHPDidChange(object sender, object args)
    {
        var stats = sender as IStatistics;
        if (stats[HealthStatName] == 0 && !BattleController.Instance.IsCurrentState<InitBattleState>())
        {
            var status = stats.GameObject.GetComponent<Status>();
            status.Add<KnockOutStatusEffect, StatusCondition>();

            var instance = BattleController.InstantiatePrefab<KnockedOutAnimation>("GenericAnimations/KnockedOutAnimation", stats.GameObject.transform);
            BattleController.Instance.AddToAnimationQueue(instance);
        }
    }
}
