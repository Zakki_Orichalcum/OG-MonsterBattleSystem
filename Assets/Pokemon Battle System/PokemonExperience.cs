﻿using UnityEngine;
using System.Collections;
using OrichalcumGames.Statistics;

public class PokemonExperience : MonoBehaviour
{
    public static int ExperienceGainedFromUnit(Unit unit, Unit knockedOutUnit, BattleType battleType,
                                                    int participatingUnits, int heldExp)
    {
        var holdsExpShare = unit.GetComponentInChildren<SharesExperience>() != null;
        var expGrowthItem = unit.GetComponentInChildren<ExperienceGrowth>();

        var a = battleType.HasFlag(BattleType.Trainer) ? 1.5f : 1f;
        var b = knockedOutUnit.MonsterType.VictoryYield;
        var L = knockedOutUnit.GetComponent<Level>().LVL;
        var Lp = unit.GetComponent<Level>().LVL;
        var s = heldExp == 0 ? participatingUnits : holdsExpShare ? 2 * heldExp : 2 * participatingUnits;
        var e = expGrowthItem != null ? expGrowthItem.GrowthAmount : 1f;

        //float delta = (((a * b * L)/5 * s) * (Mathf.Pow(2*L + 10,2.5f)/ Mathf.Pow(L + Lp + 10, 2.5f)) + 1) * e; //Scaled Formula
        float delta = (a * b * e * L) / (7f * s); //Flat Formula

        return Mathf.RoundToInt(delta);
    }
}
