﻿using UnityEngine;
using System.Collections;

public class MatchException : BaseException
{
    public readonly Unit Attacker;
    public readonly Unit Target;

    public MatchException(Unit attacker, Unit target) : base(false)
    {
        this.Attacker = attacker;
        this.Target = target;
    }
}