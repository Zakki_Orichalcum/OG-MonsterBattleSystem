﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Attribute", menuName = "Orichalcum Games/Create Attribute", order = 2)]
public class AttributeScriptable : ScriptableObject
{
    public string DisplayName;
    public Color Color;
    public AttributeEffectiveness[] AttributeEffectivenesses;

    [System.Serializable]
    public class AttributeEffectiveness
    {
        public string AttributeName;
        public bool IsStronglyEffective;
        public bool IsNotVeryEffective;
        public bool WillNotAffect;
    }
}
