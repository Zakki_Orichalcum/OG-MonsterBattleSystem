﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Monster", menuName = "Orichalcum Games/Create Monster", order = 1)]
public class MonsterScriptable : ScriptableObject
{
    public string DisplayName;
    public AttributeScriptable[] Attributes;
    public string LevelCurve;
    public float VictoryYield;
    public BaseStatistic[] Statistics;

    /// <summary>
    /// Put Level up levels in the Info section
    /// </summary>
    public AbilitiesGained[] GainedAbilites;
    public TraitsGained[] AvailableTraits;

    public Evolution[] Evolutions;

    [System.Serializable]
    public class BaseStatistic
    {
        public string StatisticName;
        public int Value;
    }

    [System.Serializable]
    public class AbilitiesGained
    {
        public Ability Ability;
        public string GainMethod;
        public string Info;
    }

    [System.Serializable]
    public class TraitsGained
    {
        public Trait Trait;
        public float Chance;
    }

    [System.Serializable]
    public class Evolution
    {
        public MonsterScriptable Monster;
        public string EvolutionMethod;
        public string Info;
    }
}
